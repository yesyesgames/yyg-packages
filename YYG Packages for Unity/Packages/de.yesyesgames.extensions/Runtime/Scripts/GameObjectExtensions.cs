﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class GameObjectExtensions
    {

        public static void DestroyChildren(this GameObject target, bool immediate = false) => target.transform.DestroyChildren(immediate);

        public static void Active(this GameObject target) => Show(target);
        public static void Inactive(this GameObject target) => Hide(target);
        public static void Active(this Behaviour target) => Show(target);
        public static void Inactive(this Behaviour target) => Hide(target);

        /// <summary>
        /// Unity's SetActive(true) method.
        /// </summary>
        public static void Show(this Behaviour target)
        {
            if (target == null) return;
            Show(target.gameObject);
        }

        /// <summary>
        /// Unity's SetActive(true) method.
        /// </summary>
        public static void Show(this GameObject target)
        {
            if (target == null) return;
            target.SetActive(true);
        }


        /// <summary>
        /// Unity's SetActive(false) method.
        /// </summary>
        public static void Hide(this Behaviour target)
        {
            if (target == null) return;
            Hide(target.gameObject);
        }

        /// <summary>
        /// Unity's SetActive(false) method.
        /// </summary>
        public static void Hide(this GameObject target)
        {
            if (target == null) return;
            target.SetActive(false);
        }
    }

}

