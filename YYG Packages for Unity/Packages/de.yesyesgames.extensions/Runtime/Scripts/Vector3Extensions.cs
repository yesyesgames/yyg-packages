﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class Vector3Extensions
    {

        /// <summary>
        /// Quantizes the x, y, and z components of the input Vector3 by rounding them to the nearest multiple of the factor.
        /// </summary>
        /// <param name="value">The original Vector3 to be quantized.</param>
        /// <param name="precision">The precision to which the components should be quantized.</param>
        /// <param name="inversePrecision">If true, the inverse of precision is used as the factor for quantization. If false, precision is used directly.</param>
        /// <returns>Returns the new Vector3 with quantized components.</returns>
        public static Vector3 Quantize(this Vector3 value, float precision = 1, bool inversePrecision = true)
        {
            var factor = inversePrecision ? (1f / precision) : precision;
            return new(
                Mathf.Round(value.x * factor) / factor,
                Mathf.Round(value.y * factor) / factor,
                Mathf.Round(value.z * factor) / factor);
        }


    }
}
