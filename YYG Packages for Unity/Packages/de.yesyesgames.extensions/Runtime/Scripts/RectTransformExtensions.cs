﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class RectTransformExtensions
    {

        /// <summary>
        /// Screen height based look-up.
        /// </summary>
        public static bool IsBecomeVisible(this RectTransform rectTransform)
        {

            var v = new Vector3[4];
            rectTransform.GetWorldCorners(v);

            var maxY = Mathf.Max(v[0].y, v[1].y, v[2].y, v[3].y);
            var minY = Mathf.Min(v[0].y, v[1].y, v[2].y, v[3].y);

            if (maxY < 0 || minY > Screen.height)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


    }
}
