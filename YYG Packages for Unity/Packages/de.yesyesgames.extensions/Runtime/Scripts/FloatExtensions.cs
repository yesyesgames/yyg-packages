﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class FloatExtensions
    {

        public static bool IsApproximately(this float target, float value, float threshold = .1f)
        {
            return (target >= value - threshold) && (target <= value + threshold);
        }


    }
}
