﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class Vector2Extensions
    {

        /// <summary>
        /// Quantizes the x and y components of the input Vector2 by rounding them to the nearest multiple of the factor.
        /// </summary>
        /// <param name="value">The original Vector2 to be quantized.</param>
        /// <param name="precision">The precision to which the components should be quantized.</param>
        /// <param name="inversePrecision">If true, the inverse of precision is used as the factor for quantization. If false, precision is used directly.</param>
        /// <returns>Returns the new Vector2 with quantized components.</returns>
        public static Vector2 Quantize(this Vector2 value, float precision = 1, bool inversePrecision = true)
        {
            var result = new Vector3(value.x, value.y, 0).Quantize(precision, inversePrecision);
            return new(result.x, result.y);
        }


    }
}
