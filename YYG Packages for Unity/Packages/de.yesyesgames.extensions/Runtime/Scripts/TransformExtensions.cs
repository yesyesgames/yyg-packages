﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class TransformExtensions
    {

        public static void Active(this Transform target) => Show(target);
        public static void Inactive(this Transform target) => Hide(target);
        public static void Active(this RectTransform target) => Show(target);
        public static void Inactive(this RectTransform target) => Hide(target);


        /// <summary>
        /// Unity's SetActive(true) method.
        /// </summary>
        public static void Show(this Transform target) => target.gameObject.Show();


        /// <summary>
        /// Unity's SetActive(false) method.
        /// </summary>
        public static void Hide(this Transform target) => target.gameObject.Hide();


        /// <summary>
        /// Unity's SetActive(false) method.
        /// </summary>
        public static void Show(this RectTransform target) => target.gameObject.Show();


        /// <summary>
        /// Unity's SetActive(false) method.
        /// </summary>
        public static void Hide(this RectTransform target) => target.gameObject.Hide();


        public static void DestroyChildren(this Transform target, bool immediate = false)
        {
            if (target == null) return;
            foreach (Transform child in target)
            {
                if (immediate)
                    GameObject.DestroyImmediate(child.gameObject);
                else
                    GameObject.Destroy(child.gameObject);
            }
        }

    }
}

