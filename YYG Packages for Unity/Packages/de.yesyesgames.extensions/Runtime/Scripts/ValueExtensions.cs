﻿///////////////////////////////////////////////////////////////////////////////
using System.Text.RegularExpressions;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Extensions
{
    public static class ValueExtensions
    {

        public static void AddAndClamp(this ref int source, int value, int min, int max)
        {
            source = Mathf.Clamp(source + value, min, max);
        }


        public static string FindAndReplace(this string value, string find, string replace, bool matchWholeWord = false)
        {
            string textToFind = matchWholeWord ? string.Format(@"\b{0}\b", find) : find;
            return Regex.Replace(value, textToFind, replace);
        }


        public static void IncrementSafe(this ref int value, int max, int step = 1)
        {
            value = (value + step) % max;
        }

    }
}
