﻿using UnityEditor;

using UnityEngine;

using YYG.Packages.Unity.AppEvents.Objects;

[CustomEditor(typeof(AppEvent))]
public class AppEventEditor : Editor
{



    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        AppEvent src = (AppEvent)target;

        GUILayout.Space(15);
        GUILayout.Label("App event data", EditorStyles.boldLabel);
        GUILayout.BeginVertical(EditorStyles.helpBox);

        src.m_int = EditorGUILayout.IntField("Int: ", src.m_int);
        src.m_text = EditorGUILayout.TextField("String: ", src.m_text);
        src.m_myBool = EditorGUILayout.Toggle("Boolean: ", src.m_myBool);
        src.m_scriptableObject = (ScriptableObject)EditorGUILayout.ObjectField("ScriptableObject: ", src.m_scriptableObject, typeof(ScriptableObject), true);

        GUILayout.EndVertical();
        GUILayout.Space(15);
        GUILayout.Label("Options", EditorStyles.boldLabel);
        GUILayout.BeginVertical(EditorStyles.helpBox);

        if (GUILayout.Button("Reset data"))
        {
            src.m_int = 0;
            src.m_text = string.Empty;
            src.m_scriptableObject = null;
        }

        GUILayout.Space(5);
        GUI.enabled = Application.isPlaying;

        if (GUILayout.Button("Raise"))
            src.Raise();
        if (GUILayout.Button("Raise (Int)"))
            src.Raise(src.m_int);
        if (GUILayout.Button("Raise (String)"))
            src.Raise(src.m_text);
        if (GUILayout.Button("Raise (Boolean)"))
            src.Raise(src.m_myBool);
        if (GUILayout.Button("Raise (ScriptableObject)"))
            src.Raise(src.m_scriptableObject);

        GUI.enabled = true;
        GUILayout.EndVertical();

    }
}
