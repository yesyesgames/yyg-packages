﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListenerBool : EventListenerAbstract
    {

        [SerializeField]
        private OnBoolEvent m_eventResponse = null;

        [System.Serializable]
        public class OnBoolEvent : UnityEvent<bool> { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke(true);
        }


        public override void OnEventRaised(object args)
        {
            if (args is bool param)
                m_eventResponse.Invoke(param);
        }



    }
}
