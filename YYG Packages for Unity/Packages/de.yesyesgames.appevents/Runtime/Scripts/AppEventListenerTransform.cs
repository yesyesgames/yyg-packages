﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListenerTransform : EventListenerAbstract
    {

        [SerializeField]
        private OnGameObjectEvent m_eventResponse = null;

        [System.Serializable]
        public class OnGameObjectEvent : UnityEvent<Transform> { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke(default);
        }


        public override void OnEventRaised(object args)
        {
            if (args is Transform param)
                m_eventResponse.Invoke(param);
        }



    }
}