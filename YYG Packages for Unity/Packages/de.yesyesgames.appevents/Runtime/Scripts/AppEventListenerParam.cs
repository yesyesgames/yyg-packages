﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{
    public class AppEventListenerParam : EventListenerAbstract
    {

        [SerializeField]
        private OnParamEvent m_eventResponse = null;

        [System.Serializable]
        public class OnParamEvent : UnityEvent<Param> { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke(default);
        }


        public override void OnEventRaised(object args)
        {
            if (args is Param param)
                m_eventResponse.Invoke(param);
        }



    }
}
