﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListenerScriptableObject : EventListenerAbstract
    {

        [SerializeField]
        private OnCustomEvent m_eventResponse = null;

        [System.Serializable]
        public class OnCustomEvent : UnityEvent<ScriptableObject> { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke(null);
        }


        public override void OnEventRaised(object args)
        {
            if (args is ScriptableObject param)
                m_eventResponse.Invoke(param);
        }


    }
}
