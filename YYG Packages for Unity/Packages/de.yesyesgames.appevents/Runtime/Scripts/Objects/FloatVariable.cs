﻿using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents.Objects
{

    [CreateAssetMenu(fileName = "FloatVariable", menuName = "YesYesGames/AppEvents/Variables/New FloatVariable")]
    public class FloatVariable : ScriptableObject
    {

        public float Value { get { return (m_value); } }

        [SerializeField]
        private float m_value;
        [SerializeField, Multiline, Tooltip("Room for your thoughts")]
        private string m_description;


        public void SetValue(float value)
        {
            m_value = value;
        }


        public void SetValue(FloatVariable value)
        {
            m_value = value.m_value;
        }


        public void ApplyChange(float amount)
        {
            m_value += amount;
        }


        public void ApplyChange(FloatVariable amount)
        {
            m_value += amount.m_value;
        }


    }
}
