﻿using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents.Objects
{

    [CreateAssetMenu(fileName = "IntVariable", menuName = "YesYesGames/AppEvents/Variables/New IntVariable")]
    public class IntVariable : ScriptableObject
    {

        public int Value { get { return (m_value); } }

        [SerializeField]
        private int m_value;
        [SerializeField, Multiline, Tooltip("Room for your thoughts")]
        private string m_description;


        public void SetValue(int value)
        {
            m_value = value;
        }


        public void SetValue(IntVariable value)
        {
            m_value = value.m_value;
        }


        public void ApplyChange(int amount)
        {
            m_value += amount;
        }


        public void ApplyChange(IntVariable amount)
        {
            m_value += amount.m_value;
        }


    }
}
