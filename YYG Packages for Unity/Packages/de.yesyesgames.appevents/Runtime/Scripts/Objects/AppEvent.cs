﻿///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;

using YYG.Packages.Unity.AppEvents.Abstracts;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents.Objects
{

    [CreateAssetMenu(fileName = "AppEvent.", menuName = "YesYesGames/AppEvents/New AppEvent")]
    public class AppEvent : ScriptableObject
    {

        public int Listeners => eventListeners.Count;

        private readonly List<EventListenerAbstract> eventListeners = new();

        [HideInInspector]
        public int m_int;
        [HideInInspector]
        public string m_text;
        [HideInInspector]
        public ScriptableObject m_scriptableObject;
        [HideInInspector]
        public bool m_myBool;


        public void Raise()
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised();
        }


        public void Raise(object param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(ScriptableObject param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(int param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(long param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(float param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(string param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(bool param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(GameObject param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(Transform param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        public void Raise(Param param)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(param);
        }


        internal void RegisterListener(EventListenerAbstract listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }


        internal void UnregisterListener(EventListenerAbstract listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }


    }
}
