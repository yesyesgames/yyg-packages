﻿using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents.Objects
{

    [CreateAssetMenu(fileName = "StringVariable", menuName = "YesYesGames/AppEvents/Variables/New StringVariable")]
    public class StringVariable : ScriptableObject
    {

        public string Value { get { return (m_value); } }

        [SerializeField]
        private string m_value;
        [SerializeField, Multiline, Tooltip("Room for your thoughts")]
        private string m_description;


        public void SetValue(string value)
        {
            m_value = value;
        }


        public void SetValue(StringVariable value)
        {
            m_value = value.m_value;
        }


    }
}
