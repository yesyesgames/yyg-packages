﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListenerLong : EventListenerAbstract
    {

        [SerializeField]
        private OnIntEvent m_eventResponse = null;

        [System.Serializable]
        public class OnIntEvent : UnityEvent<long> { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke(default);
        }


        public override void OnEventRaised(object args)
        {
            if (args is long param)
                m_eventResponse.Invoke(param);
        }



    }
}
