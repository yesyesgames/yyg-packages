﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListenerFloat : EventListenerAbstract
    {

        [SerializeField]
        private OnFloatEvent m_eventResponse = null;

        [System.Serializable]
        public class OnFloatEvent : UnityEvent<float> { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke(default);
        }


        public override void OnEventRaised(object args)
        {
            if (args is float param)
                m_eventResponse.Invoke(param);
        }



    }
}
