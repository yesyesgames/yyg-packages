﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListenerString : EventListenerAbstract
    {

        [SerializeField]
        private OnStringEvent m_eventResponse = null;

        [System.Serializable]
        public class OnStringEvent : UnityEvent<string> { }


        public override void OnEventRaised()
        {
            m_eventResponse?.Invoke(default);
        }


        public override void OnEventRaised(object args)
        {
            if (args is string param)
                m_eventResponse?.Invoke(param);
        }



    }
}
