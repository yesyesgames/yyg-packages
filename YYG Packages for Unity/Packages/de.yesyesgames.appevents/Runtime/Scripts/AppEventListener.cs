﻿using UnityEngine;
using UnityEngine.Events;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{

    public class AppEventListener : EventListenerAbstract
    {

        [SerializeField]
        private OnEvent m_eventResponse = null;

        [System.Serializable]
        public class OnEvent : UnityEvent { }


        public override void OnEventRaised()
        {
            m_eventResponse.Invoke();
        }


        public override void OnEventRaised(object args)
        {
            m_eventResponse.Invoke();
        }



    }
}
