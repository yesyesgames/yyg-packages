﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

using YYG.Packages.Unity.AppEvents.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents
{
    public class AppEventListenerObject : EventListenerAbstract
    {

        [SerializeField]
        [FormerlySerializedAs("m_eventResponse")]
        private OnCustomEvent eventResponse = null;

        [System.Serializable]
        public class OnCustomEvent : UnityEvent<object> { }


        public override void OnEventRaised() => eventResponse.Invoke(null);

        public override void OnEventRaised(object args)
        {
            if (args is object param)
            {
                eventResponse.Invoke(param);
            }
        }



    }
}
