﻿using UnityEngine;
using UnityEngine.Serialization;

using YYG.Packages.Unity.AppEvents.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AppEvents.Abstracts
{
    public abstract class EventListenerAbstract : MonoBehaviour
    {

        [SerializeField]
        [FormerlySerializedAs("m_event")]
        private AppEvent appEvent = null;


        private void OnEnable() => appEvent?.RegisterListener(this);
        private void OnDisable() => appEvent?.UnregisterListener(this);


        public abstract void OnEventRaised();
        public abstract void OnEventRaised(object args);


    }
}
