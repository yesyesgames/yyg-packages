using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.XR.ARSubsystems;

namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IAREarthAnchorModule
    {

        void Clear();
        void Remove(TrackableId id);
        Task<EarthAnchorResult> Create(Pose pose);
        Task<EarthAnchorResult> Create(GeoSpatialModel pose);
        Task<EarthAnchorResult> Resolve(GeoSpatialModel pose);

    }
}
