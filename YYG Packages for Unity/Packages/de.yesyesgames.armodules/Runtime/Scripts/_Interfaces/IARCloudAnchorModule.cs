using System.Collections.Generic;

using UnityEngine;

namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IARCloudAnchorModule
    {
        void Host(Object anchor, int lifetime, System.Action<string> resolve, System.Action reject = null);
        void Resolve(string id, System.Action<string, Pose> resolve, System.Action<string> reject = null);
    }
}
