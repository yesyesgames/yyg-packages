namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IAREarthManagerModule
    {

        event System.Action<GeoSpatialModel> OnPoseChanged;

        GeoSpatialModel Data { get; }

        void Begin();
        void Cancel();

    }
}
