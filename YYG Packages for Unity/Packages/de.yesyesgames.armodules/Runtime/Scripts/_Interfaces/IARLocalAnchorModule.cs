using System.Threading.Tasks;

using UnityEngine;

namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IARLocalAnchorModule
    {
        Task<T> Create<T>(T prefab, Pose pose) where T : MonoBehaviour;
    }
}
