///////////////////////////////////////////////////////////////////////////////
using System.Threading;
using System.Threading.Tasks;

#if YYG_USE_ARFOUNDATION_EXTENSIONS
using Google.XR.ARCoreExtensions;
#endif

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IARAnchorManagerModule
    {
        /// <param name="reject">Cloud anchor ID.</param>
        Task Validate(Pose pose, CancellationToken token = default);
        
        /// <param name="resolve"><see cref="ARCloudAnchor"/></param>
        Task<Object> Resolve(string id);
        
        /// <param name="anchor"><see cref="ARAnchor"</param>
        /// <param name="resolve"><see cref="ARCloudAnchor"/> </param>
        /// <param name="reject">Cloud anchor ID.</param>
        Task<Object> Host(Object anchor, int days);
    }
}
