///////////////////////////////////////////////////////////////////////////////
#if YYG_USE_ARFOUNDATION
using UnityEngine.XR.ARFoundation;
#endif
#if YYG_USE_ARFOUNDATION_EXTENSIONS
using Google.XR.ARCoreExtensions;
#endif

using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.ModuleSystem.Abstracts;

using YYG.Packages.Unity.ARModules.Interfaces;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{
    [DisallowMultipleComponent]
    public sealed class ARFoundationAnchorManagerModule : ModuleBase, IARAnchorManagerModule
    {

#if YYG_USE_ARFOUNDATION
        private ARAnchorManager ARAnchorManager;
#endif

#if YYG_USE_ARFOUNDATION
        private void Awake() => ARAnchorManager = FindObjectOfType<ARAnchorManager>();
#endif


        public async Task Validate(Pose pose, CancellationToken token = default)
        {
#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
            var task = Task.Run(() => ARAnchorManager.EstimateFeatureMapQualityForHosting(pose), token);
            try
            {
                while (task.Result != FeatureMapQuality.Good)
                {
                    token.ThrowIfCancellationRequested();
                    await Task.Yield();
                }
            }
            catch (System.Exception e)
            {
                ERR($"{e}");
            }
#else
            await Task.Yield();
#endif
        }


        /// <param name="anchor">ARAnchor</param>
        /// <param name="resolve">ARCloudAnchor</param>
        public async Task<Object> Host(Object anchor, int days)
        {
#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
            if (anchor is ARAnchor ARAnchor)
            {
                var cloudAnchor = ARAnchorManager.HostCloudAnchor(ARAnchor, days);
                while (cloudAnchor.cloudAnchorState == CloudAnchorState.TaskInProgress)
                    await Task.Yield();

                switch (cloudAnchor.cloudAnchorState)
                {
                    default: return (null);
                    case CloudAnchorState.Success: return (cloudAnchor);
                }
            }
            return (null);
#else
            await Task.Yield();
            return null;
#endif
        }


        public async Task<Object> Resolve(string id)
        {
#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
            var anchor = ARAnchorManager.ResolveCloudAnchorId(id);
            while (anchor.cloudAnchorState == CloudAnchorState.TaskInProgress)
                await Task.Yield();

            switch (anchor.cloudAnchorState)
            {
                default: return (null);
                case CloudAnchorState.Success: return (anchor); ;
            }
#else
            await Task.Yield();
            return null;
#endif
        }


    }
}
