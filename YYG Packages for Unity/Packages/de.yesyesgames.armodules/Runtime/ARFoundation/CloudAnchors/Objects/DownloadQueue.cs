using UnityEngine;

namespace YYG.Packages.Unity.ARModules.Objects
{
    public class DownloadQueue
    {

        /// <summary>
        /// Cloud anchor ID.
        /// </summary>
        internal string ID { get; set; }

        /// <summary>
        /// Data was uploaded successfully.
        /// </summary>
        internal System.Action<string, Pose> OnResolve { get; set; }

        /// <summary>
        /// Data upload failed.
        /// </summary>
        internal System.Action<string> OnReject { get; set; }

    }
}
