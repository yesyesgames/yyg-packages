///////////////////////////////////////////////////////////////////////////////
#if YYG_USE_ARFOUNDATION
using UnityEngine.XR.ARFoundation;
#else
using UnityEngine;
#endif
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules.Objects
{
    public class UploadQueue
    {

        internal int Days { get; set; } = 1;
#if YYG_USE_ARFOUNDATION
        internal ARAnchor Anchor { get; set; }
#else
        internal Object Anchor { get; set; }
#endif

        /// <summary>
        /// Data was uploaded successfully.
        /// </summary>
        internal System.Action<string> OnResolve { get; set; }
        /// <summary>
        /// Data upload failed.
        /// </summary>
        internal System.Action OnReject { get; set; }

    }
}
