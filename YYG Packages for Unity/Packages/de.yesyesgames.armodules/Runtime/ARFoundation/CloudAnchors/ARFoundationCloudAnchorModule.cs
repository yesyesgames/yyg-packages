#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
using UnityEngine.XR.ARFoundation;

using Google.XR.ARCoreExtensions;
#endif

using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.ModuleSystem.Abstracts;

using YYG.Packages.Unity.ARModules.Interfaces;
using YYG.Packages.Unity.ARModules.Objects;

namespace YYG.Packages.Unity.ARModules.AR
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ARFoundationAnchorManagerModule))]
    public sealed class ARFoundationCloudAnchorModule : ModuleBase, IARCloudAnchorModule
    {

        private bool m_uploadOngoing;
        private bool m_downloadOngoing;

        private Stack<UploadQueue> m_uploadQueue;
        private Stack<DownloadQueue> m_downloadQueue;


        public override void OnKill()
        {
            m_uploadQueue = new Stack<UploadQueue>();
            m_downloadQueue = new Stack<DownloadQueue>();
            base.OnKill();
        }


        /// <param name="anchor">ARAnchor</param>
        public void Host(Object anchor, int lifetime, System.Action<string> resolve, System.Action reject = null)
        {
            DBG($"ARFoundationCloudAnchorModule::Upload({anchor}, {lifetime})");
#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
            if (anchor is ARAnchor ARAnchor)
            {
                // Add the new data to the queue.
                m_uploadQueue.Push(new UploadQueue()
                {
                    Days = 1,
                    Anchor = ARAnchor,
                    OnResolve = resolve,
                    OnReject = reject,
                });

                // Only start when not running.
                if (m_uploadQueue.Count == 0)
                    Upload();
            }
#endif
        }


        public void Resolve(string id, System.Action<string, Pose> resolve, System.Action<string> reject = null)
        {
            DBG($"ARCloudAnchorModule::Download()");
            // Add the new data to the queue.
            m_downloadQueue.Push(new DownloadQueue()
            {
                ID = id,
                OnReject = reject,
                OnResolve = resolve,
            });

            // Only start when not running.
            if (m_downloadOngoing == false)
                Download();
        }


        private async void Upload()
        {
            if (m_uploadQueue.Count == 0)
                return;

            var data = m_uploadQueue.Pop();
#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
            DBG($"ARFoundationCloudAnchorModule::StartUploading(): Uploading anchor with ID {data.Anchor.trackableId} ({m_uploadQueue.Count - 1} left)...");
            var result = await Modules.Get<IARAnchorManagerModule>().Host(data.Anchor, data.Days);
            if (result is ARCloudAnchor anchor)
            {
                if (string.IsNullOrEmpty(anchor.cloudAnchorId))
                {
                    ERR($"ARFoundationCloudAnchorModule::StartUploading(): Upload failed.");
                    data.OnReject?.Invoke();
                }
                else
                {
                    DBG($"ARFoundationCloudAnchorModule::StartUploading(): Upload succeeded.");
                    data.OnResolve?.Invoke(anchor.cloudAnchorId);
                }
            }
            Upload();
#else
            await Task.Yield();
#endif
        }


        private async void Download()
        {
            if (m_downloadQueue.Count == 0)
                return;

            var data = m_downloadQueue.Pop();
#if YYG_USE_ARFOUNDATION && YYG_USE_ARFOUNDATION_EXTENSIONS
            DBG($"ARFoundationCloudAnchorModule::StartDownloading(): Downloading anchor ({m_downloadQueue.Count - 1} left)...");
            var result = await Modules.Get<IARAnchorManagerModule>().Resolve(data.ID);
            if (result is ARCloudAnchor anchor)
            {
                if (anchor == null)
                {
                    ERR($"ARFoundationCloudAnchorModule::StartDownloading(): Download failed.");
                    data.OnReject?.Invoke(data.ID);
                }
                else
                {
                    DBG($"ARFoundationCloudAnchorModule::StartDownloading(): Download succeeded.");
                    data.OnResolve?.Invoke(anchor.cloudAnchorId, anchor.pose);
                }
            }
#else
            await Task.Yield();
#endif
            Download();
        }


    }
}
