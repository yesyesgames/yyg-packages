﻿/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Threading.Tasks;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using YYG.Packages.Unity.ModuleSystem.Abstracts;

using YYG.Packages.Unity.ARModules.Interfaces;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{

    public sealed class AREarthAnchorModule : ModuleBase, IAREarthAnchorModule
    {

        private AREarthManager _earthManager;
        private ARAnchorManager _anchorManager;

        private List<ARGeospatialAnchor> _anchors;


        private void Awake()
        {
            _anchors = new();
            _earthManager = FindObjectOfType<AREarthManager>(true);
            _anchorManager = FindObjectOfType<ARAnchorManager>(true);
        }


        private void OnEnable()
        {
            //
        }


        public override void OnFocus(bool hasFocus)
        {
            base.OnFocus(hasFocus);
            if (hasFocus)
            {
                _anchorManager.anchorsChanged += AnchorsChanged;
            }
            else
            {
                _anchorManager.anchorsChanged -= AnchorsChanged;
            }
        }


        public async Task<EarthAnchorResult> Create(Pose pose)
        {
            var geoPose = _earthManager.Convert(pose);
            var anchor = _anchorManager.AddAnchor(geoPose.Latitude, geoPose.Longitude, geoPose.Altitude, geoPose.EunRotation);
            return await Create(anchor);
        }


        public async Task<EarthAnchorResult> Create(GeoSpatialModel pose)
        {
            var anchor = _anchorManager.AddAnchor(pose.Lat, pose.Lon, pose.Altitude, pose.EunRotation);
            return await Create(anchor);
        }


        public async Task<EarthAnchorResult> Resolve(GeoSpatialModel pose)
        {
            DBG($"AREarthAnchorModule::Resolve({pose})");

            ARGeospatialAnchor anchor = null;

            switch (pose.Type)
            {
                default:
                    DBG($"AREarthAnchorModule::Resolve(): Anchor type '{pose.Type}' note yet supported.");
                    break;
                case AnchorTypes.GeoSpatial:
                    var result = await Create(pose);
                    anchor = result.Anchor;
                    break;
                case AnchorTypes.Terrain:
                    anchor = await ResolveTerrain(pose);
                    break;
            }

            if (anchor is not null)
            {
                DBG($"AREarthAnchorModule::Resolve(): Anchor resolved.");
                _anchors.Add(anchor);
                return new EarthAnchorResult(true, anchor);
            }

            return new EarthAnchorResult();
        }


        public void Clear()
        {
            foreach (var value in _anchors)
            {
                Destroy(value.gameObject);
            }

            _anchors.Clear();
        }


        public void Remove(TrackableId id)
        {
            var anchor = _anchors.Find(x => x.trackableId == id);
            if (anchor == null)
            {
                ERR($"Anchor with ID '{id}' not found.");
                return;
            }

            Destroy(anchor.gameObject);
        }


        private void AnchorsChanged(ARAnchorsChangedEventArgs args)
        {
            // WRN($"Added: {args.added.Count} Removed: {args.removed.Count} Updated: {args.updated.Count}");
        }


        private async Task<ARGeospatialAnchor> ResolveTerrain(GeoSpatialModel pose)
        {
            var promise = _anchorManager.ResolveAnchorOnTerrainAsync(pose.Lat, pose.Lon, pose.Altitude, pose.EunRotation);
            while (promise.State == PromiseState.Pending)
            {
                DBG($"AREarthAnchorModule::Resolve(): ...");
                await Task.Yield();
            }

            var result = promise.Result;
            if (result.TerrainAnchorState == TerrainAnchorState.Success && result.Anchor != null)
            {
                return result.Anchor;
            }

            ERR($"AREarthAnchorModule::ResolveTerrain(): Error with a state of '{promise.State}'.");
            return null;
        }


        private async Task<EarthAnchorResult> Create(ARGeospatialAnchor anchor)
        {
            if (await Validate(anchor))
            {
                _anchors.Add(anchor);
                return new EarthAnchorResult(true, anchor);
            }

            return new EarthAnchorResult();
        }


        private async Task<bool> Validate(ARGeospatialAnchor anchor)
        {
            DBG($"AREarthAnchorModule::Validate({anchor})");
            while (anchor.trackingState == TrackingState.None)
            {
                DBG($"AREarthAnchorModule::Validate(): ...");
                await Task.Yield();
            }
            return true;
        }



    }
}
