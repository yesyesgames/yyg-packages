﻿using Newtonsoft.Json;

using UnityEngine;

[System.Serializable]
public enum AnchorTypes
{
    GeoSpatial = 0,
    Rooftop = 1,
    Terrain = 2,
}

[System.Serializable]
public struct GeoSpatialModel
{

    [JsonRequired]
    public double Lat;
    [JsonRequired]
    public double Lon;
    [JsonRequired]
    public double Altitude;
    [JsonRequired]
    public Quaternion EunRotation;
    [JsonRequired]
    public double HeadingAccuracy;
    [JsonRequired]
    public double HorizontalAccuracy;
    [JsonRequired]
    public bool VPS;
    [JsonRequired]
    public AnchorTypes Type;

    public void SetVPS(bool isAvailable) => VPS = isAvailable;

}
