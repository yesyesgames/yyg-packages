﻿/////////////////////////////////////////////////////////////////////////////////
using System.Collections;

using Google.XR.ARCoreExtensions;

using UnityEngine;

using YYG.Packages.Unity.ModuleSystem.Abstracts;

using YYG.Packages.Unity.ARModules.Interfaces;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{
    public sealed class AREarthManagerModule : ModuleBase, IAREarthManagerModule
    {

        public event System.Action<GeoSpatialModel> OnPoseChanged;

        public GeoSpatialModel Data { get; private set; }

        private GeospatialPose _geoPose;
        private AREarthManager _earthManager;
        private ARCoreExtensions _coreExtensions;

        private VpsAvailabilityPromise _vpsResult;


        private void Awake()
        {
            Data = new();
            _earthManager = FindObjectOfType<AREarthManager>(true);
            _coreExtensions = FindObjectOfType<ARCoreExtensions>(true);
        }


        private void OnEnable()
        {
            //
        }


        public override void OnFocus(bool hasFocus)
        {
            base.OnFocus(hasFocus);
            if (hasFocus)
            {
                StartCoroutine(GetVPS());
            }
            else
            {
                StopCoroutine(GetVPS());
            }
        }


        public void Begin()
        {
            Cancel();
            StartCoroutine(GetPose());
        }


        public void Cancel()
        {
            StopAllCoroutines();
        }


        private bool Ready()
        {
            var state =
            _earthManager == null ||
            _coreExtensions == null ||
            _coreExtensions.enabled == false ||
            _coreExtensions.gameObject.activeSelf == false;
            return !state;
        }


        private IEnumerator GetVPS()
        {
            DBG($"AREarthManagerModule::GetVPS():");
            var delay = 3;
            while (true)
            {
                if (Ready() == false)
                {
                    ERR($"AREarthManagerModule::GetVPS(): Not ready.");
                    yield return new WaitForSeconds(delay);
                }

                _vpsResult?.Cancel();
                _vpsResult = AREarthManager.CheckVpsAvailabilityAsync(Data.Lat, Data.Lon);

                DBG($"AREarthManagerModule::GetVPS(): Resolving state...");
                while (_vpsResult.State == PromiseState.Pending)
                {
                    DBG($"AREarthManagerModule::GetVPS(): ...");
                    yield return new WaitForSeconds(delay);
                }

                DBG($"AREarthManagerModule::GetVPS(): State results in '{_vpsResult.State}'.");

                Data.SetVPS(_vpsResult.Result == VpsAvailability.Available);
                yield return new WaitForSeconds(delay);
            }
        }


        private IEnumerator GetPose()
        {
            DBG($"AREarthManagerModule::GetPose()");
            while (Ready() == false)
            {
                WRN($"AREarthManagerModule::GetPose(): Missing requirements, retry in 3 seconds...");
                yield return new WaitForSeconds(3);
            }

            while (Ready())
            {
                var value = _earthManager.CameraGeospatialPose;
                if (_geoPose.Equals(value) == false)
                {
                    DBG($"AREarthManagerModule::GetPose(): Pose received at: lat: {value.Latitude}, lon: {value.Longitude}.");
                    _geoPose = value;

                    Data = new()
                    {
                        Lat = value.Latitude,
                        Lon = value.Longitude,
                        Altitude = value.Altitude,
                        EunRotation = value.EunRotation,
                        HeadingAccuracy = value.OrientationYawAccuracy,
                        HorizontalAccuracy = value.HorizontalAccuracy
                    };

                    OnPoseChanged?.Invoke(Data);
                }

                // TODO: Is it okay to pull the pose that often?
                yield return null;
            }
        }

    }
}
