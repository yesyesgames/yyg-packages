﻿/////////////////////////////////////////////////////////////////////////////////
using Google.XR.ARCoreExtensions;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{
    public class EarthAnchorResult
    {

        public bool Success = false;
        public ARGeospatialAnchor Anchor = null;

        public EarthAnchorResult() { }
        public EarthAnchorResult(bool success, ARGeospatialAnchor anchor)
        {
            Success = success;
            Anchor = anchor;
        }


    }
}
