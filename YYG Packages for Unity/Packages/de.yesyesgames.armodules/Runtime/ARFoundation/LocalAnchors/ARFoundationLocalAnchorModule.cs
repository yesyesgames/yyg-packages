///////////////////////////////////////////////////////////////////////////////
#if YYG_USE_ARFOUNDATION
using UnityEngine.XR.ARFoundation;
#endif

using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.ModuleSystem.Abstracts;

using YYG.Packages.Unity.ARModules.Interfaces;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{
    [DisallowMultipleComponent]
    public sealed class ARFoundationLocalAnchorModule : ModuleBase, IARLocalAnchorModule
    {

        public async Task<T> Create<T>(T prefab, Pose pose) where T : MonoBehaviour
        {
            DBG($"ARFoundationLocalAnchorModule::Create_Async({prefab}, {pose})");
            T anchor = CreateAnchor<T>(prefab, pose);

            DBG($"ARFoundationLocalAnchorModule::Create_Async(): Waiting for local anchor ...");
#if YYG_USE_ARFOUNDATION
            while (anchor.GetComponent<ARAnchor>().pending == true)
                await Task.Yield();
#endif
            DBG($"ARFoundationLocalAnchorModule::Create_Async(): Local anchor is ready.");
            await Task.Yield();
            return anchor;
        }


        private T CreateAnchor<T>(T prefab, Pose pose) where T : MonoBehaviour
        {
            DBG($"ARFoundationLocalAnchorModule::Create({prefab}, {pose})");
            T anchor = Instantiate<T>(prefab);
            anchor.transform.SetPositionAndRotation(pose.position, pose.rotation);
#if YYG_USE_ARFOUNDATION
            anchor.gameObject.AddComponent<ARAnchor>();
#endif
            return anchor;
        }


    }
}
