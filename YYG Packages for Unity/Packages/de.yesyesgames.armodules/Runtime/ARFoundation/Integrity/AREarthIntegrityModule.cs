﻿/////////////////////////////////////////////////////////////////////////////////
using System.Collections;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.XR.ARSubsystems;

using YYG.Packages.Unity.ARModules.Interfaces;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{
    public sealed class AREarthIntegrityModule : ModuleBase, IAREarthIntegrityModule
    {

        public event System.Action OnTrackingChanged;
        public event System.Action OnIntegrityChanged;

        public bool IsTracking { get; private set; }
        public bool IsAvailable { get; private set; }

        private AREarthManager _earthManager;
        private ARCoreExtensions _coreExtensions;


        private void OnEnable()
        {
            _earthManager = FindObjectOfType<AREarthManager>(true);
            _coreExtensions = FindObjectOfType<ARCoreExtensions>(true);
        }


        public void Check()
        {
            StopAllCoroutines();
            StartCoroutine(CheckTracking());
            StartCoroutine(CheckIntegrity());
        }


        public void Cancel()
        {
            StopAllCoroutines();
        }


        private bool Ready()
        {
            var state =
            _earthManager == null ||
            _coreExtensions == null ||
            _coreExtensions.enabled == false ||
            _coreExtensions.gameObject.activeSelf == false;
            return !state;
        }


        private IEnumerator CheckIntegrity()
        {
            DBG($"AREarthIntegrityModule::CheckIntegrity()");

            while (Ready() == false)
            {
                Unavailable("Missing requirements.");
                yield return new WaitForSeconds(3);
            }

            var retries = 3;
            yield return new WaitForEndOfFrame();
            while (true)
            {
                var result = _earthManager.IsGeospatialModeSupported(GeospatialMode.Enabled);
                DBG($"AREarthIntegrityModule::CheckIntegrity(): {result}");

                switch (result)
                {
                    default:
                        Unavailable(result.ToString());
                        break;
                    case FeatureSupported.Supported:
                        Available();
                        break;
                }

                if (IsAvailable || retries <= 0)
                {
                    break;
                }

                retries--;
                yield return new WaitForSeconds(3);
            }
        }


        private IEnumerator CheckTracking()
        {
            while (Ready() == false || IsAvailable == false)
            {
                yield return new WaitForSeconds(3);
            }

            var lastState = TrackingState.None;
            while (true)
            {
                var state = _earthManager.EarthTrackingState;
                if (lastState.Equals(state) == false)
                {
                    lastState = state;
                    IsTracking = state == TrackingState.Tracking;
                    OnTrackingChanged?.Invoke();
                }
                yield return new WaitForSeconds(3);
            }
        }


        private void Available()
        {
            DBG($"AREarthIntegrityModule::Available()");
            IsAvailable = true;
            Broadcast();
        }


        private void Unavailable(string value)
        {
            DBG($"AREarthIntegrityModule::Unavailable({value})");
            IsAvailable = false;
            Broadcast();
        }


        private void Broadcast() => OnIntegrityChanged?.Invoke();


    }
}
