﻿namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IARFoundationIntegrityModule
    {

        event System.Action OnIntegrityChanged;

        bool IsAvailable { get; }

        void Check();

    }
}
