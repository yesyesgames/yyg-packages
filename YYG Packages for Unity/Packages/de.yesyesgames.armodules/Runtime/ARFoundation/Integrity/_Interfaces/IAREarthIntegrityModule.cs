﻿namespace YYG.Packages.Unity.ARModules.Interfaces
{
    public interface IAREarthIntegrityModule
    {

        event System.Action OnTrackingChanged;
        event System.Action OnIntegrityChanged;

        bool IsTracking { get; }
        bool IsAvailable { get; }

        void Check();
        void Cancel();

    }
}
