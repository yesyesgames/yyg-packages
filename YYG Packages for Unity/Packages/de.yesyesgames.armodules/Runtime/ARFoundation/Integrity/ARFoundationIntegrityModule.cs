﻿/////////////////////////////////////////////////////////////////////////////////
using System.Collections;

using UnityEngine;

using YYG.Packages.Unity.ARModules.Interfaces;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.ModuleSystem.Abstracts;

#if YYG_USE_ARFOUNDATION
using UnityEngine.XR.ARFoundation;
#endif
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ARModules
{
    /// <summary>
    /// The idea behind this namespace is to provide a way to code another integrity not based on ARFoundation.
    /// </summary>
    public sealed class ARFoundationIntegrityModule : ModuleBase, IARFoundationIntegrityModule
    {

        #region DECLARES
        // ~~~~~~~~~~~~~~~~~~~~~~~~~


        /// <summary>
        /// Returns <code>true</code> if the system is capable to run AR-features.
        /// </summary>
        public event System.Action OnIntegrityChanged;

        public bool IsAvailable { get; private set; }

        [Topic("ARFoundationIntegrityModule")]
        [Header("Settings")]
        [SerializeField]
        private bool matchFrameRate = true;


        #endregion



        #region INHERITANCE_METHODS
        // ~~~~~~~~~~~~~~~~~~~~~~~~~


        public void Check()
        {
            DBG($"ARFoundationIntegrityModule::Check()");
            StopAllCoroutines();
            StartCoroutine(CheckIntegrity());
        }


        #endregion



        #region PRIVATE_METHODS
        // ~~~~~~~~~~~~~~~~~~~~~~~~~


        private void ARAvailable()
        {
            DBG($"ARIntegrityModule::ARAvailable()");
            IsAvailable = true;
            OnIntegrityChanged?.Invoke();

#if YYG_USE_ARFOUNDATION
            if (FindObjectOfType<ARSession>() == null)
                return;

            FindObjectOfType<ARSession>().matchFrameRateRequested = matchFrameRate;
#endif

        }

        private void ARUnavailable()
        {
            DBG($"ARIntegrityModule::ARDisabled()");
            IsAvailable = false;
            OnIntegrityChanged?.Invoke();
        }


        private IEnumerator CheckIntegrity()
        {
            DBG($"ARFoundationIntegrityModule::CheckIntegrity()");
            // Wait to for potential initialization of the AR system.
            yield return new WaitForEndOfFrame();

            DBG($"ARFoundationIntegrityModule::CheckIntegrity(): Waiting for result...");
#if YYG_USE_ARFOUNDATION
            if (ARSession.state == ARSessionState.None ||
                ARSession.state == ARSessionState.Installing ||
                ARSession.state == ARSessionState.NeedsInstall ||
                ARSession.state == ARSessionState.CheckingAvailability)
            {
                yield return ARSession.CheckAvailability();
            }

            DBG($"ARIntegrityModule::CheckIntegrity(): Integrity result is '{ARSession.state}'.");
            switch (ARSession.state)
            {
                default:
                    ARUnavailable();
                    break;

                case ARSessionState.Ready:
                case ARSessionState.SessionTracking:
                case ARSessionState.SessionInitializing:
                    ARAvailable();
                    break;
            }
#else
            Debug.Log($"ARFoundationIntegrityModule:CheckIntegrity(): Add 'YYG_USE_ARFOUNDATION' to use this module.");
            ARUnavailable();
#endif
        }


        #endregion



    }
}
