/////////////////////////////////////////////////////////////////////////////////
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.AddressableModules.Interfaces;
using YYG.Packages.Unity.AddressableModules.Objects;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules
{
    [RequireComponent(typeof(AddressableCreatorBasic))]
    public class AddressableCreatorAdvanced : ModuleBase, IAddressableCreatorAdvanced
    {

        [Topic("AddressableCreatorAdvanced")]
        [Header("Settings")]
        [SerializeField]
        private AddressableCollectionObject addressableCollection;


        public async Task<Object> Create(Param label) => await Create<Object>(label);

        public async Task<T> Create<T>(Param label) where T : Object
        {
            if (GetSpecs(label, out var specs))
            {
                return await Modules.Get<IAddressableCreatorBasic>().Create<T>(specs.reference);
            }
            return null;
        }


        private bool GetSpecs(Param label, out AddressableSpecificationObject specs)
        {
            specs = addressableCollection.specifications.Find(x => x.label == label);
            if (specs == null)
            {
                DBG($"AddressableCreatorAdvanced::GetSpecs(): Label '{label}' is NULL or invalid.");
            }
            return specs != null;
        }



    }
}
