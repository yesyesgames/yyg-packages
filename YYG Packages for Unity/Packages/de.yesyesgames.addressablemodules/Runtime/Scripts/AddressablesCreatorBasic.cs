/////////////////////////////////////////////////////////////////////////////////
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;

using YYG.Packages.Unity.AddressableModules.Interfaces;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules
{
    public class AddressableCreatorBasic : ModuleBase, IAddressableCreatorBasic
    {

        public async Task<Object> Create(AssetReference asset) => await LoadAndInstantiate<Object>(asset);
        public async Task<T> Create<T>(AssetReference asset) where T : Object => await LoadAndInstantiate<T>(asset);


        private async Task<T> LoadAndInstantiate<T>(AssetReference asset) where T : Object
        {
            DBG($"AddressableCreatorBasic::LoadAndInstantiate({asset}, )");
            if (asset.IsValid())
            {
                DBG($"AddressableCreatorBasic::LoadAndInstantiate(): {asset} is valid waiting for done state.");
                // While requesting this asset again, make sure it's ready and not still loading.
                while (asset.IsDone == false)
                {
                    DBG($"AddressableCreatorBasic::LoadAndInstantiate(): ...");
                    await Task.Yield();
                }

                DBG($"AddressableCreatorBasic::LoadAndInstantiate(): {asset} is done.");
                var result = asset.OperationHandle.Result as T;
                return Instantiate(result);
            }

            DBG($"AddressableCreatorBasic::LoadAndInstantiate(): {asset} is not yet valid and needs to be loaded.");
            try
            {
                var result = await asset.LoadAssetAsync<T>().Task;
                return Instantiate(result);
            }
            catch (System.Exception e)
            {
                ERR($"AddressableCreatorBasic::LoadAndInstantiate(): {e}");
                return null;
            }
        }

    }
}
