/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

using YYG.Packages.Unity.AddressableModules.Interfaces;
using YYG.Packages.Unity.AddressableModules.Objects;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules
{
    public class AddressableResources : ModuleBase, IAddressableResources
    {

        public event System.Action OnComplete;
        public event System.Action<long, long> OnProgress;

        public List<IList<IResourceLocation>> Locations => _locations;

        [Header("AddressableResources")]
        [Header("Settings")]
        [SerializeField]
        private List<AddressableHolderBaseObject> addressableHolders = default;

        [Space]
        [SerializeField]
        private bool wipeCache = false;

        private List<IList<IResourceLocation>> _locations;


        public override void OnInit()
        {
            base.OnInit();
            if (wipeCache)
            {
                Caching.ClearCache();
                Addressables.ClearResourceLocators();
            }
        }


        public async void Load()
        {
            _locations = new();
            foreach (var holder in addressableHolders)
            {
                var l = await Extract(holder);
                _locations.Add(l);
            }
            OnComplete?.Invoke();
        }


        private async Task<IList<IResourceLocation>> Extract(AddressableHolderBaseObject holder)
        {
            AsyncOperationHandle<IList<IResourceLocation>> asyncOperation;

            if (holder is AddressableHolderForReferencesObject referenceHolder)
            {
                asyncOperation = Addressables.LoadResourceLocationsAsync(referenceHolder.references, Addressables.MergeMode.Union);
            }
            else if (holder is AddressableHolderForLabelsObject labelHolder)
            {
                asyncOperation = Addressables.LoadResourceLocationsAsync(labelHolder.labels, Addressables.MergeMode.Union);
            }
            else
            {
                ERR($"ERR1");
                return default;
            }

            if (asyncOperation.IsValid() == false)
            {
                ERR($"ERR2");
                return default;
            }

            var status = asyncOperation.GetDownloadStatus();
            if (asyncOperation.IsDone)
            {
                DBG($"AddressableResources::Extract(): Already done.");
            }
            else
            {
                while (asyncOperation.IsDone == false)
                {
                    DBG($"AddressableResources::Extract(): {status.DownloadedBytes}/{status.TotalBytes}.");
                    OnProgress?.Invoke(status.DownloadedBytes, status.TotalBytes);
                    await Task.Yield();
                }
            }

            DBG($"AddressableResources::Extract(): Done with status of: '{asyncOperation.Status}' & '{asyncOperation.Result.Count}' locations.");

            var result = asyncOperation.Result;
            Addressables.Release(asyncOperation);

            return result;
        }


    }
}
