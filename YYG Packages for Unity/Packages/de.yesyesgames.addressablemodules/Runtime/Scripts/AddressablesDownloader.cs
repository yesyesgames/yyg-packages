/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

using YYG.Packages.Unity.AddressableModules.Interfaces;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules
{
    /// <summary>
    /// Will download assets async from download location.
    /// </summary>
    public class AddressableDownloader : ModuleBase, IAddressableDownloader
    {

        public event System.Action OnComplete;
        public event System.Action<long, long> OnProgress;

        [SerializeField]
        private bool simulateLatency = false;


        public async void Load(IList<IResourceLocation> locations)
        {
            DBG($"AddressableDownloader::Load({locations})");
            if (IsEnabled == false)
                return;

#if UNITY_EDITOR == false
            simulateLatency = false;
#endif

            await Download(locations);
            OnComplete?.Invoke();
        }


        private async Task Download(IList<IResourceLocation> locations)
        {
            DBG($"AddressableDownloader::Download({locations})");
            var asyncOperation = Addressables.DownloadDependenciesAsync(locations);

            if (asyncOperation.IsValid() == false)
            {
                ERR($"AddressableDownloader::Download(): AsyncOperationException");
                return;
            }

            while (asyncOperation.IsDone == false)
            {
                var status = asyncOperation.GetDownloadStatus();
                DBG($"AddressableDownloader::Download(): {status.Percent}% - [{status.DownloadedBytes}/{status.TotalBytes} Bytes].");
                OnProgress?.Invoke(status.DownloadedBytes, status.TotalBytes);

                if (simulateLatency)
                {
                    await Task.Delay(Random.Range(100, 2000));
                }
                else
                {
                    await Task.Yield();
                }
            }

            DBG($"AddressableDownloader::Download(): Done with status of: '{asyncOperation.Status}' & '{asyncOperation.Result}' locations.");

            Addressables.Release(asyncOperation);
        }


    }
}
