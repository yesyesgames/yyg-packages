/////////////////////////////////////////////////////////////////////////////////
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Interfaces
{
    public interface IAddressableCreatorBasic
    {
        public Task<Object> Create(AssetReference asset);
        public Task<T> Create<T>(AssetReference asset) where T : Object;
    }
}
