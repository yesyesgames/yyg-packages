/////////////////////////////////////////////////////////////////////////////////
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.Core.Objects;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Interfaces
{
    public interface IAddressableCreatorAdvanced
    {
        public Task<Object> Create(Param label);
        public Task<T> Create<T>(Param label) where T : Object;
    }
}
