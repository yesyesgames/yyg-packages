/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine.ResourceManagement.ResourceLocations;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Interfaces
{
    public interface IAddressableDownloader
    {
        event System.Action OnComplete;
        event System.Action<long, long> OnProgress;

        void Load(IList<IResourceLocation> locations);
    }
}
