/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine.ResourceManagement.ResourceLocations;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Interfaces
{
    public interface IAddressablesPreloader
    {
        event System.Action OnComplete;
        /// <summary>
        /// Loading progress.
        /// </summary>
        /// <remarks>First argument [string] only valid for single loading (AllAtOnce must be false).</remarks>
        event System.Action<string, long, long> OnProgress;

        void Load(IList<IResourceLocation> locations);
    }
}
