/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine.ResourceManagement.ResourceLocations;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Interfaces
{
    public interface IAddressableResources
    {
        event System.Action OnComplete;
        event System.Action<long, long> OnProgress;

        List<IList<IResourceLocation>> Locations { get; }

        void Load();
    }
}
