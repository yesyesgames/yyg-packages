/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.AddressableAssets;

using YYG.Packages.Unity.Core.Objects;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Objects
{
    [CreateAssetMenu(fileName = "AddressablesSpecification.", menuName = "BPO Packages for Unity/AddressablesModules/New AddressablesSpecification")]
    public class AddressableSpecificationObject : ScriptableObject
    {
        public Param label;
        public AssetReference reference;
    }
}
