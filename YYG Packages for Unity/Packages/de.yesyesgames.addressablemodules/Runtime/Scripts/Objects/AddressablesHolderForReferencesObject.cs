/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Objects
{
    [CreateAssetMenu(fileName = "AddressablesHolderForReferences.", menuName = "BPO Packages for Unity/AddressablesModules/New AddressablesHolderForReferences")]
    public class AddressableHolderForReferencesObject : AddressableHolderBaseObject
    {
        public List<AssetReference> references = default;
    }
}
