/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Objects
{
    [CreateAssetMenu(fileName = "AddressablesCollection.", menuName = "BPO Packages for Unity/AddressablesModules/New AddressablesCollection")]
    public class AddressableCollectionObject : ScriptableObject
    {
        public List<AddressableSpecificationObject> specifications;
    }
}
