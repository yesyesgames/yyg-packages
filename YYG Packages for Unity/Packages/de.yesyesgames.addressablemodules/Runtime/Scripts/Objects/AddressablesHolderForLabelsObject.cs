/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules.Objects
{
    [CreateAssetMenu(fileName = "AddressablesHolderForLabels.", menuName = "BPO Packages for Unity/AddressablesModules/New AddressablesHolderForLabels")]
    public class AddressableHolderForLabelsObject : AddressableHolderBaseObject
    {
        public List<AssetLabelReference> labels = default;
    }
}
