/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

using YYG.Packages.Unity.AddressableModules.Interfaces;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.AddressableModules
{
    /// <summary>
    /// Will load asset async in memory without instantiating.
    /// </summary>
    public class AddressablesPreloader : ModuleBase, IAddressablesPreloader
    {

        public event System.Action OnComplete;
        public event System.Action<string, long, long> OnProgress;

        [Topic("AddressablesPreloader")]
        [Header("Settings")]
        [SerializeField]
        [Tooltip("Loads all assets at once or individual ones. Useful for loading screens which should output asset names.")]
        private bool allAtOnce = true;


        public async void Load(IList<IResourceLocation> locations)
        {
            if (allAtOnce)
            {
                await PreloadAll(locations);
            }
            else
            {
                await PreloadSingle(locations);
            }
            OnComplete?.Invoke();
        }


        private async Task PreloadAll(IList<IResourceLocation> locations)
        {
            var asyncOperation = Addressables.LoadAssetsAsync<Object>(locations, null);

            if (asyncOperation.IsValid() == false)
            {
                ERR($"ERR1");
                return;
            }

            var status = asyncOperation.GetDownloadStatus();
            while (asyncOperation.IsDone == false)
            {
                DBG($"AddressablePreloader::PreloadAll() {status.DownloadedBytes}/{status.TotalBytes}.");
                OnProgress?.Invoke("AllAtOnce", status.DownloadedBytes, status.TotalBytes);
                await Task.Yield();
            }

            DBG($"AddressablePreloader::PreloadAll(): Done with status of: '{asyncOperation.Status}' & '{asyncOperation.Result.Count}' locations.");

            Addressables.Release(asyncOperation);
        }


        private async Task PreloadSingle(IList<IResourceLocation> locations)
        {
            foreach (var loca in locations)
            {
                var asyncOperation = Addressables.LoadAssetAsync<Object>(loca);

                if (asyncOperation.IsValid() == false)
                {
                    ERR($"ERR1");
                    return;
                }

                var status = asyncOperation.GetDownloadStatus();
                while (asyncOperation.IsDone == false)
                {
                    DBG($"AddressablePreloader::PreloadSingle(): {loca.PrimaryKey} - {status.DownloadedBytes}/{status.TotalBytes}.");
                    OnProgress?.Invoke(loca.PrimaryKey, status.DownloadedBytes, status.TotalBytes);
                    await Task.Yield();
                }

                DBG($"AddressablePreloader::PreloadSingle(): Done with status of: '{asyncOperation.Status}' & '{asyncOperation.Result}' locations.");

                Addressables.Release(asyncOperation);
            }

        }



    }
}
