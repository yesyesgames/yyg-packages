///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Interfaces;

using static YYG.Packages.Unity.Core.Statics.CustomExceptions;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem
{
    [DisallowMultipleComponent]
    public sealed class ModuleManager : MonoBehaviour, IModuleManager, IModuleManagerInternal
    {

        #region DECLARES


        List<IModuleBase> IModuleManagerInternal.Modules => _modules;

        [Topic("ModuleManager")]
        [Header("Settings")]
        [SerializeField]
        private DebugLevel debugLevel = DebugLevel.Warnings | DebugLevel.Errors;

        [SerializeField]
        private DebugLevelInheritance debugInheritance = DebugLevelInheritance.Inherited;

        private List<IModuleBase> _modules;


        #endregion



        #region INHERITANCE_METHODS


        void IModuleManagerInternal.OnInitInternal()
        {
            FindAllModules();
            foreach (var module in _modules)
            {
                ((IModuleBaseInternal)module).InternalInit(this, debugLevel, debugInheritance);
                module.OnInit();
            }
        }


        void IModuleManagerInternal.OnKillInternal()
        {
            foreach (var module in _modules)
                module.OnKill();
        }


        void IModuleManagerInternal.OnFocusInternal(bool hasFocus)
        {
            foreach (var module in _modules)
                module.OnFocus(hasFocus);
        }


        #endregion



        #region PUBLIC_METHODS


        public T Get<T>()
        {
            if (_modules == null)
            {
                Debug.LogError($"The Module is NULL. Please note: Modules are available after Awake via ComponentBase or OnInit via ActivityBase.");
                return default;
            }

            IModuleBase module = _modules.Find(x => x is T);
            return module == null ? throw new ModuleNotFoundException(typeof(T), this) : (T)module;
        }


        #endregion



        #region PRIVATE_METHODS


        private void FindAllModules()
        {
            _modules = new List<IModuleBase>();
            GetComponents<IModuleBase>().ToList().ForEach(x =>
            {
                if (_modules.Contains(x) == false)
                    _modules.Add(x);
            });
        }


        [ContextMenu("Reset debugging")]
        private void ResetDebugging()
        {
            debugInheritance = DebugLevelInheritance.Inherited;
            debugLevel = DebugLevel.Warnings | DebugLevel.Errors;
        }


        [ContextMenu("Reset all module debugging")]
        private void ResetAllDebugging()
        {
            foreach (var item in gameObject.GetComponents<IModuleBaseInternal>())
            {
                item.InternalReset();
            }
        }


        #endregion


    }
}
