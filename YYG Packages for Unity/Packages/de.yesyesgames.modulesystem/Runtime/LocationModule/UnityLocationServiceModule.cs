///////////////////////////////////////////////////////////////////////////////
using System.Collections;

using UnityEngine;

using YYG.Packages.Unity.ModuleSystem.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem
{
    [DisallowMultipleComponent]
    public sealed class UnityLocationServiceModule : ModuleBase, IUnityLocationServiceModule
    {

        public event System.Action<LocationModel> OnLocationChanged;

        public LocationModel Data { get; private set; }

        [Header("UnityLocationServiceModule")]
        [SerializeField, Header("Settings")]
        private UnityLocationServiceSettingsObject m_locationObject = default;

        public LocationInfo _locationInfo;


        private void Awake()
        {
            Data = new();
        }


        public void Begin()
        {
            DBG($"UnityLocationServiceModule::Begin()");
            StopAllCoroutines();
            Input.location.Start(1, 1);

#if UNITY_EDITOR
            StartCoroutine(EditorLoop());
#else
            StartCoroutine(DeviceLoop());
#endif

        }

        public void Cancel()
        {
            DBG($"UnityLocationServiceModule::Cancel()");
            StopAllCoroutines();
            Input.location.Stop();
        }


        private IEnumerator DeviceLoop()
        {
            DBG($"UnityLocationServiceModule::DeviceLoop()");
            yield return new WaitForEndOfFrame();
            while (true)
            {
                if (Input.location.status == LocationServiceStatus.Running)
                {
                    var data = Input.location.lastData;
                    if (data.Equals(_locationInfo) == false)
                    {
                        _locationInfo = data;
                        DBG($"UnityLocationServiceModule::DeviceLoop(): {data.latitude}, {data.longitude}");
                        Data = new(data.latitude, data.longitude, data.horizontalAccuracy);
                        OnLocationChanged?.Invoke(Data);
                    }
                }
                else
                {
                    WRN($"UnityLocationServiceModule::DeviceLoop(): Location service status: {Input.location.status}");
                }
                yield return new WaitForSeconds(m_locationObject.PullFrequency);
            }
        }

        private IEnumerator EditorLoop()
        {
            DBG($"UnityLocationServiceModule::EditorLoop()");
            yield return new WaitForEndOfFrame();
            int mockIndex = -1;
            while (true)
            {
                if (m_locationObject.Simulate)
                {
                    var data = m_locationObject.MockLocations[(++mockIndex) % m_locationObject.MockLocations.Count];
                    DBG($"UnityLocationServiceModule::EditorLoop(): {data.Lat}, {data.Lon}");

                    Data = new(data.Lat, data.Lon);
                    OnLocationChanged?.Invoke(Data);

                    if (m_locationObject.SimulateOnce)
                    {
                        m_locationObject.Set(false, m_locationObject.SimulateOnce);
                    }
                }
                yield return new WaitForSeconds(m_locationObject.PullFrequency);
            }
        }


    }
}
