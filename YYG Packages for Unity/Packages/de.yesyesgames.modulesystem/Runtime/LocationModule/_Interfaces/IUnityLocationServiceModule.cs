public interface IUnityLocationServiceModule
{
    event System.Action<LocationModel> OnLocationChanged;

    LocationModel Data { get; }

    void Begin();
    void Cancel();
}
