using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = "UnityLocationServiceSettings", menuName = "YesYesGames/ModuleSystem/New UnityLocationServiceSettingsObject")]
public class UnityLocationServiceSettingsObject : ScriptableObject
{

    internal bool Simulate => m_simulate;
    internal bool SimulateOnce => m_simulateOnce;
    internal float PullFrequency => m_pullFrequency;
    internal List<LocationModel> MockLocations => m_mockLocations;

    [SerializeField, Header("Settings")]
    [Header("UnityLocationServiceSettings")]
    [Tooltip("Update frequency to ask for location in seconds.")]
    private float m_pullFrequency = 2f;

    [SerializeField, Space]
    private bool m_simulate = true;
    [SerializeField]
    private bool m_simulateOnce = false;
    [SerializeField]
    private List<LocationModel> m_mockLocations = default;


    public UnityLocationServiceSettingsObject()
    {
        // Default home locations around the Sportplatz. :-)
        m_mockLocations = new List<LocationModel>()
        {
            new LocationModel(52.556571, 13.344209),
            new LocationModel(52.556179, 13.344906),
            new LocationModel(52.556806, 13.345822),
            new LocationModel(52.557182, 13.345144),
        };
    }


    internal void Set(bool simulate, bool simulateOnce)
    {
        m_simulate = simulate;
        m_simulateOnce = simulateOnce;
    }


}