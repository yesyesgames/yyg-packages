using Newtonsoft.Json;

[System.Serializable]
public class LocationModel
{

    [JsonRequired]
    public double Lat;
    [JsonRequired]
    public double Lon;
    [JsonRequired]
    public double HorizontalAccuracy;


    public LocationModel() { }
    public LocationModel(double lat, double lon, double horizontalAccuracy = 0)
    {
        Lat = lat;
        Lon = lon;
        HorizontalAccuracy = horizontalAccuracy;
    }


}
