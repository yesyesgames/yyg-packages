/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Objects;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    public interface IRaycastModule : IModuleBase
    {
        bool ScreenCast(Vector2 screenPos, out RaycastHit hit);
        void ScreenCast(Vector2 screenPos, DataPackage data = null);
    }
}
