using UnityEngine;

using YYG.Packages.Unity.Core.Objects;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    public interface IRaycastReceiver
    {
        void Hit(RaycastHit hit);
        void Hit2(RaycastHit hit);
        void Hit(RaycastHit hit, DataPackage data);
        void Hit2(RaycastHit hit, DataPackage data);
    }
}