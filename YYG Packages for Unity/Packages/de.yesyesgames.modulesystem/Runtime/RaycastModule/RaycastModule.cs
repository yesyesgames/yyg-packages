/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.EventSystems;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem
{
    public sealed class RaycastModule : ModuleBase, IRaycastModule
    {

        [Topic("RaycastModule")]
        [Header("Settings")]
        [SerializeField]
        private LayerMask layerMask = default;
        [SerializeField]
        private float rayDistance = 100f;

        private bool _rayBlockedByUI;

        private Camera _camera;
        private EventSystem _eventSystem;

        [Header("DEBUGGING")]
        public bool useTryGet = true;
        public bool showRayCast = true;
        public bool blockFromUI = true;


        private bool Validate()
        {
            if (_eventSystem == null)
                _eventSystem = EventSystem.current;

            if (_camera == null)
                _camera = Camera.main;

            return _eventSystem != null && _camera != null;
        }


        private void Update()
        {
            if (blockFromUI == false)
                return;

            if (_eventSystem == null)
                return;

            _rayBlockedByUI = _eventSystem.IsPointerOverGameObject();
        }


        public void ScreenCast(Vector2 screenPos, DataPackage data = null)
        {
            if (IsEnabled == false)
                return;

            if (Validate() == false)
            {
                ERR($"RaycastModule::RayCast(): Can not perform screen cast; no MainCamera and/or EventSystem found.");
                return;
            }

            DBG($"RaycastModule::ScreenCast({screenPos}, {data})");
            if (ScreenCast(screenPos, out var hit))
            {
                DBG($"RaycastModule::ScreenCast(): Hit detected: '{hit.collider.name}'.");
                if (useTryGet)
                {
                    if (hit.collider.TryGetComponent<RaycastReceiver>(out var value))
                    {
                        value.Hit(hit);
                        value.Hit2(hit);
                        value.Hit(hit, data);
                        value.Hit2(hit, data);
                    }
                    else
                    {
                        WRN($"RaycastModule::ScreenCast(): No receiver found on '{hit.collider.name}'.");
                    }
                    return;
                }

                var receiver = hit.collider.GetComponent<IRaycastReceiver>();
                if (receiver == null)
                {
                    WRN($"RaycastModule::ScreenCast(): No receiver found on '{hit.collider.name}'.");
                    return;
                }

                receiver.Hit(hit);
                receiver.Hit2(hit);
                receiver.Hit(hit, data);
                receiver.Hit2(hit, data);
            }
        }


        public bool ScreenCast(Vector2 screenPos, out RaycastHit hit)
        {
            hit = default;

            if (Validate() == false)
            {
                ERR($"RaycastModule::RayCast(): Can not perform screen cast; no MainCamera and/or EventSystem found.");
                return false;
            }

            var ray = _camera.ScreenPointToRay(screenPos);
            if (showRayCast)
            {
                Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.cyan, 1.42f);
            }

            if (blockFromUI && _rayBlockedByUI)
            {
                return false;
            }

            // if (_eventSystem != null)
            // {
            //     if (blockFromUI && _eventSystem.IsPointerOverGameObject())
            //     {
            //         return false;
            //     }
            // }

            if (Physics.Raycast(ray, out hit, rayDistance, layerMask))
            {
                return true;
            }

            return false;
        }


    }
}
