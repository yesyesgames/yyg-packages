/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.Serialization;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem
{

    public class RaycastReceiver : MonoBehaviour, IRaycastReceiver
    {

        [Topic("RaycastReceiver")]
        [Header("Settings")]
        [SerializeField]
        private RaycastReceiverEvent HitEvent;
        [SerializeField]
        [FormerlySerializedAs("HitEvent2")]
        private RaycastReceiverEvent2 HitEventWithCaller;

        [Space]
        [SerializeField]
        private RaycastReceiverDataEvent HitDataEvent;
        [SerializeField]
        [FormerlySerializedAs("HitDataEvent2")]
        private RaycastReceiverDataEvent2 HitDataEventWithCaller;

        [Space]
        [SerializeField]
        private Component caller;


        public void Hit(RaycastHit hit) => HitEvent.Invoke(hit);
        public void Hit2(RaycastHit hit) => HitEventWithCaller.Invoke(hit, caller);

        public void Hit(RaycastHit hit, DataPackage data) => HitDataEvent.Invoke(hit, data);
        public void Hit2(RaycastHit hit, DataPackage data) => HitDataEventWithCaller.Invoke(hit, caller, data);


    }
}
