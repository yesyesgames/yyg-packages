namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    public interface IModuleManager
    {
        T Get<T>();
    }
}
