using System.Collections.Generic;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    /// <summary>
    ///     Used to hide internal calls.
    /// </summary>
    public interface IModuleManagerInternal
    {
        List<IModuleBase> Modules { get; }

        void OnInitInternal();
        void OnKillInternal();
        void OnFocusInternal(bool state);
    }
}
