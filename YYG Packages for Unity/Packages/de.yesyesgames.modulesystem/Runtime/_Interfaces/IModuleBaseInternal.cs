using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    internal interface IModuleBaseInternal
    {
        void InternalInit(IModuleManager modules, DebugLevel level, DebugLevelInheritance levelOverrides);
        void InternalReset();
    }
}
