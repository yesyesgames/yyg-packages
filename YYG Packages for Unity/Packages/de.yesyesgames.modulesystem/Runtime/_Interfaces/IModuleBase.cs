namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    public interface IModuleBase
    {
        bool Enable { set; }

        void OnInit();
        void OnKill();
        void OnFocus(bool hasFocus);
    }
}
