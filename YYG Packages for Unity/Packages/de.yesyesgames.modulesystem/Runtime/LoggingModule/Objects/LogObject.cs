﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Objects
{
    /// <summary>
    /// Helper object to create a storage on-the-fly.
    /// </summary>
    public class LogObject : ScriptableObject
    {
        public string message;
        public Object context;
        public DebugTypes type;
    }
}
