﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.AppEvents.Objects;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.Core.Statics;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
using YYG.Packages.Unity.ModuleSystem.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem
{
    [DisallowMultipleComponent]
    public sealed class LoggingModule : ModuleBase, ILoggingModule
    {

        #region DECLARES


        [Topic("ModuleManager")]
        [Header("Settings")]
        [ReadOnly]
        [SerializeField]
        private AppEvent logEvent;

#pragma warning disable 0414
        private readonly string AssetName1 = "AppEvent.LoggingModule.Log";


        #endregion



        #region MONO_METHODS


#if UNITY_EDITOR
        private void OnValidate() => Goodies.FindAsset<AppEvent>(AssetName1, out logEvent);
#endif


        #endregion



        #region PUBLIC_METHODS


        public void DBG(string message, Object context) => Log(message, context, DebugTypes.DBG);
        public void WRN(string message, Object context) => Log(message, context, DebugTypes.WRN);
        public void ERR(string message, Object context) => Log(message, context, DebugTypes.ERR);


        #endregion



        #region PRIVATE_METHODS


        private void Log(string message, Object context, DebugTypes type)
        {
#if YYG_USE_LOGGING
            var args = ScriptableObject.CreateInstance<LogObject>();
            args.type = type;
            args.context = context;
            args.message = message;
            logEvent.Raise(args);

            switch (type)
            {
                default:
                case DebugTypes.DBG:
                    Debug.Log(message, context);
                    break;

                case DebugTypes.WRN:
                    Debug.LogWarning(message, context);
                    break;

                case DebugTypes.ERR:
                    Debug.LogError(message, context);
                    break;
            }
#else
        Debug.Log($"LoggingModule:Log(): Add ScriptSymbol 'YYG_USE_LOGGING' to use logging.");
#endif
        }


        #endregion



    }
}
