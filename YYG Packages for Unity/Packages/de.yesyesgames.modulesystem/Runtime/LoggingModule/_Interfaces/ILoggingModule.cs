﻿using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    public interface ILoggingModule
    {
        void DBG(string message, Object context);
        void WRN(string message, Object context);
        void ERR(string message, Object context);
    }
}
