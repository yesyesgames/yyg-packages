using YYG.Packages.Unity.ModuleSystem.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Models
{
    [System.Serializable]
    public class AccountModel : ModelBase
    {
        public UserModel User { get; set; } = new UserModel();
    }
}
