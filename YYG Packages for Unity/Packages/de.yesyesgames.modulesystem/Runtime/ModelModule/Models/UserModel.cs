﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Models
{

    [System.Serializable]
    public class UserModel
    {

        /// <summary>
        /// Unique device id.
        /// </summary>
        [JsonRequired]
        public string UID { get; set; } = "undefined";
        /// <summary>
        /// User's token id.
        /// </summary>
        [JsonRequired]
        public string TID { get; set; } = "undefined";
        /// <summary>
        /// User's authentication id.
        /// </summary>
        [JsonRequired]
        public string AID { get; set; } = "undefined";
        /// <summary>
        /// User's mail address.
        /// </summary>
        [JsonRequired]
        public string Mail { get; set; } = "john.doe@sample.mail";
        /// <summary>
        /// User's nickname.
        /// </summary>
        [JsonRequired]
        public string Nickame { get; set; } = "John Doe";

    }
}
