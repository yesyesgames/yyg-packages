///////////////////////////////////////////////////////////////////////////////
using System.Threading.Tasks;

using YYG.Packages.Unity.ModuleSystem.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Interfaces
{
    public interface IModelModule
    {
        Task<T> Load<T>(string uri) where T : ModelBase;
        Task<bool> Save<T>(string uri, T data) where T : ModelBase;
    }
}
