///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.ModuleSystem.Abstracts;
using YYG.Packages.Unity.ModuleSystem.Models;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Objects
{
    [CreateAssetMenu(fileName = "AppModel", menuName = "YesYesGames/ModuleSystem/New AppModelObject")]
    public class AppModelObject : ScriptableObject
    {

        private ModelBase config;
        private ModelBase session;
        private AccountModel account;


        public void Config<T>(T model) where T : ModelBase => config = model;
        public void Session<T>(T model) where T : ModelBase => session = model;
        public void Account<T>(T model) where T : AccountModel => account = model;


        /// <summary>
        /// Contains all configurations for all players world-wide.
        /// </summary>
        public T Config<T>() where T : ModelBase => config as T;

        /// <summary>
        /// Contains all data only valid each play session.
        /// </summary>
        public T Session<T>() where T : ModelBase => session as T;

        /// <summary>
        /// Contains all configurations made and valid only for this user.
        /// </summary>
        public T Account<T>() where T : AccountModel => account as T;


    }
}
