/////////////////////////////////////////////////////////////////////////////////
using System;
using System.Threading.Tasks;

using YYG.Packages.Unity.Core.Statics;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem
{
    public class ModelModule : ModuleBase, IModelModule
    {

        public async Task<T> Load<T>(string uri) where T : ModelBase
        {
            T result = null;
            if (IOSystem.Exists(uri))
            {
                result = await IOSystem.LoadJson<T>(uri);
            }
            return (result ?? Activator.CreateInstance<T>());
        }


        public async Task<bool> Save<T>(string uri, T data) where T : ModelBase
        {
            return (await IOSystem.SaveJson(uri, data));
        }


    }
}
