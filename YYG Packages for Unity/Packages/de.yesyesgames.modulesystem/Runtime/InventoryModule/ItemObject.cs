using UnityEngine;
using UnityEngine.Localization;

using YYG.Packages.Unity.Core.Attributes;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects
{
    [CreateAssetMenu(fileName = "Item.", menuName = "YesYesGames/ModuleSystem/InventoryModule/New ItemObject")]
    public class ItemObject : ScriptableObject
    {

        [ReadOnly]
        [SerializeField]
        internal string Label;

        public int IID => _iID;

        [Space]
        public ItemGroups group;

        [Space]
        public LocalizedString m_header;
        public LocalizedString m_body;

        [Space]
        public bool IsStackable = true;
        public bool KeepStacked = false;

        [Space]
        public bool DisplayAmount = true;

        [ReadOnly]
        [SerializeField]
        private int _iID;

        [Space]
        [SerializeField]
        [TextArea(5, 10)]
        private string _notes;


        private void Reset()
        {
            Clear();
            Refresh();
        }


        private void OnValidate()
        {
            Label = name;

#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif

        }


        [ContextMenu("Clear ID")]
        public void Clear() => _iID = -1;

        [ContextMenu("Refresh ID")]
        public void Refresh()
        {
            if (_iID != -1)
                return;

            _iID = GetInstanceID();
        }



    }
}
