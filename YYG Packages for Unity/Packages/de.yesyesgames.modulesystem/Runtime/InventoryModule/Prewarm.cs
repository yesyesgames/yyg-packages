using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects
{
    [System.Serializable]
    public class Prewarm
    {
        [ReadOnly]
        [SerializeField]
        internal string Label;

        public int Amount;
        public ItemObject Item;
    }
}
