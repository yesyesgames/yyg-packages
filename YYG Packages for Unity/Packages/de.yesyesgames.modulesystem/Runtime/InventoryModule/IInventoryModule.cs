using System.Collections.Generic;

using YYG.Packages.Unity.ModuleSystem.InventoryModule.Models;
using YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Interfaces
{
    public interface IInventoryModule
    {
        bool Set(int iID, int amount, InventoryObject inventory = null);
        bool Add(int iID, int amount = 1, InventoryObject inventory = null);
        bool Del(int iID, int amount = 1, InventoryObject inventory = null);
        bool Find(int iID, out ItemModel item, InventoryObject inventory = null);
        bool Transfer(int iID, int amount, InventoryObject from, InventoryObject to);
        bool Find(ItemGroups[] filter, out List<ItemModel> items, InventoryObject inventory = null);
    }
}
