using Newtonsoft.Json;

using Unity.Collections;

using UnityEngine;
using UnityEngine.Localization;

using YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Models
{
    // TODO: This should be an struct, right?
    // No! But yes, but no... Because as an reference, values can be altered at different places... BUT!
    // BUT, this is dangerous, right? So, I need to modify scripts to use it as an struct!
    [System.Serializable]
    public class ItemModel
    {

        [ReadOnly]
        [SerializeField]
        internal string Label;

        public int IID;
        public int Amount;
        public bool IsStackable;
        public bool KeepStacked;
        public bool DisplayAmount;

        [JsonRequired]
        public ItemGroups Group;

        public LocalizedString Body;
        public LocalizedString Header;


        public ItemModel() { }
        public ItemModel(int iID, int amount = 1)
        {
            IID = iID;
            Amount = amount;
        }



    }
}
