using System.Collections.Generic;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
using YYG.Packages.Unity.ModuleSystem.InventoryModule.Interfaces;
using YYG.Packages.Unity.ModuleSystem.InventoryModule.Models;
using YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects;
using YYG.Packages.Unity.Utilities;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule
{
    public class InventoryModule : ModuleBase, IInventoryModule
    {

        [Topic("InventoryModule")]
        [Header("Settings")]
        [SerializeField]
        private List<InventoryCatalogueObject> catalogues = default;

        [Space]
        [SerializeField]
        private InventoryObject inventory;


        public bool Set(int iID, int amount, InventoryObject inventory = null)
        {
            Validate(ref inventory);
            if (inventory.Find(iID, out var item))
            {
                item.Amount = amount;
                return true;
            }
            return false;
        }


        public bool Add(int iID, int amount = 1, InventoryObject inventory = null)
        {
            DBG($"InventoryModule::Add({iID}, {amount}, {inventory})");
            Validate(ref inventory);
            foreach (var catalogue in catalogues)
            {
                // TODO: Integrate 'Find' within the catalog.
                var nfo = catalogue.items.Find(x => x.IID == iID);
                if (nfo == null)
                    continue;

                if (inventory.Find(iID, out var item))
                {
                    item.Amount += amount;
                    DBG($"InventoryModule::Add(): Item '{iID}' amount added to '{inventory.name}' inventory.");
                    return true;
                }

                inventory.Add(new ItemModel()
                {
                    Label = nfo.Label,

                    IID = iID,
                    Amount = amount,

                    Group = nfo.group,

                    Body = nfo.m_body,
                    Header = nfo.m_header,

                    IsStackable = nfo.IsStackable,
                    KeepStacked = nfo.KeepStacked,
                    DisplayAmount = nfo.DisplayAmount,
                });

                DBG($"InventoryModule::Add(): Item '{iID}' newly added to '{inventory.name}' inventory.");
                return true;
            }
            ERR($"InventoryModule::Add(): Item '{iID}' not found in any catalog(s) on '{this.name}'.");
            return false;
        }

        public bool Del(int iID, int amount = 1, InventoryObject inventory = null)
        {
            Validate(ref inventory);
            if (inventory.Find(iID, out var item))
            {
                item.Amount = Mathf.Clamp(item.Amount - amount, 0, int.MaxValue);
                if (item.Amount == 0)
                    inventory.Remove(item);

                return true;
            }
            ERR($"InventoryModule::Del(): Item '{iID}' not found in inventory: '{inventory}'.");
            return false;
        }


        public bool Find(int iID, out ItemModel item, InventoryObject inventory = null)
        {
            Validate(ref inventory);
            return inventory.Find(iID, out item);
        }


        public bool Find(ItemGroups[] filter, out List<ItemModel> items, InventoryObject inventory = null)
        {
            Validate(ref inventory);
            return inventory.Find(filter, out items);
        }


        public bool Transfer(int iID, int amount, InventoryObject from, InventoryObject to)
        {
            Validate(ref from);
            Validate(ref to);
            var a = Add(iID, amount, to);
            var b = Del(iID, amount, from);
            if (a && b)
                return true;

            return false;
        }


        private void Validate(ref InventoryObject inventory)
        {
            if (inventory == null)
                inventory = this.inventory;

            if (inventory == null)
                ERR($"InventoryModule::Del(): Inventory is NULL or invalid.");
        }


    }
}
