using System.Collections.Generic;

using UnityEngine;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects
{
    [CreateAssetMenu(fileName = "InventoryCatalogue", menuName = "YesYesGames/ModuleSystem/InventoryModule/New InventoryCatalogueObject")]
    public class InventoryCatalogueObject : ScriptableObject
    {
        public List<ItemObject> items;
    }
}
