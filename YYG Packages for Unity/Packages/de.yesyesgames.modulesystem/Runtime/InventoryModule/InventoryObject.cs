using System.Collections.Generic;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.ModuleSystem.InventoryModule.Models;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects
{
    [CreateAssetMenu(fileName = "Inventory.", menuName = "YesYesGames/ModuleSystem/InventoryModule/New InventoryObject")]
    public class InventoryObject : ScriptableObject
    {

        public bool KeepZeroItems { set => keepZeroItems = value; }

        [SerializeField]
        private List<Prewarm> prewarm;

        [Topic("InventoryObject")]
        [Header("Settings")]
        [SerializeField]
        [Tooltip("Not yet implemented.")]
        private bool autoRebuild = false;
        [SerializeField]
        private bool keepZeroItems = false;

        [SerializeField]
        [HideInInspector]
        private List<ItemModel> _items;

        [Header("DEBUGGING")]
        [ReadOnly]
        public bool IsDirty;
        [ReadOnly]
        public bool IsPersistent;


        internal void Refresh() => OnValidate();

        internal bool Find(int iID, out ItemModel item)
        {
            item = default;
            if (_items == null)
                return false;

            item = _items.Find(x => x.IID == iID);
            return item != null;
        }


        internal bool Find(ItemGroups[] filter, out List<ItemModel> items)
        {
            items = default;
            if (_items == null)
                return false;

            items = new();
            if (filter[0] == ItemGroups.NONE)
            {
                items.AddRange(_items);
            }
            else
            {
                foreach (var item in filter)
                    items.AddRange(_items.FindAll(x => x.Group.HasFlag(item)));
            }
            return items != null;
        }


        internal void Add(ItemModel item)
        {
            if (_items == null)
                return;

            _items.Add(item);
        }


        internal void Remove(ItemModel item)
        {
            if (_items == null)
                return;

            if (item.Amount <= 0 && keepZeroItems)
                return;

            _items.Remove(item);
        }


        [ContextMenu("Refresh")]
        private void OnValidate()
        {
#if UNITY_EDITOR
            IsDirty = UnityEditor.EditorUtility.IsDirty(this);
            IsPersistent = UnityEditor.EditorUtility.IsPersistent(this);
#endif

            if (_items == null)
                return;

            if (prewarm.Count != _items.Count)
                return;

            for (int i = 0; i < prewarm.Count; i++)
            {
                if (prewarm[i] == null || _items[i] == null)
                    break;

                prewarm[i].Label = _items[i].Label;
            }
        }


        [ContextMenu("Rebuild")]
        internal void Rebuild()
        {
            _items ??= new();
            _items.Clear();
            foreach (var item in prewarm)
            {
                _items.Add(new ItemModel()
                {
                    Label = item.Item.Label,

                    IID = item.Item.IID,
                    Amount = item.Amount,

                    Group = item.Item.group,

                    Body = new(item.Item.m_body.TableReference, item.Item.m_body.TableEntryReference),
                    Header = new(item.Item.m_header.TableReference, item.Item.m_header.TableEntryReference),

                    IsStackable = item.Item.IsStackable,
                    KeepStacked = item.Item.KeepStacked,
                    DisplayAmount = item.Item.DisplayAmount,
                });
            }

#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif

        }


    }
}
