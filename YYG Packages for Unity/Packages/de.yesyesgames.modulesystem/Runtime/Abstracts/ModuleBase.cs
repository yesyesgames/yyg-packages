///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Abstracts
{
    [RequireComponent(typeof(LoggingModule))]
    [RequireComponent(typeof(ModuleManager))]
    public abstract class ModuleBase : MonoBehaviour, IModuleBase, IModuleBaseInternal
    {

        public bool IsEnabled => moduleEnabled;
        public bool Enable { set => moduleEnabled = value; }
        public DebugLevel DebugLevel => debugLevel;
        public IModuleManager Modules { get; private set; }

        [Topic("ModuleBase")]
        [Header("Settings")]
        [SerializeField]
        private bool moduleEnabled = true;

        [Space]
        [SerializeField]
        private DebugLevel debugLevel = DebugLevel.Warnings | DebugLevel.Errors;

        protected bool m_hasFocus;


        public virtual void OnInit() { }
        public virtual void OnKill() { }
        public virtual void OnFocus(bool hasFocus) => m_hasFocus = hasFocus;


        public void DBG(string message)
        {
            if (DebugLevel.HasFlag(DebugLevel.Debugs))
                Modules.Get<ILoggingModule>().DBG(message, this);
        }


        public void WRN(string message)
        {
            if (DebugLevel.HasFlag(DebugLevel.Warnings))
                Modules.Get<ILoggingModule>().WRN(message, this);
        }


        public void ERR(string message)
        {
            if (DebugLevel.HasFlag(DebugLevel.Errors))
                Modules.Get<ILoggingModule>().ERR(message, this);
        }


        void IModuleBaseInternal.InternalInit(IModuleManager moduleManager, DebugLevel level, DebugLevelInheritance levelOverrides)
        {
            Modules = moduleManager;
            if (levelOverrides == DebugLevelInheritance.Inherited)
                debugLevel = level;
        }

        void IModuleBaseInternal.InternalReset()
        {
            ResetDebugging();
        }


        [ContextMenu("Reset debugging")]
        private void ResetDebugging()
        {
            debugLevel = DebugLevel.Warnings | DebugLevel.Errors;
        }


    }
}
