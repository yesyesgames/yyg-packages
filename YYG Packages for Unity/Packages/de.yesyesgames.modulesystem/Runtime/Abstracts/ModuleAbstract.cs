﻿using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ModuleSystem.Abstracts
{
    [System.Obsolete("")]
    public abstract class ModuleAbstract : MonoBehaviour
    {

        internal bool ModuleEnabled => m_moduleIsEnabled;
        internal bool IgnoreLogOverride => m_ignoreLogOverride;

        [Header("ModuleAbstract"), SerializeField]
        [Header("Settings")]
        private bool m_moduleIsEnabled = true;
        [SerializeField]
        private bool m_ignoreLogOverride = false;

        /// <summary>
        /// Be super careful!
        /// OnInit is not Singleton safe, what means it's called within Awake.
        /// </summary>
        internal virtual void OnInit()
        {
            //
        }


        internal virtual void OnFocus(bool hasFocus)
        {
            //
        }


    }
}
