using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;
using YYG.Packages.Unity.ModuleSystem.Interfaces;

namespace YYG.Packages.Unity.ModuleSystem.Editor
{
    [CustomEditor(typeof(ModuleManager))]
    public class ModuleManagerEditor : EditorBase
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            BeginSection();
            BeginVertical();

            if (GetScriptTarget<ModuleManager>(out var script, out var isPlaymode))
            {
                if (((IModuleManagerInternal)script).Modules == null)
                    DrawLabel("No modules found.");
                else
                {
                    if (isPlaymode)
                        DrawList<IModuleBase>("Module(s)", ((IModuleManagerInternal)script).Modules);
                    else
                        DrawLabel("Only while in Playmode.");
                }
            }

            EndVertical();
            EndSection();
        }
    }
}
