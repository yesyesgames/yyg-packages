using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;
using YYG.Packages.Unity.ModuleSystem.InventoryModule.Objects;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Editor
{
    [CustomEditor(typeof(InventoryObject))]
    public class InventoryObjectEditor : EditorBase
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GetScriptTarget<InventoryObject>(out var script, out var isPlaymode))
            {
                BeginSection("Inventory Items");
                BeginVertical();

                DrawButton("Rebuild", script.Rebuild);
                DrawButton("Refresh", script.Refresh);
                DrawGap();
                DrawLabel("Items", HeaderStyle);
                DrawGap();

                if (script.Find(new ItemGroups[] { ItemGroups.NONE }, out var items))
                {
                    if (items.Count == 0)
                    {
                        DrawLabel($"No items added.");
                    }
                    else
                    {
                        foreach (var item in items)
                        {
                            EditorGUILayout.BeginHorizontal();
                            DrawLabel($"{item.Label}");
                            DrawLabel($"ID: {item.IID}");
                            DrawLabel($"Group: {item.Group}");
                            DrawLabel($"Amount: {item.Amount}");
                            EditorGUILayout.EndHorizontal();
                        }
                    }
                }
                EndVertical();
                EndSection();
            }

        }

    }
}
