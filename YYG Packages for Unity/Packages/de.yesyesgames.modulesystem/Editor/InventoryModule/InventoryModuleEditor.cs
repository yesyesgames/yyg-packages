using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;

namespace YYG.Packages.Unity.ModuleSystem.InventoryModule.Editor
{
    [CustomEditor(typeof(InventoryModule))]
    public class InventoryModuleEditor : EditorBase
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GetScriptTarget<InventoryModule>(out var script, out var isPlaymode))
            {

            }

        }

    }
}
