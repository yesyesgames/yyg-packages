﻿/////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Abstracts.Editor
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyAttributeEditor : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = true;
        }


    }
}
