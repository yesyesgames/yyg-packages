﻿/////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Abstracts.Editor
{
    [CustomPropertyDrawer(typeof(TopicAttribute))]
    public class TopicAttributeDrawer : DecoratorDrawer
    {

        public override float GetHeight() => EditorGUIUtility.singleLineHeight * 2.5f;

        public override void OnGUI(Rect position)
        {
            position.yMin += EditorGUIUtility.singleLineHeight * 0.5f;
            position = EditorGUI.IndentedRect(position);

            var text = $"{(attribute as TopicAttribute).topic}\n--------------------";

            GUI.Label(position, text, EditorStyles.boldLabel);
        }


    }
}
