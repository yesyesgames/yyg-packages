﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEditor;

using UnityEngine;

/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Editor.Abstracts
{
    public abstract class EditorBase : UnityEditor.Editor
    {

        protected GUIStyle ErrorStyle { get; private set; }
        protected GUIStyle HeaderStyle { get; private set; }
        protected GUIStyle DefaultStyle { get; private set; }
        protected GUIStyle WarningStyle { get; private set; }


        private readonly Dictionary<int, bool> _elements = new Dictionary<int, bool>();


        protected virtual void OnEnable()
        {
            DefaultStyle = new GUIStyle
            {
                fontSize = 10,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                normal =
                {
                    textColor = Color.gray
                }
            };
            WarningStyle = new GUIStyle
            {
                fontSize = 10,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                normal =
                {
                    textColor = new Color(.75f, .45f, .15f)
                }
            };
            ErrorStyle = new GUIStyle
            {
                fontSize = 10,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                normal =
                {
                    textColor = new Color(.85f, .15f, .15f)
                }
            };
            HeaderStyle = new GUIStyle
            {
                fontSize = 11,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleLeft,
                normal =
                {
                    textColor = EditorGUIUtility.isProSkin ? Color.white : new Color(.12f, .12f, .12f)
                }
            };
        }


        protected virtual void OnDisable()
        {
            _elements.Clear();
        }


        protected static void EndSection() => EditorGUI.indentLevel--;
        protected bool GetScriptTarget<T>(out T script) where T : UnityEngine.Object => GetScriptTarget<T>(out script, out var isPlaying);


        protected bool GetScriptTarget<T>(out T script, out bool isPlaying) where T : UnityEngine.Object
        {
            script = default;
            isPlaying = Application.isPlaying;
            if (target is T t)
                script = t;

            return (script != default);
        }


        protected static void BeginVertical()
        {
            EditorGUILayout.Space(15);
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUI.indentLevel++;
        }


        protected void BeginSection(string header = null)
        {
            EditorGUILayout.Space();
            if (string.IsNullOrEmpty(header) == false)
                EditorGUILayout.LabelField(header, HeaderStyle);

            EditorGUI.indentLevel++;
        }


        protected static void EndVertical()
        {
            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
        }


        protected void DrawLabel(string label, GUIStyle style = null) => DrawLabel(label, string.Empty, style);
        protected void DrawLabel(string label, string toolTip, GUIStyle style = null)
        {
            // Try to detect lines & adjust the height & add some magic to make it look nice!
            var height = (label.Split('\n').Length + 1) * (style != null ? style.fontSize : DefaultStyle.fontSize);
            var content = new GUIContent(label, toolTip);
            EditorGUILayout.LabelField(content, style ?? DefaultStyle, GUILayout.Height(height));
        }


        protected void DrawList<T>(string content, List<T> data)
        {
            var hash = data.GetHashCode();
            if (_elements.ContainsKey(hash) == false)
                _elements.Add(hash, false);

            EditorGUI.indentLevel++;
            _elements[hash] = EditorGUILayout.Foldout(_elements[hash], $"> {content} ({data.Count})", true, DefaultStyle);
            if (_elements[hash])
            {
                EditorGUI.indentLevel++;
                data?.ForEach(x => { EditorGUILayout.LabelField(x.ToString(), DefaultStyle, GUILayout.Height(10)); });
                EditorGUI.indentLevel--;
                EditorGUILayout.Space(5);
            }
            EditorGUI.indentLevel--;
        }


        protected void DrawDictionary<TK, TV>(string content, Dictionary<TK, TV> data)
        {
            var hash = data.GetHashCode();
            if (_elements.ContainsKey(hash) == false)
                _elements.Add(hash, false);

            EditorGUI.indentLevel++;
            _elements[hash] = EditorGUILayout.Foldout(_elements[hash], $"> {content} ({data.Count})", true, DefaultStyle);
            if (_elements[hash])
            {
                EditorGUI.indentLevel++;
                for (var k = 0; k < data.Keys.Count; k++)
                {
                    EditorGUILayout.LabelField($"{data.ElementAt(k).Key}", DefaultStyle, GUILayout.Height(10));
                    data.TryGetValue(data.ElementAt(k).Key, out var value);
                    EditorGUI.indentLevel++;
                    if (value is IList list)
                    {
                        foreach (var item in list)
                            EditorGUILayout.LabelField($"{item}", DefaultStyle, GUILayout.Height(10));
                    }
                    else
                        EditorGUILayout.LabelField($"{value}", DefaultStyle, GUILayout.Height(10));
                    EditorGUI.indentLevel--;
                }
                EditorGUI.indentLevel--;
                EditorGUILayout.Space(5);
            }
            EditorGUI.indentLevel--;
        }


        protected void DrawToggle(string label, ref bool value)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(label, DefaultStyle, GUILayout.Width(100));
            value = EditorGUILayout.Toggle(value);
            GUILayout.EndHorizontal();
        }


        protected void DrawInput<T>(string label, ref T value) => DrawInput<T>(label, string.Empty, ref value);
        protected void DrawInput<T>(string label, string toolTip, ref T value)
        {
            GUILayout.BeginHorizontal();

            var content = new GUIContent(label, toolTip);
            EditorGUILayout.LabelField(content, DefaultStyle, GUILayout.Width(100));

            switch (value)
            {
                default: throw new SystemException("DrawInput");
                case int i:
                    value = (T)(object)EditorGUILayout.IntField(i);
                    break;
                case float f:
                    value = (T)(object)EditorGUILayout.FloatField(f);
                    break;
                case string str:
                    value = (T)(object)EditorGUILayout.TextField(str);
                    break;
            }
            GUILayout.EndHorizontal();
        }


        protected static void DrawGap(float height = 5) => EditorGUILayout.Space(height);


        protected void DrawButton(string label, Action resolve, bool isEnabled = true)
        {
            GUI.enabled = isEnabled;
            if (GUILayout.Button(label))
                resolve?.Invoke();

            GUI.enabled = true;
        }


    }
}
