///////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Editor
{
    [CustomEditor(typeof(InspectorButton))]
    public class InspectorButtonEditor : EditorBase
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GetScriptTarget<InspectorButton>(out var script, out var isPlaymode) == false)
                return;

            DrawButton("Trigger event", () => script.unityEvent.Invoke());
        }

    }
}
