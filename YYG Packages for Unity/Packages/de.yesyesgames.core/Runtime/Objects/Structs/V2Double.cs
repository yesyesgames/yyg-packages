﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    public struct V2Double
    {

        [JsonIgnore]
        public static V2Double Zero => new V2Double(0, 0);
        [JsonIgnore]
        public Vector2 ToV2 => new Vector2((float)m_x, (float)m_z);
        [JsonIgnore]
        public Vector3 ToV3 => new Vector3((float)m_x, 0, (float)m_z);

        public double m_x;
        public double m_z;


        public override string ToString() => $"{m_x}, {m_z}";

        public V2Double(Vector2 value)
        {
            m_x = value.x;
            m_z = value.y;
        }


        public V2Double(Vector3 value)
        {
            m_x = value.x;
            m_z = value.z;
        }


        public V2Double(double x, float y)
        {
            m_x = x;
            m_z = y;
        }


    }
}
