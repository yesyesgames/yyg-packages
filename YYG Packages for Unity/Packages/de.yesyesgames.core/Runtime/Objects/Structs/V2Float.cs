﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    public struct V2Float
    {

        [JsonIgnore]
        public static V2Float Zero => new V2Float(0, 0);
        [JsonIgnore]
        public Vector2 ToV2 => new Vector2(m_x, m_z);
        [JsonIgnore]
        public Vector3 ToV3 => new Vector3(m_x, 0, m_z);

        public float m_x;
        public float m_z;


        public override string ToString() => $"{m_x}, {m_z}";

        public V2Float(Vector2 value)
        {
            m_x = value.x;
            m_z = value.y;
        }


        public V2Float(Vector3 value)
        {
            m_x = value.x;
            m_z = value.z;
        }


        public V2Float(float x, float y)
        {
            m_x = x;
            m_z = y;
        }


    }
}
