﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    public struct V2Int
    {

        [JsonIgnore]
        public static V2Int Zero => new V2Int(0, 0);
        [JsonIgnore]
        public Vector2 V2 => new Vector2(m_x, m_z);
        public Vector3 V3 => new Vector3(m_x, 0, m_z);


        [JsonProperty]
        public int m_x;
        [JsonProperty]
        public int m_z;


        public V2Int(Vector2 value)
        {
            m_x = (int)value.x;
            m_z = (int)value.y;
        }


        public V2Int(Vector3 value)
        {
            m_x = (int)value.x;
            m_z = (int)value.z;
        }


        public V2Int(float x, float z)
        {
            m_x = (int)x;
            m_z = (int)z;
        }


        public V2Int(int x, int z)
        {
            m_x = x;
            m_z = z;
        }


        public override string ToString()
        {
            return ($"({m_x}, {m_z})");
        }


    }
}
