﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    public struct V3Float
    {

        [JsonIgnore]
        public Vector3 ToUnity => new Vector3(x, y, z);

        public float x;
        public float y;
        public float z;


        public V3Float(Vector3 value)
        {
            x = value.x;
            y = value.y;
            z = value.z;
        }


        public V3Float(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }


    }
}
