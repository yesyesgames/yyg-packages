﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    public struct V3Int
    {

        [JsonIgnore]
        public static V3Int Zero => new V3Int();
        [JsonIgnore]
        public Vector3 ToUnity => new Vector3(x, y, z);

        public int x;
        public int y;
        public int z;


        public override string ToString() => $"V3Int [{x}, {y}, {z}]";


        public V3Int(Vector3 value)
        {
            x = (int)value.x;
            y = (int)value.y;
            z = (int)value.z;
        }


        public V3Int(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }


        public V3Int(float x, float y, float z)
        {
            this.x = (int)x;
            this.y = (int)y;
            this.z = (int)z;
        }


    }
}
