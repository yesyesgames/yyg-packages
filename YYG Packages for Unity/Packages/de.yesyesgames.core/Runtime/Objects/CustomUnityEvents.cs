/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.Events;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    public class RaycastReceiverEvent : UnityEvent<RaycastHit> { }
    
    [System.Serializable]
    public class RaycastReceiverEvent2 : UnityEvent<RaycastHit, Component> { }

    [System.Serializable]
    public class RaycastReceiverDataEvent : UnityEvent<RaycastHit, DataPackage> { }
    
    [System.Serializable]
    public class RaycastReceiverDataEvent2 : UnityEvent<RaycastHit, Component, DataPackage> { }
}
