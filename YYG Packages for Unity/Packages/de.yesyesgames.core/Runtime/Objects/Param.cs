﻿///////////////////////////////////////////////////////////////////////////////
using Newtonsoft.Json;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "Param.", menuName = "YYG Packages for Unity/Core/New Param")]
    public class Param : ScriptableObject
    {

        [JsonIgnore]
        public string ID => _internalID;

        [JsonIgnore]
        public string Name => this.name;

        [SerializeField]
        [Tooltip("Use context menu 'Create' to create a unique persistent ID.")]
        private string _internalID;


        public void Set(string id) => _internalID = id;

        [ContextMenu("Create")]
        private void Create() => _internalID = System.Guid.NewGuid().ToString();



    }
}
