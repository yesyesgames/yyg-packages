﻿namespace YYG.Packages.Unity.Core.Objects
{
    /// <summary>
    /// Optional object to pass data between Activities.
    /// TODO: This is more like a Core component; move me.
    /// </summary>
    /// <remarks>Supports method-chaining.</remarks>
    public sealed class DataPackage
    {
        public int Integer { get; private set; }
        public float Float { get; private set; }
        public string String { get; private set; }
        public object Custom { get; private set; }


        public DataPackage Set(int value)
        {
            Integer = value;
            return (this);
        }


        public DataPackage Set(float value)
        {
            Float = value;
            return (this);
        }


        public DataPackage Set(string value)
        {
            String = value;
            return (this);
        }


        public DataPackage Set(object custom)
        {
            Custom = custom;
            return (this);
        }
    }
}
