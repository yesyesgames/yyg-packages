﻿namespace YYG.Packages.Unity.Core.Objects
{
    [System.Flags]
    public enum DebugTypes
    {
        Disabled = 0,
        DBG = 1,
        WRN = 2,
        ERR = 4,
    }

    [System.Flags]
    public enum DebugLevel
    {
        Disabled = 0,
        Debugs = 1,
        Warnings = 2,
        Errors = 4,
    }

    public enum DebugLevelInheritance
    {
        Inherited = 0,
        Individual = 1,
    }
}
