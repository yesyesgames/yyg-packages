﻿///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using YYG.Packages.Unity.Core.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Objects
{
    public class DataPool : SingletonBase<DataPool>
    {

        #region DECLARES


        private Dictionary<string, object> _dataPool = new();


        #endregion



        #region PUBLIC_METHODS


        public void Push<T>(string key, T value)
        {
            if (_dataPool.ContainsKey(key))
            {
                _dataPool[key] = value;
                return;
            }
            _dataPool.Add(key, value);
        }


        public T Pull<T>(string key)
        {
            if (_dataPool.ContainsKey(key) == false)
                return (default);

            if (_dataPool[key] is T value)
                return (value);

            return (default);
        }


        public bool Delete(string key)
        {
            if (_dataPool.ContainsKey(key) == false)
                return (false);

            _dataPool.Remove(key);
            return (true);
        }


        #endregion



    }
}
