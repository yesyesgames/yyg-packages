﻿///////////////////////////////////////////////////////////////////////////////
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Interfaces
{
    public interface IPoolObjectParam
    {
        Param PoolID { get; }
    }

    public interface IPoolObjectString
    {
        string PoolID { get; }
    }

    public interface IPoolObjectInt
    {
        int PoolID { get; }
    }
}
