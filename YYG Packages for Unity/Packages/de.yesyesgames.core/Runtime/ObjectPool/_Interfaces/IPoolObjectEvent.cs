﻿///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Interfaces
{
    public interface IPoolObjectEvent
    {
        void PoolStateChanged(bool isPooled);
    }
}
