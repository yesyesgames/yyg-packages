///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Interfaces;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core
{
    public class PoolObject : MonoBehaviour, IPoolObjectParam, IPoolObjectEvent
    {

        public Param PoolID => _poolID;

        [SerializeField]
        protected Param _poolID;


        public virtual void PoolStateChanged(bool isPooled)
        {
            //
        }


    }
}
