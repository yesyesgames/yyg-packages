///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core
{
    [System.Serializable]
    public class PrewarmObject

    {
        public int Amount = 1;
        public Param PoolID;
        public GameObject GameObject;
    }
}
