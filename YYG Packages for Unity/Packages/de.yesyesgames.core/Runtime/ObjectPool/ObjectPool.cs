﻿///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.Core.Abstracts;
using YYG.Packages.Unity.Core.Interfaces;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core
{
    public class ObjectPool : SingletonBase<ObjectPool>
    {

        public bool IsPrewarming { get; private set; }

        [Header("Settings")]
        [Tooltip("OPTIONAL: But will be disable in final build.")]
        public bool UseParenting = true;
        [Tooltip("If set to 0 the pooled object will stay where it got pooled. Means, manually steps are necessary to hide it.")]
        public int MoveAwayFactor = 2048;
        public bool VerboseLogging = false;

        private Dictionary<object, Queue<GameObject>> _data = new();


        protected override void Awake()
        {
#if UNITY_EDITOR == false
            UseParenting = false;
#endif
        }


        public int HowMany(int poolID) => Count(poolID);
        public int HowMany(Param poolID) => Count(poolID);
        public int HowMany(string poolID) => Count(poolID);

        public void Push(int poolID, GameObject poolObject) => Add<int>(poolID, poolObject);
        public void Push(Param poolID, GameObject poolObject) => Add<Param>(poolID, poolObject);
        public void Push(string poolID, GameObject poolObject) => Add<string>(poolID, poolObject);

        public bool Pull(int poolID, out GameObject poolObject) => Get<int>(poolID, out poolObject);
        public bool Pull(Param poolID, out GameObject poolObject) => Get<Param>(poolID, out poolObject);
        public bool Pull(string poolID, out GameObject poolObject) => Get<string>(poolID, out poolObject);

        public bool Pull<T>(int poolID, out T poolObject) where T : MonoBehaviour => Get<int, T>(poolID, out poolObject);
        public bool Pull<T>(Param poolID, out T poolObject) where T : MonoBehaviour => Get<Param, T>(poolID, out poolObject);
        public bool Pull<T>(string poolID, out T poolObject) where T : MonoBehaviour => Get<string, T>(poolID, out poolObject);


        /// <summary>
        /// Push a object to the pool that implements the IPoolObjectParam, IPoolObjectString or IPoolObjectInt
        /// </summary>
        /// <param name="poolObject"></param>
        public bool Push(GameObject poolObject)
        {
            if (GetPoolID(poolObject, out var poolID))
            {
                Add(poolID, poolObject);
                return true;
            }

            if (VerboseLogging)
            {
                Debug.LogWarning($"ObjectPool::Push(): '{poolObject.name}' does not implement a IPoolObject interface; skipping.", poolObject);
            }

            return false;
        }


        public async Task Push(List<GameObject> poolObjects, bool useAsync = false)
        {
            foreach (var poolObject in poolObjects)
            {
                Push(poolObject);
                if (useAsync)
                {
                    await Task.Yield();
                }
            }
        }


        public int HowMany()
        {
            var amount = 0;
            foreach (var item in _data)
            {
                amount += item.Value.Count;
            }
            return amount;
        }


        public async Task Prewarm(List<PrewarmObject> objects, bool useAsync = false)
        {
            IsPrewarming = true;
            foreach (var item in objects)
            {
                for (int i = 0; i < item.Amount; i++)
                {
                    var go = Instantiate(item.GameObject);
                    Push(item.PoolID, go);

                    if (useAsync)
                    {
                        await Task.Yield();
                    }
                }
                if (useAsync)
                {
                    await Task.Yield();
                }
            }
            IsPrewarming = false;
        }


        private int Count(object poolID)
        {
            if (_data.ContainsKey(poolID))
            {
                if (_data[poolID].Count > 0)
                {
                    return _data[poolID].Count;
                }
            }

            return 0;
        }


        private bool Get<T, C>(object id, out C obj) where C : MonoBehaviour
        {
            obj = default;
            if (Get(id, out var go))
            {
                go.TryGetComponent<C>(out obj);
                return obj != null;
            }
            return false;
        }


        private bool Get<T>(T id, out GameObject obj)
        {
            obj = null;
            if (id == null)
            {
                if (VerboseLogging)
                {
                    Debug.LogError($"ObjectPool::Pull(): PoolID is NULL or invalid.");
                }
                return false;
            }

            if (_data.ContainsKey(id))
            {
                if (_data[id].Count > 0)
                {
                    obj = _data[id].Dequeue();
                    if (obj == null)
                    {
                        return false;
                    }

                    obj.transform.parent = null;
                    if (GetPoolEventComponent(obj, out var components))
                    {
                        foreach (var item in components)
                        {
                            item.PoolStateChanged(false);
                        }
                    }

                    return true;
                }
            }
            return false;
        }


        private void Add<T>(T id, GameObject obj)
        {
            if (obj == null)
            {
                if (VerboseLogging)
                {
                    Debug.LogError($"ObjectPool::Push(): PoolObject '{obj}' is NULL or invalid, skipping.");
                }
                return;
            }

            if (_data.ContainsKey(id) == false)
            {
                _data.Add(id, new Queue<GameObject>());
            }

            _data[id].Enqueue(obj);

            if (UseParenting)
            {
                obj.transform.SetParent(transform, false);
            }

            obj.transform.localPosition += Vector3.one * MoveAwayFactor;

            if (GetPoolEventComponent(obj, out var components))
            {
                foreach (var item in components)
                {
                    item.PoolStateChanged(true);
                }
            }
        }


        private bool GetPoolID(GameObject poolObject, out object poolID)
        {
            poolID = null;

            var c1 = poolObject.GetComponentInChildren<IPoolObjectInt>(true);
            if (c1 != null)
            {
                poolID = c1.PoolID;
                return true;
            }

            var c2 = poolObject.GetComponentInChildren<IPoolObjectParam>(true);
            if (c2 != null)
            {
                poolID = c2.PoolID;
                return true;
            }

            var c3 = poolObject.GetComponentInChildren<IPoolObjectString>(true);
            if (c3 != null)
            {
                poolID = c3.PoolID;
                return true;
            }
            return false;
        }


        private bool GetPoolEventComponent(GameObject poolObject, out IPoolObjectEvent[] components)
        {
            components = poolObject.GetComponentsInChildren<IPoolObjectEvent>(true);
            return components != null && components.Length > 0;
        }


    }
}
