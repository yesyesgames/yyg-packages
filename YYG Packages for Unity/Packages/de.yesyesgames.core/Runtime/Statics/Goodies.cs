﻿/////////////////////////////////////////////////////////////////////////////////
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using UnityEngine;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Statics
{
    public static class Goodies
    {

        /// <summary>
        /// Time since Unix Epoch 1970-01-01.
        /// </summary>
        public static long EpochSeconds => System.DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        /// <summary>
        /// Time since Unix Epoch 1970-01-01.
        /// </summary>
        public static long EpochMilliseconds => System.DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();


        /// <summary>
        /// Serialize a given object and returns the string on success otherwise NULL.
        /// </summary>
        /// <param name="data">Data object to be serialized.</param>
        /// <param name="settings">Optional settings.</param>
        /// <param name="cts">Optional token to cancel Task operation.</param>
        /// <returns>String otherwise string.Empty.</returns>
        public static async Task<string> TryToSerialize(object data, JsonSerializerSettings settings = null, CancellationToken cts = default)
        {
            settings ??= new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.All,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                //
            };

            try
            {
                return await Task.Run(() => JsonConvert.SerializeObject(data, settings), cts);
            }
            catch (System.Exception e)
            {
                throw new System.Exception(e.Message);
                // Debug.LogError($"{e}");
                // return string.Empty;
            }
        }


        /// <summary>
        /// DeSerialize given string to specified object.
        /// </summary>
        /// <remarks>Runs async and can be canceled.</remarks>
        /// <param name="json"><code>string</code> to deserialize.</param>
        /// <param name="cts">Cancellation token.</param>
        /// <typeparam name="T">Type of serializable class.</typeparam>
        /// <returns>Object as typeof(T) otherwise NULL.</returns>
        public static async Task<T> TryToDeserialize<T>(string json, JsonSerializerSettings settings = null, CancellationToken cts = default)
        {
            settings ??= new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Include,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                //
            };
            try
            {
                return await Task.Run(() => JsonConvert.DeserializeObject<T>(json, settings), cts);
            }
            catch (System.Exception e)
            {
                throw new System.Exception(e.Message);
                // Debug.LogError($"{e}");
                // return default;
            }
            finally
            {
                //
            }
        }


        /// <summary>
        /// Looks into Unity's AssetDatabase to find assets. 
        /// </summary>
        /// <typeparam name="T">Type of expected object to be returned.</typeparam>
        /// <param name="filter">Asset name or filter line based on Unity's specifications.</param>
        /// <returns>Found object of type T or NULL.</returns>
        public static T FindAsset<T>(string filter) where T : UnityEngine.Object
        {
            FindAsset<T>(filter, out T asset);
            return asset;
        }


        /// <summary>
        /// Looks into Unity's AssetDatabase to find assets. 
        /// </summary>
        /// <typeparam name="T">Type of expected object to be returned.</typeparam>
        /// <param name="filter">Asset name or filter line based on Unity's specifications.</param>
        /// <param name="value">Database asset matching specified filter.</param>
        /// <returns>TRUE on success otherwise FALSE.</returns>
        public static bool FindAsset<T>(string filter, out T value) where T : UnityEngine.Object
        {
            value = null;
#if UNITY_EDITOR
            if (string.IsNullOrEmpty(filter))
            {
                Debug.LogError($"Goodies::FindAsset(): ERR#01 - Asset filter '{filter}' is NULL or invalid.");
            }
            else
            {
                var asset = UnityEditor.AssetDatabase.FindAssets(filter);
                if (asset == null)
                {
                    Debug.LogError($"Goodies::FindAsset(): ERR#02 - Asset for filter '{filter}' is NULL or invalid.");
                }
                else if (asset.Length == 0)
                {
                    // TODO: I disabled this message since it gets thrown but the asset will be find anyways if it's valid.
                    // I mean, at the moment the error is valid but in another frame/tick the asset got found.
                    // Debug.LogError($"Goodies::FindAsset(): ERR#03 - Asset for filter '{filter}' is NULL or invalid.");
                }
                else
                {
                    value = (T)UnityEditor.AssetDatabase.LoadAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(asset[0]), typeof(T));
                }
            }
#endif
            return value == default;
        }



    }
}
