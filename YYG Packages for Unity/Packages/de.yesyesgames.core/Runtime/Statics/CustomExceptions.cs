﻿/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Statics
{
    public sealed class CustomExceptions
    {
        public class FeatureNotYetImplementedException : System.Exception
        {
            public FeatureNotYetImplementedException(string message) : base(message)
            {
                //
            }
        }

        public class ModuleNotFoundException : System.Exception
        {
            public ModuleNotFoundException(System.Type module, UnityEngine.Object activity) : base($"Module '{module}' not found at {activity.name}.")
            {
                //
            }
        }
    }
}
