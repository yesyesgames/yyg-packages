﻿/////////////////////////////////////////////////////////////////////////////////
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using UnityEngine;
using UnityEngine.Networking;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Statics
{
    public static class IOSystem
    {

        /// <summary>
        /// Check a file for existence.
        /// </summary>
        /// <param name="uri">Full file path plus extension.</param>
        /// <returns>TRUE on success otherwise FALSE.</returns>
        public static bool Exists(string uri) => File.Exists(uri);


        /// <summary>
        /// Check if the URI path exists and if not, it will be created.
        /// (Basically a full directory path check-up.)
        /// </summary>
        /// <param name="uri">Full file path plus extension.</param>
        public static void ResolveDirectory(string uri)
        {
            var nfo = new FileInfo(uri);
            if (nfo.Exists == false)
            {
                Directory.CreateDirectory(nfo.Directory.FullName);
            }
        }


        /// <summary>
        /// Loads a string-based file and tries to deserialize it back to the specified type of (T).
        /// </summary>
        /// <remarks>
        /// This call runs asynchronous and can be canceled via cancellation token.
        /// </remarks>
        /// <param name="uri">Full path plus extension</param>
        /// <param name="token">Cancellation token</param>
        /// <param name="progress">Returns progress values while loading the file.</param>
        /// <returns>String otherwise NULL.</returns>
        public static async Task<string> LoadText(string uri) => await LoadText(uri, null, default);
        public static async Task<string> LoadText(string uri, System.Action<float, float> progress = null, CancellationToken token = default)
        {
            try
            {
                using (var stream = new StreamReader(uri, true))
                {
                    var task = stream.ReadToEndAsync();
                    while (task.Status != TaskStatus.RanToCompletion)
                    {
                        if (token.IsCancellationRequested)
                        {
                            stream.Close();
                            stream.Dispose();
                            return null;
                        }
                        progress?.Invoke(stream.BaseStream.Position, stream.BaseStream.Length);
                        await Task.Yield();
                    }
                    stream.Close();
                    stream.Dispose();
                    return task.Result;
                }
            }
            catch (System.Exception e)
            {
                throw new System.Exception(e.Message);
                // Debug.LogError($"{e}");
                // return null;
            }
        }


        /// <summary>
        /// Serializes a object to a string-based file and saves it.
        /// </summary>
        /// <remarks>
        /// This call runs asynchronous and can be canceled via cancellation token.
        /// </remarks>
        /// <param name="uri">Full path plus extension</param>
        /// <param name="data"></param>
        /// <param name="token">Cancellation token</param>
        /// <param name="progress">Returns progress values while loading the file.</param>
        /// <returns>TRUE on success otherwise FALSE.</returns>
        public static async Task<bool> SaveText(string uri, string data) => await SaveText(uri, data, null, default);
        public static async Task<bool> SaveText(string uri, string data, System.Action<float, float> progress = null, CancellationToken token = default)
        {
            ResolveDirectory(uri);
            try
            {
                using (var stream = new StreamWriter(uri))
                {
                    var task = stream.WriteAsync(data);
                    while (task.Status != TaskStatus.RanToCompletion)
                    {
                        if (token.IsCancellationRequested)
                        {
                            stream.Close();
                            stream.Dispose();
                            return false;
                        }

                        progress?.Invoke(stream.BaseStream.Position, stream.BaseStream.Length);
                        await Task.Yield();
                    }
                    stream.Close();
                    stream.Dispose();
                    return true;
                }
            }
            catch (System.Exception e)
            {
                throw new System.Exception(e.Message);
                // Debug.LogError($"{e}");
                // return false;
            }
        }


        /// <summary>
        /// Loads a JSON file and tries to deserialize it back to the specified type of (T).
        /// </summary>
        /// <remarks>
        /// This call runs asynchronous and can be canceled via cancellation token.
        /// </remarks>
        /// <param name="uri">Full path plus extension</param>
        /// <param name="token">Cancellation token</param>
        /// <param name="progress">Returns progress values while loading the file.</param>
        /// <typeparam name="T">Expected type for deserialization.</typeparam>
        /// <returns>Object as typeof(T) otherwise NULL.</returns>
        public static async Task<T> LoadJson<T>(string uri) => await LoadJson<T>(uri, null, null, default);
        public static async Task<T> LoadJson<T>(string uri, System.Action<float, float> progress = null, JsonSerializerSettings settings = null, CancellationToken token = default)
        {
            try
            {
                var result = await LoadText(uri, progress, token);
                return await Goodies.TryToDeserialize<T>(result, settings, token);
            }
            catch (System.Exception e)
            {
                throw new System.Exception(e.Message);
                // Debug.LogError($"{e}");
                // return default;
            }
        }


        /// <summary>
        /// Serializes a data and saves it as a string-based file.
        /// </summary>
        /// <remarks>
        /// This call runs asynchronous and can be canceled via cancellation token.
        /// </remarks>
        /// <param name="uri">Full path plus extension</param>
        /// <param name="data"></param>
        /// <param name="token">Cancellation token</param>
        /// <param name="progress">Returns progress values while loading the file.</param>
        /// <returns>TRUE on success otherwise FALSE.</returns>
        public static async Task<bool> SaveJson(string uri, object data) => await SaveJson(uri, data, null, null, default);
        public static async Task<bool> SaveJson(string uri, object data, JsonSerializerSettings settings) => await SaveJson(uri, data, null, settings, default);
        public static async Task<bool> SaveJson(string uri, object data, System.Action<float, float> progress = null, JsonSerializerSettings settings = null, CancellationToken token = default)
        {
            try
            {
                var json = await Goodies.TryToSerialize(data, settings, token);
                return await SaveText(uri, json, progress, token);
            }
            catch (System.Exception e)
            {
                throw new System.Exception(e.Message);
                // Debug.LogError($"{e}");
                // return false;
            }
        }


    }
}
