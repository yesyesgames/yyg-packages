﻿///////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;

using UnityEngine;
using UnityEngine.Events;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Statics
{
    public sealed class Helper
    {

        [Serializable]
        public class UnityEventBool : UnityEvent<bool>
        {

        }

        private static readonly string m_rootInternal = Path.Combine(Application.dataPath, "Data");
        private static readonly string m_rootMobile = Path.Combine(Application.persistentDataPath, "data");


        public static class FIREBASE
        {

            public readonly static string Symbols_FirebaseBasics = "YYG_FIREBASE_BASICS";
            public readonly static string Symbols_FirebaseDatabase = "YYG_FIREBASE_DATABASE";
            public readonly static string Symbols_FirebaseFunctions = "YYG_FIREBASE_FUNCTIONS";
            public readonly static string Symbols_FirebaseAuthentication = "YYG_FIREBASE_AUTHENTICATION";

            public static class USERS
            {

                public static string Admin => "admin@admin.admin";

            }

            public static class PASSWORDS
            {

                public static string Admin => "1234567890";
                public static string Player => "1234567890";

            }

        }

        public static class IOS
        {

            public static class PATHS
            {

                public static string ROOT_MOBILE => m_rootMobile;
                public static string ROOT_INTERNAL => m_rootInternal;

            }

            public static class DIRNAMES
            {

                //

            }

            public static class FILENAMES
            {

                public static string GameLoca => "gameLoca.txt";
                public static string GameConfig => "gameSettings.txt";
                public static string PlayerConfig => "playerSettings.txt";

            }

            public static class URIS_CLOUD
            {

                public static string GameLoca => "gameLoca";
                public static string GameConfig => "gameSettings";
                public static string PlayerConfig => "playerSettings";

            }

            public static class URIS_LOCAL
            {

                public static string GameLoca => Path.Combine(m_rootMobile, FILENAMES.GameLoca);
                public static string GameConfig => Path.Combine(m_rootMobile, FILENAMES.GameConfig);
                public static string PlayerConfig => Path.Combine(m_rootMobile, FILENAMES.PlayerConfig);

            }

            public static class URIS_INTERNAL
            {

                public static string GameLoca => Path.Combine(m_rootInternal, FILENAMES.GameLoca);
                public static string GameConfig => Path.Combine(m_rootInternal, FILENAMES.GameConfig);
                public static string PlayerConfig => Path.Combine(m_rootInternal, FILENAMES.PlayerConfig);

            }

        }


        public static class CAMERA_PROFILES
        {

            public static string Default => "default";

        }


        public static class RAYCAST_PROFILES
        {

            public static string Default => "default";

        }


        public static class DataPoolKeys
        {

            public static readonly string CanvasScaleFactor = "canvas-scale-factor";
            public static readonly string SceneLoader_LastScene = "custom-play-mode-last-scene";
            public static readonly string SceneLoader_MainScene = "custom-play-mode-main-scene";

            public static string GameLoca => "gameLoca";
            public static string GameConfig => "gameConfig";
            public static string FirebaseUser => "firebaseUser";
            public static string PlayerConfig => "playerConfig";

        }

    }
}
