﻿/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Abstracts
{
    public abstract class SingletonBase<T> : MonoBehaviour where T : MonoBehaviour
    {

        public static T Instance
        {
            get
            {
                if (_instance == null && _quitting == false)
                {
                    _instance = GameObject.FindObjectOfType<T>();
                    if (_instance == null)
                    {
                        GameObject go = new(typeof(T).ToString());
                        _instance = go.AddComponent<T>();
                        DontDestroyOnLoad(_instance.gameObject);
                    }
                }

                return _instance;
            }
        }

        private static T _instance;
        private static bool _quitting;


        protected virtual void OnApplicationQuit() => _quitting = true;

        protected virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = gameObject.GetComponent<T>();
            }
            else if (_instance.GetInstanceID() != GetInstanceID())
            {
                Destroy(gameObject);
                throw new System.Exception($"Instance of '{GetType().FullName}' already exists, removing '{ToString()}'.");
            }
        }



    }
}
