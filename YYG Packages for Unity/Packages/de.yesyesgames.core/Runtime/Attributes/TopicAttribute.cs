﻿/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Core.Attributes
{
    public class TopicAttribute : PropertyAttribute
    {

        public readonly string topic;
        public TopicAttribute(string header) => topic = header;

    }
}
