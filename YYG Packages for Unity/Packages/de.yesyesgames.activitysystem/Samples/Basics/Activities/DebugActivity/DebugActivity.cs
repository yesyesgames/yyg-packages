﻿///////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using TMPro;

using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Objects;
///////////////////////////////////////////////////////////////////////////////
public class DebugActivity : ActivityBase
{

    [Header("DebugActivity")]
    [Header("Declares")]
    [SerializeField]
    private TMP_Text consoleLog;

    [SerializeField]
    private GameObject banner;

    [SerializeField]
    private UIStats uiStats;

    [Header("Settings")]
    [SerializeField]
    private DebugSettingsObject settings = default;

    private bool _running;
    private Queue<string> _messages;


    protected override void Awake()
    {
        base.Awake();
        consoleLog.SetText(string.Empty);
        _messages = new Queue<string>();
    }

    private void Update()
    {
        banner.SetActive(settings.showBanner);
        uiStats.AddVersion(settings.showVersion).AddFPS(settings.showFPS).Draw();
    }


    private void OutputMessages()
    {
        string tmp = string.Empty;
        _messages.ToList().ForEach(x => { tmp += $"{x}\n"; });
        consoleLog.SetText(tmp);
    }

    public void Log(ScriptableObject data)
    {
        if (data is LogObject log)
        {
            string text = $"<color=#{ColorUtility.ToHtmlStringRGB(settings.TimeColor)}>{DateTime.Now:hh:mm:ss}</color>: ";
            switch (log.type)
            {
                default:
                    text += $"<color=#{ColorUtility.ToHtmlStringRGB(settings.DBGColor)}>{log.message}</color>";
                    break;

                case DebugTypes.WRN:
                    text += $"<color=#{ColorUtility.ToHtmlStringRGB(settings.WRNColor)}>{log.message}</color>";
                    break;

                case DebugTypes.ERR:
                    text += $"<color=#{ColorUtility.ToHtmlStringRGB(settings.ERRColor)}>{log.message}</color>";
                    break;
            }

            _messages.Enqueue(text);

            if (_messages.Count > settings.MaxLines)
                _messages.Dequeue();

            if (!_running)
                StartCoroutine(DecayCycle());

            OutputMessages();
        }
    }

    private IEnumerator DecayCycle()
    {
        _running = true;
        while (_messages.Count > 0)
        {
            yield return new WaitForSecondsRealtime(settings.MSGDecay);
            _messages.Dequeue();
            OutputMessages();
        }

        _running = false;
    }

}
