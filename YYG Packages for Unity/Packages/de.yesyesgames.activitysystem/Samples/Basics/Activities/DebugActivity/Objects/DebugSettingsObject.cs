﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
[CreateAssetMenu(fileName = "DebugSettings.", menuName = "YesYesGames/Debugging/New DebugSettingsObject")]
public class DebugSettingsObject : ScriptableObject
{

    internal int MaxLines => maxMessages;
    internal float MSGDecay => decayTime;
    internal Color TimeColor => timeColor;
    internal Color DBGColor => dbgColor;
    internal Color WRNColor => wrnColor;
    internal Color ERRColor => errColor;

    public bool showFPS = true;
    public bool showVersion = true;
    public bool showBanner = true;

    [Space]
    [SerializeField, Range(1, 128)]
    private int maxMessages = 42;
    [SerializeField, Range(1, 10)]
    private float decayTime = 4f;

    [Space]
    [SerializeField]
    private Color timeColor = new(.65f, .65f, .65f, 1);

    [Space]
    [SerializeField]
    private Color dbgColor = new(.95f, .95f, .95f, 1);
    [SerializeField]
    private Color wrnColor = new(.95f, .45f, .15f, 1);
    [SerializeField]
    private Color errColor = new(.95f, .05f, .25f, 1);


}