﻿///////////////////////////////////////////////////////////////////////////////
using TMPro;

using UnityEngine;

using YYG.Packages.Unity.Utilities;
///////////////////////////////////////////////////////////////////////////////
public class UIStats : MonoBehaviour
{

    [SerializeField]
    private TMP_Text label;
    [SerializeField]
    private VersionModelObject model;

    private string _out;


    private void OnEnable()
    {
        label.SetText(string.Empty);
    }


    internal void Draw()
    {
        gameObject.SetActive(!string.IsNullOrEmpty(_out));
        if (gameObject.activeSelf == false)
            return;

        label.SetText($"<mark=#00000055>{_out}</mark>");
        _out = string.Empty;
    }

    internal UIStats AddFPS(bool add = true)
    {
        if (add == false)
            return this;

        var fps = Time.frameCount / Time.time;
        _out += $"FPS: {fps:F2}\n";
        return this;
    }

    internal UIStats AddVersion(bool add = true)
    {
        if (add == false)
            return this;

        if (model == null)
        {
            _out += $"Version: Version data NULL or invalid.\n";
            return this;
        }
        _out += $"Version: {model.VersionData.Output}\n";
        return this;
    }

}
