///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.ActivitySystem.Samples.Objects;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Samples
{
    public class ActivityStart : ComponentBase
    {

        public static event System.Action OnInitiateComplete;

        [Topic("ActivityStart")]
        [Header("Declares")]
        [SerializeField]
        private Param startActivity;


        protected virtual void Start()
        {
            Initiate(new DataPackage().Set(new InitiateDataPackage()
            {
                OnComplete = OnInitiateComplete
                //
            }));
        }

        protected virtual void Initiate(DataPackage package = null) => Switch(startActivity, package);


    }
}
