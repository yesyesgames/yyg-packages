﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Objects;
using YYG.Packages.Unity.AppEvents.Objects;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.Core.Statics;
using YYG.Packages.Unity.ModuleSystem;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Abstracts
{
    [RequireComponent(typeof(LoggingModule))]
    [RequireComponent(typeof(ModuleManager))]
    public abstract class ActivityMono : MonoBehaviour
    {

        #region DECLARES


        protected IModuleManager Modules { get; private set; }

        [Topic("ActivityMono")]
        [Header("Settings")]
        [SerializeField]
        private DebugLevel debugLevel = DebugLevel.Warnings | DebugLevel.Errors;

        [Space]
        [ReadOnly]
        [SerializeField]
        private AppEvent switchEvent;

        private const string AssetName1 = "AppEvent.ActivitySystem.SwitchActivity";


        #endregion



        #region MONO_METHODS


        protected virtual void Awake()
        {
            Modules = GetComponent<IModuleManager>();
        }


        protected virtual void OnValidate()
        {
#if UNITY_EDITOR
            Goodies.FindAsset<AppEvent>(AssetName1, out switchEvent);
#endif
        }


        #endregion



        #region PUBLIC_METHODS


        public virtual void Back() => Back(null);

        protected void Back(DataPackage dataPackage = null)
        {
            switchEvent.Raise(ScriptableObject.CreateInstance<ActivityObject>().Set(null, ActivityActions.Back, dataPackage));
        }


        protected void Switch(Param activity, DataPackage dataPackage = null)
        {
            switchEvent.Raise(ScriptableObject.CreateInstance<ActivityObject>().Set(activity, ActivityActions.Switch, dataPackage));
        }


        protected bool ValidateDataPackage<T>(DataPackage package)
        {
            return package?.Custom is T;
        }

        protected bool ValidateDataPackage<T>(DataPackage package, out T data)
        {
            data = default;
            if (package?.Custom is T value)
            {
                data = value;
                return true;
            }
            return false;
        }


        protected void DBG(object message)
        {
            if (debugLevel.HasFlag(DebugLevel.Debugs))
            {
                Modules.Get<ILoggingModule>().DBG(message.ToString(), this);
            }
        }


        protected void WRN(string message)
        {
            if (debugLevel.HasFlag(DebugLevel.Warnings))
                Modules.Get<ILoggingModule>().WRN(message, this);
        }


        protected void ERR(string message)
        {
            if (debugLevel.HasFlag(DebugLevel.Errors))
                Modules.Get<ILoggingModule>().ERR(message, this);
        }


        #endregion



        #region PRIVATE_METHODS


        [ContextMenu("Reset debug level")]
        private void ResetDebugLevel() => debugLevel = DebugLevel.Warnings | DebugLevel.Errors;


        #endregion



    }
}
