﻿///////////////////////////////////////////////////////////////////////////////
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ModuleSystem.Interfaces;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Abstracts
{
    /// <summary>
    /// Base class to create new Activities from.
    /// It also handles the ModuleSystem.
    /// </summary>
    public abstract class ActivityBase : ActivityMono
    {
        public virtual void OnInit(DataPackage package = null) => ((IModuleManagerInternal)Modules)?.OnInitInternal();
        public virtual void OnKill(DataPackage package = null) => ((IModuleManagerInternal)Modules)?.OnKillInternal();
        public virtual void OnFocus(bool hasFocus, DataPackage package = null) => ((IModuleManagerInternal)Modules)?.OnFocusInternal(hasFocus);
    }
}
