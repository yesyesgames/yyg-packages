﻿///////////////////////////////////////////////////////////////////////////////
using YYG.Packages.Unity.ModuleSystem.Interfaces;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Abstracts
{
    /// <summary>
    /// Allows to use the & ModuleSystem without the need to have an Activity created.
    /// </summary>
    public abstract class ComponentBase : ActivityMono
    {

        protected virtual void OnApplicationFocus(bool hasFocus) => ((IModuleManagerInternal)Modules)?.OnFocusInternal(hasFocus);

        protected override void Awake()
        {
            base.Awake();
            ((IModuleManagerInternal)Modules)?.OnInitInternal();
        }

    }
}
