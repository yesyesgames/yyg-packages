using UnityEngine;
using UnityEngine.UI;

using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.Core.Statics;

namespace YYG.Packages
{
    public class LayoutBleeding : MonoBehaviour
    {

        [Header("LayoutBleeding")]
        [SerializeField, Header("Settings")]
        private LayoutElement top;

        [SerializeField]
        private LayoutElement left;

        [SerializeField]
        private LayoutElement right;

        [SerializeField]
        private LayoutElement bottom;

        [SerializeField, Header("Settings")]
        private bool enableBleeding;

        [SerializeField]
        private bool debugBleeding;

        [SerializeField]
        private Color bleedingColor = Color.black;

        [Header("DEBUGGING"), Range(0, 100)]
        public float simulate = 42;


        private ScreenOrientation _screenOrientation;


        private void OnEnable() => Calculate();
        private void OnValidate() => Simulate();


        private void Awake()
        {
            _screenOrientation = Screen.orientation;
            top.GetComponent<Image>().color = debugBleeding ? Color.green : bleedingColor;
            left.GetComponent<Image>().color = debugBleeding ? Color.green : bleedingColor;
            right.GetComponent<Image>().color = debugBleeding ? Color.green : bleedingColor;
            bottom.GetComponent<Image>().color = debugBleeding ? Color.green : bleedingColor;
        }


        private void Update()
        {
            if (_screenOrientation.Equals(Screen.orientation) == false)
            {
                _screenOrientation = Screen.orientation;
                Calculate();
            }
        }


        private void Simulate()
        {
            if (Application.isPlaying)
                return;

            Awake();
            var screen = Screen.currentResolution;
            var l = simulate;
            var b = simulate;
            var r = screen.width - (screen.width - simulate);
            var t = screen.height - (screen.height - simulate);
            Apply(t, l, r, b);
        }


        private void Calculate()
        {
            if (enableBleeding)
            {
                var sArea = Screen.safeArea;
                var scale = DataPool.Instance.Pull<float>(Helper.DataPoolKeys.CanvasScaleFactor);

                var l = sArea.x;
                var b = sArea.y;
                var r = Screen.width - (sArea.x + sArea.width);
                var t = Screen.height - (sArea.y + sArea.height);
                Apply(t / scale, l / scale, r / scale, b / scale);
            }
            else
            {
                Apply(0, 0, 0, 0);
            }
        }


        private void Apply(float t, float l, float r, float b)
        {
            top.preferredHeight = t;
            left.preferredWidth = l;
            right.preferredWidth = r;
            bottom.preferredHeight = b;
        }

    }
}
