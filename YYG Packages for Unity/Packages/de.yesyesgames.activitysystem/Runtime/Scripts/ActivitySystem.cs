﻿///////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.ActivitySystem.Objects;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.Core.Statics;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem
{
    /// <summary>
    /// A basic UI system to create activities from a collection as a replacement or overlay.
    /// </summary>
    public class ActivitySystem : ComponentBase
    {

        #region DECLARES


        /// <summary>
        /// Experimental.
        /// </summary>
        public static float ScaleFactor => DataPool.Instance.Pull<float>(Helper.DataPoolKeys.CanvasScaleFactor);


        [Header("ActivitySystem")]
        [SerializeField, Header("Declares")]
        private Canvas appCanvas;

        [SerializeField, Header("Settings")]
        private ActivityCollectionObject activityCollection;

        [SerializeField, Space]
        [Tooltip("Enable this to add a frame between Activity switches.")]
        private bool addWaitFrame;

        private ActivityBase _currentActivity;
        private readonly Stack<ActivityBase> _overlayActivities = new Stack<ActivityBase>();


        #endregion



        #region MONO_METHODS


        protected override void Awake()
        {
            base.Awake();
            // TODO: Oh, that's interesting?! I added this into the system... Well, okay. :-)
            DataPool.Instance.Push<float>(Helper.DataPoolKeys.CanvasScaleFactor, appCanvas.scaleFactor);
        }


        #endregion



        #region PUBLIC_METHODS


        public void Switch(ScriptableObject data)
        {
            if (data is ActivityObject activityObj)
            {
                switch (activityObj.Behaviour)
                {
                    case ActivityActions.Switch:
                        SwitchActivity(activityObj.Label, activityObj.DataPackage);
                        break;
                    case ActivityActions.Back:
                        StartCoroutine(CloseOverlay(activityObj.DataPackage));
                        break;
                }
            }
        }


        #endregion



        #region PRIVATE_METHODS


        private void SwitchActivity(Param label, DataPackage package)
        {
            DBG($"ActivitySystem::SwitchActivity({label}, {package})");
            if (activityCollection == null)
            {
                ERR($"ActivitySystem::SwitchActivity(): Activity collection NULL or invalid.");
                return;
            }

            var specs = activityCollection.Activities.Find(x => x.Label == label);
            if (specs == null)
            {
                ERR($"ActivitySystem::SwitchActivity(): Activity '{label}' NULL or invalid.");
                return;
            }

            if (specs.Behaviour == ActivityBehaviours.Replace && _overlayActivities.Count > 0)
            {
                WRN($"ActivitySystem::SwitchActivity(): Can not switch Activity since it has {_overlayActivities.Count} active Overlay(s).");
                return;
            }

            switch (specs.Behaviour)
            {
                case ActivityBehaviours.Replace:
                    {
                        StartCoroutine(CreateReplacement(specs, package));
                        break;
                    }
                case ActivityBehaviours.Overlay:
                    {
                        StartCoroutine(CreateOverlay(specs, package));
                        break;
                    }
            }
        }


        private IEnumerator CloseOverlay(DataPackage package)
        {
            DBG($"ActivitySystem::CloseOverlay({package})");
            if (_overlayActivities.Count <= 0)
            {
                DBG($"ActivitySystem::CloseOverlay(): No Overlay available; skipping.");
                yield break;
            }

            var overlay = _overlayActivities.Pop();
            overlay.OnFocus(false, package);
            overlay.OnKill(package);
            Destroy(overlay.gameObject);

            if (addWaitFrame)
            {
                DBG($"ActivitySystem::CloseOverlay(): WaitFrame");
                yield return new WaitForEndOfFrame();
            }

            if (_overlayActivities.Count > 0)
            {
                DBG($"ActivitySystem::CloseOverlay(): Focus previous Overlay.");
                _overlayActivities.ElementAt(0).OnFocus(true, package);
            }
            else
            {
                DBG($"ActivitySystem::CloseOverlay(): Focus latest Activity.");
                _currentActivity.OnFocus(true, package);
            }
        }


        private IEnumerator CreateReplacement(ActivitySpecificationObject specs, DataPackage package)
        {
            DBG($"ActivitySystem::CreateReplacement({specs}, {package})");

            // Kill the current/active Activity.
            if (_currentActivity != null)
            {
                _currentActivity.OnFocus(false, package);
                _currentActivity.OnKill(package);
                Destroy(_currentActivity.gameObject);
            }

            if (addWaitFrame)
            {
                yield return new WaitForEndOfFrame();
            }

            // Create the new one.
            _currentActivity = Instantiate(specs.Activity, appCanvas.transform, false);
            _currentActivity.OnInit(package);
            _currentActivity.OnFocus(true, package);
        }


        private IEnumerator CreateOverlay(ActivitySpecificationObject specs, DataPackage package)
        {
            DBG($"ActivitySystem::CreateOverlay({specs}, {package})");

            if (_overlayActivities.Count > 0)
            {
                DBG($"ActivitySystem::CreateOverlay(): Defocus previous Overlay.");
                _overlayActivities.ElementAt(0).OnFocus(false, package);
            }
            else
            {
                DBG($"ActivitySystem::CreateOverlay(): Defocus current Activity.");
                _currentActivity.OnFocus(false, package);
            }

            if (addWaitFrame)
            {
                DBG($"ActivitySystem::CreateOverlay(): WaitFrame.");
                yield return new WaitForEndOfFrame();
            }

            DBG($"ActivitySystem::CreateOverlay(): Create Overlay.");

            // Create the overlay.
            var overlay = Instantiate(specs.Activity, appCanvas.transform, false);
            overlay.OnInit(package);
            overlay.OnFocus(true, package);
            _overlayActivities.Push(overlay);
        }


        #endregion



    }
}
