﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.Serialization;

using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.ActivitySystem.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Objects
{
    [CreateAssetMenu(fileName = "ActivitySpecification.", menuName = "YesYesGames/Packages for Unity/ActivitySystem/New ActivitySpecificationObject")]
    public class ActivitySpecificationObject : ScriptableObject
    {

        internal Param Label => label;
        internal ActivityBase Activity => activity;
        internal ActivityBehaviours Behaviour => behaviour;

        [SerializeField]
        [FormerlySerializedAs("m_label")]
        private Param label;

        [SerializeField]
        [FormerlySerializedAs("m_activity")]
        private ActivityBase activity;

        [SerializeField]
        [FormerlySerializedAs("m_behaviour")]
        private ActivityBehaviours behaviour;

    }
}
