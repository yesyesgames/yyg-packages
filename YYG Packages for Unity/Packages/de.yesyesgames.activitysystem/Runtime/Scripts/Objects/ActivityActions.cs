﻿namespace YYG.Packages.Unity.ActivitySystem.Objects
{
    // TODO: There is another one doing basically the same. Why?
    /// <seealso cref="ActivityBehaviours"/>
    public enum ActivityActions
    {
        /// <summary>
        /// Will replace the current Activity.
        /// </summary>
        Switch = 0,

        /// <summary>
        /// Will close the latest overlay Activity.
        /// </summary>
        Back = 1,
    }
}
