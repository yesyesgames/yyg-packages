﻿namespace YYG.Packages.Unity.ActivitySystem.Objects
{
    // TODO: There is another one doing basically the same. Why?
    /// <seealso cref="ActivityActions"/>
    public enum ActivityBehaviours
    {
        /// <summary>
        /// The previous Activity will be closed before the new one gets created.
        /// </summary>
        Replace = 0,

        /// <summary>
        /// The current Activity stays and the new one is handled as an overlay on top.
        /// </summary>
        Overlay = 1,
    }
}
