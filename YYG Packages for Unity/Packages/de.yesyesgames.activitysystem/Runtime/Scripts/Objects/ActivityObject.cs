﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.Serialization;

using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Objects
{
    public class ActivityObject : ScriptableObject
    {

        internal Param Label => label;
        internal DataPackage DataPackage { get; private set; }
        internal ActivityActions Behaviour => behaviour;

        [SerializeField]
        [FormerlySerializedAs("m_label")]
        private Param label;

        [SerializeField]
        [FormerlySerializedAs("m_behaviour")]
        private ActivityActions behaviour;


        internal ActivityObject Set(Param label, ActivityActions behaviour, DataPackage data = null)
        {
            this.label = label;
            this.behaviour = behaviour;
            DataPackage = data;
            return this;
        }

    }
}
