﻿///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Serialization;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem.Objects
{
    [CreateAssetMenu(fileName = "ActivityCollection.", menuName = "YesYesGames/Packages for Unity/ActivitySystem/New ActivityCollectionObject")]
    public class ActivityCollectionObject : ScriptableObject
    {

        internal List<ActivitySpecificationObject> Activities => activities;

        [Header("ActivityCollectionObject")]
        [SerializeField, Header("Settings")]
        [FormerlySerializedAs("m_activities")]
        private List<ActivitySpecificationObject> activities;

    }
}
