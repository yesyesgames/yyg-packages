///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.UI;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem
{
    public class UISlider : UILabel
    {

        public string Text => base.Get;

#pragma warning disable
        public float Get => slider.value;

        [Header("UISlider")]
        [SerializeField, Header("Declares")]
        private Slider slider;


        protected override void Reset()
        {
            base.Reset();
            slider = GetComponentInChildren<Slider>();
        }


        public new UISlider Set(bool visibility)
        {
            gameObject.SetActive(visibility);
            return this;
        }


        public UISlider Set(float value, bool add = true)
        {
            slider.value = add ? Get + value : value;
            return this;
        }


        public UISlider Set(float min, float max, float value = 0)
        {
            slider.minValue = min;
            slider.maxValue = max;
            slider.value = value;
            return this;
        }



    }
}
