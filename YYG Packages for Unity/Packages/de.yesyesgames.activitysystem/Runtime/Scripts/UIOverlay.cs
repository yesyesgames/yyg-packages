using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.Extensions;

#if YYG_USE_DOTWEEN
using DG.Tweening;
#endif

namespace YYG.Packages.Unity.ActivitySystem
{
    public class UIOverlay : MonoBehaviour
    {

        [SerializeField]
        private CanvasGroup blackFade;
        [SerializeField]
        private Animation indicatorAnim;
        [SerializeField]
        private CanvasGroup indicatorGroup;

#if YYG_USE_DOTWEEN
        [SerializeField, Space]
        internal float time = .64f;

        [SerializeField]
        internal Ease easing = Ease.InOutQuint;
#endif


        public void SetFade(bool toBlack)
        {
            var to = toBlack ? 1 : 0;
            blackFade.alpha = to;
        }


        public void BlockRaycats(bool state)
        {
            blackFade.blocksRaycasts = state;
        }


        public void Indicator(bool isVisible)
        {
#if YYG_USE_DOTWEEN
            indicatorGroup.DOKill();
#endif
            if (isVisible)
            {
                indicatorAnim.Active();
                indicatorAnim.Play();
#if YYG_USE_DOTWEEN
                indicatorGroup.DOFade(1, .42f).SetEase(Ease.InExpo);
#else
                indicatorGroup.alpha = 1;
#endif
            }
            else
            {
#if YYG_USE_DOTWEEN
                indicatorGroup.DOFade(0, .42f).OnComplete(() =>
                {
                    indicatorAnim.Inactive();
                    indicatorAnim.Stop();
                }).SetEase(Ease.OutCirc);
#else
                indicatorAnim.Stop();
                indicatorAnim.Inactive();
                indicatorGroup.alpha = 0;
#endif
            }
        }


        public async Task DoFade(bool fromBlack, bool toBlack, bool blockRaycasts = true)
        {
            if (blackFade == null)
            {
                return;
            }

            BlockRaycats(blockRaycasts);

            var to = toBlack ? 1 : 0;
            var from = fromBlack ? 1 : 0;

            blackFade.alpha = from;

#if YYG_USE_DOTWEEN
            blackFade.DOKill();
            await blackFade.DOFade(to, time).SetEase(easing).AsyncWaitForCompletion();
#else
            blackFade.alpha = to;
#endif

        }

    }
}
