///////////////////////////////////////////////////////////////////////////////
using System.Text.RegularExpressions;

using TMPro;

using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem
{
    public class UILabel : MonoBehaviour
    {

        public string Get { set => label.text = value; get => label.text; }

        [Header("UILabel")]
        [Header("Declares")]
        [SerializeField]
        private TMP_Text label;
        [SerializeField]
        private LocalizeStringEvent localizeString;

        [Header("Settings")]
        [SerializeField]
        private float fontSize = 42f;


        protected void OnValidate() => Refresh();
        // private void Localize(string text) => Set(text);
        // private void OnEnable() => localizeString.StringReference.StringChanged += Localize;
        // private void OnDisable() => localizeString.StringReference.StringChanged -= Localize;

        protected virtual void Reset()
        {
            label = GetComponentInChildren<TMP_Text>();
            localizeString = GetComponentInChildren<LocalizeStringEvent>();
        }


        public void Set(LocalizedString stringReference) => localizeString.StringReference = stringReference;

        public UILabel Set(string text, bool unEscape = false)
        {
            label.SetText(unEscape ? Regex.Unescape(text) : text);
            return this;
        }

        public UILabel Set(bool visibility)
        {
            gameObject.SetActive(visibility);
            return this;
        }


        private void Refresh()
        {
            if (label == null)
                return;

            label.fontSize = fontSize;
        }

    }
}
