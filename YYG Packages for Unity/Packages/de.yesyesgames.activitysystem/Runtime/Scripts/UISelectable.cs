///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivitySystem
{
    public class UISelectable : UILabel
    {

        [Header("UISelectable")]
        [Header("Declares")]
        [SerializeField]
        private Selectable selectable;


        protected override void Reset()
        {
            base.Reset();
            selectable = GetComponentInChildren<Selectable>();
        }


        public UISelectable Set(UnityAction action)
        {
            if (selectable is Button button)
                button.onClick.AddListener(action);
            return this;
        }

        public UISelectable Set(UnityAction<bool> action)
        {
            if (selectable is Toggle toggle)
                toggle.onValueChanged.AddListener(action);
            return this;
        }

        public UISelectable Set(bool state, bool silent = false)
        {
            if (selectable is Toggle toggle)
            {
                if (silent)
                    toggle.SetIsOnWithoutNotify(state);
                else
                    toggle.isOn = state;
            }
            return this;
        }




    }
}
