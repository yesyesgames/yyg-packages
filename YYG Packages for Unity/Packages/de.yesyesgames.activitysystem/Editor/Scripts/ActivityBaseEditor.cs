///////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.Core.Editor.Abstracts;
///////////////////////////////////////////////////////////////////////////////
[CustomEditor(typeof(ActivityMono), true)]
public class MonoBaseEditor : EditorBase
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GetScriptTarget<ActivityMono>(out var script, out var isPlaymode))
        {
            return;

            // System.Type myTypeA = typeof(MonoBase);
            // System.Reflection.FieldInfo myFieldInfo = myTypeA.GetField("Version");
            // if (myFieldInfo == null)
            //     return;
            // 
            // var version = myFieldInfo.GetValue(myFieldInfo) as string;
            // var margin = 5f;
            // var width = EditorGUIUtility.currentViewWidth - margin; ;
            // GUI.Label(new Rect(0, 25, width, 50), new GUIContent()
            // {
            //     text = version,
            // }, new GUIStyle()
            // {
            //     fontSize = 10,
            //     alignment = TextAnchor.UpperRight,
            //     normal = new GUIStyleState()
            //     {
            //         textColor = Color.gray,
            //     }
            // });

        }
    }
}
