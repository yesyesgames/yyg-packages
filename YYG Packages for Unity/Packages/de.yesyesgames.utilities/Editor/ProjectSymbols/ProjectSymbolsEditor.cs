///////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities.Editor
{
    [CustomEditor(typeof(ProjectSymbols))]
    public class ProjectSymbolsEditor : EditorBase
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GetScriptTarget<ProjectSymbols>(out var script, out var isPlaying);

            DrawButton("Pull", script.Pull, !isPlaying);
            DrawButton("Push", script.Push, !isPlaying);

            DrawGap();
            DrawButton("Copy", script.Copy, !isPlaying);
        }

    }
}
