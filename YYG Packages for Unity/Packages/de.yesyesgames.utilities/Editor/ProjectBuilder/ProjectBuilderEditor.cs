﻿///////////////////////////////////////////////////////////////////////////////
using System.Linq;

using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;

using UnityEngine;

using YYG.Packages.Unity.Core.Editor.Abstracts;
using YYG.Packages.Unity.Core.Statics;
using YYG.Packages.Utilities.ProjectSymbols;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities.Editor
{
    [CustomEditor(typeof(ProjectBuilder))]
    public class ProjectBuilderEditor : EditorBase
    {
        private const string BuildPath = "Builds/";


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GetScriptTarget<ProjectBuilder>(out var script, out var isPlaying);

            BeginVertical();
            BeginSection("Options");

            var label = string.Empty;
            switch (script.Platform)
            {
                default:
                    label = "Build Default";
                    break;

                case PlatformTargets.Android:
                    label = "Build APK";
                    break;

                case PlatformTargets.iOS:
                    label = "Build XCode";
                    break;

                case PlatformTargets.Windows64:
                    label = "Build Standalone";
                    break;
            }

            DrawButton(label, () => BuildProject(script), !isPlaying);
            DrawButton("Reveal build folder", () => EditorUtility.RevealInFinder(BuildPath), !isPlaying);

            EndSection();
            EndVertical();
        }


        private string GetDefaultURI(BuilderSettings settings, VersionModel versionModel)
        {
            var path = settings.IncludeVersion ?
                System.IO.Path.Combine($"{versionModel.PackageIdentifier}", $"{versionModel.ProductName} - {versionModel.VersionBuildCode}") :
                System.IO.Path.Combine($"{versionModel.PackageIdentifier}", $"{versionModel.ProductName}");

#if UNITY_ANDROID
            return EditorUserBuildSettings.buildAppBundle ?
                System.IO.Path.Combine(BuildPath, $"Android/{path}.aab") :
                System.IO.Path.Combine(BuildPath, $"Android/{path}.apk");
#elif UNITY_IOS
            // TODO: FIXME: Path extension missing?
            return System.IO.Path.Combine(BuildPath, $"iOS/{path}");
#elif UNITY_STANDALONE_WIN
            return System.IO.Path.Combine(BuildPath, $"Standalone/{path}.exe");
#endif

            throw new CustomExceptions.FeatureNotYetImplementedException($"Build target {Application.platform}");
        }


        private string GetAdHocURI(VersionModel versionModel)
        {
            var path = $"AdHoc";

#if UNITY_ANDROID
            return EditorUserBuildSettings.buildAppBundle ?
                System.IO.Path.Combine(BuildPath, $"Android/{path}/adhoc.aab") :
                System.IO.Path.Combine(BuildPath, $"Android/{path}/adhoc.apk");
#elif UNITY_IOS
            // TODO: FIXME: Is something missing?
            return System.IO.Path.Combine(BuildPath, $"iOS/{path}/adhoc");
#elif UNITY_STANDALONE_WIN
            return System.IO.Path.Combine(BuildPath, $"Standalone/{path}/adhoc.exe");
#endif

            throw new CustomExceptions.FeatureNotYetImplementedException($"Build target {Application.platform}");
        }


        private BuildTarget GetBuildTarget(PlatformTargets platform)
        {
            switch (platform)
            {
                default: return EditorUserBuildSettings.activeBuildTarget;

                case PlatformTargets.iOS: return BuildTarget.iOS;
                case PlatformTargets.Android: return BuildTarget.Android;
                case PlatformTargets.Windows64: return BuildTarget.StandaloneWindows64;
            }
        }


        private BuildTargetGroup GetBuildTargetGroup(PlatformTargets platform)
        {
            switch (platform)
            {
                default: return EditorUserBuildSettings.selectedBuildTargetGroup;

                case PlatformTargets.iOS: return BuildTargetGroup.iOS;
                case PlatformTargets.Android: return BuildTargetGroup.Android;
                case PlatformTargets.Windows64: return BuildTargetGroup.Standalone;
            }
        }


        private void BuildProject(ProjectBuilder builder)
        {
            PlayerPrefs.SetString("currentScenePath", EditorSceneManager.GetActiveScene().path);
            PlayerPrefs.Save();

            if (builder.Settings.IncrementBuildNumber)
            {
                builder.ProjectVersion.BuildNumber++;
            }

            // TODO: It might be much better to just use the settings made by the user within the BuildWindow...
            // But, the idea is to setup everything at one place... Well...
            var bpo = new BuildPlayerOptions()
            {
                target = GetBuildTarget(builder.Platform),
                targetGroup = GetBuildTargetGroup(builder.Platform),
                scenes = (from scene in EditorBuildSettings.scenes where scene.enabled select scene.path).ToArray(),
            };

            if (builder.Settings.BuildAndRun)
                bpo.options |= BuildOptions.AutoRunPlayer;

            if (builder.Settings.DevelopmentBuild)
                bpo.options |= BuildOptions.Development;

            if (builder.Settings.AppBundle)
            {
                EditorUserBuildSettings.buildAppBundle = true;
                // TODO: Necessary?
                EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
            }

            if (builder.Settings.AdHocBuild)
            {
                bpo.locationPathName = GetAdHocURI(builder.ProjectVersion);
            }
            else
            {
                bpo.locationPathName = GetDefaultURI(builder.Settings, builder.ProjectVersion);
            }

            var report = BuildPipeline.BuildPlayer(bpo);
            var summary = report.summary;

            switch (summary.result)
            {
                default:
                    Debug.Log($"<b>Build '{summary.result}' but don't worry, Jim! He was dead already...</b>");
                    if (builder.Settings.IncrementBuildNumber)
                    {
                        builder.ProjectVersion.BuildNumber--;
                    }
                    break;

                case BuildResult.Succeeded:
                    Debug.Log($"<b>Build succeeded. Have a wonderful day! :-)</b>");
                    break;
            }

            var pVersion = FindObjectOfType<Unity.Utilities.ProjectVersion.ProjectVersion>();
            if (pVersion == null)
            {
                Debug.LogWarning("Could not save version changes. Forgot to add ProjectVersion?");
            }
            else
            {
                pVersion.Save();
            }

            if (summary.result != BuildResult.Succeeded)
            {
                return;
            }

            var path = PlayerPrefs.GetString("currentScenePath", string.Empty);
            if (string.IsNullOrEmpty(path))
            {
                return;
            }

            EditorSceneManager.OpenScene(path);
        }
    }
}
