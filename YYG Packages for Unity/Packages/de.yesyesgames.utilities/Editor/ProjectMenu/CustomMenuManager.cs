using UnityEditor;

using UnityEngine;

public class GenericMenuExample : EditorWindow
{

    [MenuItem("Project/Open Scenes", false, 52)]
    private static void OpenScenes()
    {
        EditorWindow window = GetWindow<GenericMenuExample>();
        window.position = new Rect(50f, 50f, 200f, 24f);
        window.Show();
    }


    private void OnEnable()
    {
        titleContent = new GUIContent("Project Scenes");
    }


    private void AddMenuItemForColor(GenericMenu menu, string menuPath)
    {
        menu.AddItem(new GUIContent(menuPath), true, OnColorSelected, "hello world");
    }


    private void OnColorSelected(object data)
    {
        if (data is string str)
        {
            Debug.Log(str);
        }
    }


    private void OnGUI()
    {
        if (GUILayout.Button("Select GUI Color"))
        {
            GenericMenu menu = new GenericMenu();

            AddMenuItemForColor(menu, "RGB/Red");

            menu.AddSeparator("");

            menu.AddSeparator("CMYK/");

            menu.AddSeparator("");

            menu.ShowAsContext();
        }
    }
}