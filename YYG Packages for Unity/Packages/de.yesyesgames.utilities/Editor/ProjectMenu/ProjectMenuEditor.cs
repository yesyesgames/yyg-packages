/////////////////////////////////////////////////////////////////////////////////
using UnityEditor;
using UnityEditor.SceneManagement;

using UnityEngine;

using YYG.Packages.Unity.Core.Editor.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities.Editor
{
    [InitializeOnLoad]
    public class ProjectMenuEditor : EditorBase
    {

        private const string TRUE = "true";
        private const string FALSE = "false";
        private const string LAST_SCENE = "LAST";
        private const string WAS_PLAYED_FROM_MAIN_SCENE = "USED";


        static ProjectMenuEditor()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }


#if YYG_USE_PROJECTMENU

        [MenuItem("Project/Play from Main", false, 0)]
        private static void PlayFromMain()
        {
            var last = EditorSceneManager.GetActiveScene().name;
            PlayerPrefs.SetString(LAST_SCENE, last);

            var path = FindScene(ProjectSceneTypes.Main.ToString());

            PlayerPrefs.SetString(WAS_PLAYED_FROM_MAIN_SCENE, TRUE);

            EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
            EditorApplication.EnterPlaymode();
        }


        [MenuItem("Project/Open Main", false, 11)]
        private static void OpenMain() => Open(ProjectSceneTypes.Main);

        [MenuItem("Project/Open UI", false, 30)]
        private static void OpenUI() => Open(ProjectSceneTypes.UI);

        [MenuItem("Project/Open Core", false, 31)]
        private static void OpenCore() => Open(ProjectSceneTypes.Core);

        [MenuItem("Project/Open Level", false, 32)]
        private static void OpenLevel() => Open(ProjectSceneTypes.Level);

        [MenuItem("Project/Open Camera", false, 33)]
        private static void OpenCamera() => Open(ProjectSceneTypes.Camera);

        [MenuItem("Project/Open Custom1", false, 50)]
        private static void OpenCustom1() => Open(ProjectSceneTypes.Custom1);


#endif

        private static void Open(ProjectSceneTypes scene)
        {
            var name = scene.ToString();

            // Do we have an override specified?
            var custom = PlayerPrefs.GetString(scene.ToString(), string.Empty);
            if (string.IsNullOrEmpty(custom) == false)
                name = custom;

            var path = FindScene(name);
            if (string.IsNullOrEmpty(path))
            {
                Debug.Log($"'{name}' could not be opened.");
                return;
            }
            EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
        }

        private static string FindScene(string name)
        {
            var file = $"{name}.unity";
            foreach (var item in EditorBuildSettings.scenes)
            {
                if (item.path.Contains(file))
                    return item.path;
            }
            Debug.Log($"'{name}' could not be found.");
            return default;
        }

        private static void PlayModeStateChanged(PlayModeStateChange state)
        {
            var used = bool.Parse(PlayerPrefs.GetString(WAS_PLAYED_FROM_MAIN_SCENE, FALSE));
            switch (state)
            {
                case PlayModeStateChange.EnteredEditMode:
                    if (used == false)
                        return;

                    PlayerPrefs.SetString(WAS_PLAYED_FROM_MAIN_SCENE, FALSE);

                    var last = PlayerPrefs.GetString(LAST_SCENE);
                    if (string.IsNullOrEmpty(last))
                    {
                        Debug.Log("Last scene was NULL or invalid.");
                        return;
                    }
                    if (EditorSceneManager.GetActiveScene().name == last)
                    {
                        Debug.Log("Active scene is last scene; skipping.");
                        return;
                    }

                    var path = FindScene(last);
                    EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
                    break;
            }
        }


    }
}
