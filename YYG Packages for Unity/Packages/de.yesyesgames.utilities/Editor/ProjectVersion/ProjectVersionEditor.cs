﻿/////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities.ProjectVersion.Editor
{
    [CustomEditor(typeof(ProjectVersion), true)]
    public class ProjectVersionEditor : EditorBase
    {

#if UNITY_EDITOR
        protected override void OnEnable()
        {
            base.OnEnable();
            if (GetScriptTarget<ProjectVersion>(out var script))
                script.Load();
        }
#endif


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GetScriptTarget<ProjectVersion>(out var script, out var isPlaying);

            if (script.Data == null)
            {
                return;
            }

            BeginVertical();
            BeginSection("Project details");

            DrawInput("Product", "This is what the app is called & how it is visible in the platform's ecosystem.", ref script.Data.ProductName);
            DrawInput("Company", ref script.Data.CompanyName);

            DrawGap();
            DrawInput("Package", ref script.Data.PackageName);
            DrawInput("Domain", ref script.Data.PackageDomain);

            DrawLabel($"{script.Data.PackageIdentifier}");

            DrawInput("Major", ref script.Data.MajorCode);
            DrawInput("Minor", ref script.Data.MinorCode);
            DrawInput("Patch", ref script.Data.PatchCode);
            DrawInput("Build", ref script.Data.BuildNumber);

            DrawGap();
            DrawInput("Output", "{V}: Major\n{v}: Minor\n{p}: Patch\n{b}: Build\n{t}: Time\n{d}: Date\n{e}: Epoch", ref script.Data.VersionLine);
            DrawLabel($"{script.Data.Output}");

            EndSection();
            EndVertical();

            BeginVertical();
            BeginSection("Options");

            DrawButton("Refresh values", script.Refresh, !isPlaying);
            DrawButton("Refresh Unity settings", () =>
            {
                script.Refresh();
                script.Set();
                //
            }, !isPlaying);

            DrawGap();
            DrawButton("Load values", script.Load, !isPlaying);
            DrawButton("Save values", script.Save, !isPlaying);
            DrawButton("Clear values", script.Clear, !isPlaying);

            EndSection();
            EndVertical();
        }
    }
}
