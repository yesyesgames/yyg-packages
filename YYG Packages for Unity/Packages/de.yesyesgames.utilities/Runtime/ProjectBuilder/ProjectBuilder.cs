﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Statics;
using YYG.Packages.Utilities.ProjectSymbols;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public sealed class ProjectBuilder : ProjectMono
    {
        internal PlatformTargets Platform => platform;
        internal BuilderSettings Settings => settings;
        internal VersionModel ProjectVersion => version.VersionData;

        [Topic("ProjectBuilder")]
        [Header("Settings")]
        [SerializeField]
        private BuilderSettings settings;

        [Space]
        [SerializeField]
        private PlatformTargets platform = PlatformTargets.Default;

        [Space]
        [SerializeField]
        private VersionModelObject version;

        [ReadOnly]
        private const string AssetName1 = "VersionModel.Default";


        private void OnValidate()
        {
#if UNITY_EDITOR

            UnityEditor.EditorUserBuildSettings.buildAppBundle = settings.AppBundle;
            UnityEditor.EditorUserBuildSettings.development = settings.DevelopmentBuild;

            if (version == null)
            {
                Goodies.FindAsset<VersionModelObject>(AssetName1, out version);
            }
#endif
        }




    }
}
