﻿///////////////////////////////////////////////////////////////////////////////
using System;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    [System.Serializable]
    public class BuilderSettings
    {
        [Tooltip("Creates a build without product name or versioning & saves it separately.")]
        public bool AdHocBuild = false;

        [Space]
        public bool BuildAndRun = false;
        public bool DevelopmentBuild = false;

        [Space]
        [Tooltip("Add the version codes at the end of the filename.")]
        public bool IncludeVersion = false;
        public bool IncrementBuildNumber = true;

        [Header("Android")]
        public bool AppBundle = false;

    }

}
