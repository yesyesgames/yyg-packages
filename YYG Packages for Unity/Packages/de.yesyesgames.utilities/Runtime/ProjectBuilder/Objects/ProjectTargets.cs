﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace YYG.Packages.Unity.Utilities
{
    public enum PullTargets
    {
        [Tooltip("Based on build settings.")]
        Default = 0,
        Standalone,
        Android,
        iOS,
    }

    [System.Flags]
    public enum PushTargets
    {
        [Tooltip("Based on build settings.")]
        Default = 0,
        iOS = 1,
        Android = 2,
        Standalone = 4,
    }
}
