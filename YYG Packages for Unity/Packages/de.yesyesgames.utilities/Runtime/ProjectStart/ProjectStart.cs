/////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

using YYG.Packages.Unity.Core.Attributes;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public class ProjectStart : ProjectMono
    {

        [Topic("ProjectStart")]
        [Header("Settings")]
        [SerializeField]
        private int targetFPS = 60;

        [Space]
        [SerializeField]
        private ProjectScene rootScene;
        [SerializeField]
        private List<ProjectScene> additionalScenes;

        [Space]
        [SerializeField]
        private ProjectLoggerSettings loggerSettings = default;

        protected ProjectSceneLoader sceneLoader;


        protected virtual void Awake()
        {
            DontDestroyOnLoad(this);
            sceneLoader = new();
            Application.targetFrameRate = targetFPS;
        }


        protected virtual void Start()
        {
            ProjectLogger.Instance.Set(loggerSettings);
            sceneLoader.OnCompleted += RootSceneLoaded;
            sceneLoader.Load(rootScene);
        }


        protected virtual void RootSceneLoaded(Scene scene)
        {
            DBG($"ProjectStart::RootSceneLoaded(${scene})");
            sceneLoader.OnCompleted -= RootSceneLoaded;
            sceneLoader.OnCompleted += AdditionalScenesLoaded;
            sceneLoader.Load(additionalScenes);
        }


        protected virtual void AdditionalScenesLoaded(Scene scene)
        {
            DBG($"ProjectStart::AdditionalScenesLoaded(${scene})");
            sceneLoader.OnCompleted -= AdditionalScenesLoaded;
        }


    }
}
