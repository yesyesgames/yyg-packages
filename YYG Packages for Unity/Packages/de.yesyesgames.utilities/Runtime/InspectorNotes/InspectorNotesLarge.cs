﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    /// <summary>
    /// Take a break! Have a note!
    /// </summary>
    public class InspectorNotesLarge : MonoBehaviour
    {
#pragma warning disable
        [SerializeField, TextArea(minLines: 15, maxLines: 15)]
        private string notes;
    }
}
