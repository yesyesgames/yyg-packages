﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    /// <summary>
    /// Take a break! Have a note!
    /// </summary>
    public class InspectorNotesSmall : MonoBehaviour
    {
#pragma warning disable
        [SerializeField, TextArea(minLines: 5, maxLines: 5)]
        private string notes;
    }
}
