﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    /// <summary>
    /// Take a break! Have a note!
    /// </summary>
    public class InspectorNotesMedium : MonoBehaviour
    {
#pragma warning disable
        [SerializeField, TextArea(minLines: 10, maxLines: 10)]
        private string notes;
    }
}
