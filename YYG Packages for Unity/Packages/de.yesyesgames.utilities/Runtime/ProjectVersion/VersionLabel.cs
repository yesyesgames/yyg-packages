﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities.ProjectVersion
{
    /// <summary>
    /// Helper class to inject the version based on BPO's Packages Version Model.
    /// </summary>
    public class VersionLabel : MonoBehaviour
    {

        [SerializeField]
        private VersionModelObject versionObject;
#if TEXTMESHPRO_AVAILABLE
        [SerializeField]
        private TMPro.TMP_Text label;
#else
        [SerializeField]
        private UnityEngine.UI.Text label;
#endif


        private void Start() => Set(versionObject?.VersionData.Output);


        private void Set(string text)
        {
#if TEXTMESHPRO_AVAILABLE
            label.SetText(text);
#else
            label.text = text;
#endif
        }


    }
}
