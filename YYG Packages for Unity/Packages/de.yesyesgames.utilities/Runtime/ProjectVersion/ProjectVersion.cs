﻿///////////////////////////////////////////////////////////////////////////////
using System;

using UnityEngine;

using YYG.Packages.Unity.AppEvents.Objects;
using YYG.Packages.Unity.Core.Statics;
using YYG.Packages.Utilities.ProjectSymbols;
using YYG.Packages.Unity.Core.Attributes;



#if UNITY_EDITOR
using UnityEditor;
#endif
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities.ProjectVersion
{
    public class ProjectVersion : ProjectMono
    {
        internal VersionModel Data => versionObject?.VersionData;
        private string File => VersionFile.Replace("{0}", Data.PackageName);

        [Space]
        [SerializeField]
        private VersionModelObject versionObject;

        [Space]
        [ReadOnly]
        [SerializeField]
        private AppEvent versionEvent;

        private const string VersionFile = "{0}.json";

#pragma warning disable
        private const string AssetName1 = "VersionModel.Default";
        private const string AssetName2 = "AppEvent.VersionEvent";


        private void Start() => versionEvent.Raise(Data.Output);

        private void OnValidate()
        {
            versionObject?.VersionData.Validate();

            Goodies.FindAsset<AppEvent>(AssetName2, out versionEvent);
            if (versionObject == null)
            {
                Goodies.FindAsset<VersionModelObject>(AssetName1, out versionObject);
            }
        }


        internal void Clear() => versionObject.VersionData = new VersionModel();
        private void ValidateNumber(ref int number, int min = 0) => number = Mathf.Clamp(number, min, number);


        public async void Load()
        {
            if (versionObject == null)
                return;

            if (IOSystem.Exists(File) == false)
                return;

            var data = await IOSystem.LoadJson<VersionModel>(File);
            versionObject.VersionData = data;
            Refresh();
        }


        public void Save()
        {
            if (versionObject == null)
                return;

            versionObject.IsDirty();
            IOSystem.SaveJson(File, Data);
        }


        public void Refresh()
        {
            if (versionObject == null)
                return;

            ValidateNumber(ref Data.MajorCode);
            ValidateNumber(ref Data.MinorCode);
            ValidateNumber(ref Data.PatchCode);
            ValidateNumber(ref Data.BuildNumber, 1);

            Data.Output = Data.VersionLine;
            Data.Output = Data.Output.Replace("{product}", Data.ProductName);
            Data.Output = Data.Output.Replace("{package}", Data.PackageName);
            Data.Output = Data.Output.Replace("{company}", Data.CompanyName);
            Data.Output = Data.Output.Replace("{V}", Data.MajorCode.ToString());
            Data.Output = Data.Output.Replace("{v}", Data.MinorCode.ToString());
            Data.Output = Data.Output.Replace("{p}", Data.PatchCode.ToString());
            Data.Output = Data.Output.Replace("{b}", Data.BuildNumber.ToString());
            Data.Output = Data.Output.Replace("{m}", GetBuildTarget().ToString().ToUpper());
            Data.Output = Data.Output.Replace("{d}", DateTime.Now.ToString("yyyy.MM.dd"));
            Data.Output = Data.Output.Replace("{t}", DateTime.Now.ToString("hh.mm.tt"));
            Data.Output = Data.Output.Replace("{e}", Goodies.EpochSeconds.ToString());

#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(versionObject);
#endif

        }


        private PlatformTargets GetBuildTarget()
        {
#if UNITY_EDITOR
            var target = EditorUserBuildSettings.activeBuildTarget;
            switch (target)
            {
                default: return PlatformTargets.Default;
                case BuildTarget.iOS: return PlatformTargets.iOS;
                case BuildTarget.Android: return PlatformTargets.Android;
                case BuildTarget.StandaloneWindows64: return PlatformTargets.Windows64;
            }
#else
            return PlatformTargets.Default;
#endif
        }


        public void Set()
        {
            if (versionObject == null)
                return;

#if UNITY_EDITOR

            // Basics
            PlayerSettings.productName = Data.ProductName;
            PlayerSettings.companyName = Data.CompanyName;
            PlayerSettings.bundleVersion = Data.VersionCode;

            // Android
            PlayerSettings.Android.bundleVersionCode = Data.BuildNumber;
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, Data.PackageIdentifier);

            // iOS
            PlayerSettings.iOS.buildNumber = Data.BuildNumber.ToString();
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, Data.PackageIdentifier);

            // Standalone
            PlayerSettings.macOS.buildNumber = Data.BuildNumber.ToString();
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Standalone, Data.PackageIdentifier);

#endif
        }
    }
}
