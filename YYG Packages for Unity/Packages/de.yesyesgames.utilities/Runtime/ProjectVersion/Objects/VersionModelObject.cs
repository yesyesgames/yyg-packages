﻿///////////////////////////////////////////////////////////////////////////////
using UnityEditor;

using UnityEngine;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    [CreateAssetMenu(fileName = "ProjectVersion", menuName = "YYG Packages for Unity/Utilities/ProjectVersion/New VersionModel")]
    public class VersionModelObject : ScriptableObject
    {

        public VersionModel VersionData = new();

        internal void IsDirty()
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

    }
}

