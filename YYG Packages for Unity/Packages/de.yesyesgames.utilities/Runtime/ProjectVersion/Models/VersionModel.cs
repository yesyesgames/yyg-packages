﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;

namespace YYG.Packages.Unity.Utilities
{
    [System.Serializable]
    public class VersionModel
    {
        internal string VersionCode => $"{MajorCode}.{MinorCode}.{PatchCode}";
        internal string VersionBuildCode => $"{MajorCode}.{MinorCode}.{PatchCode}B{BuildNumber}";
        internal string PackageIdentifier => $"{PackageDomain}.{PackageName}".ToLower();


        public string CompanyName = "YesYesGames";
        public string PackageName = "app";

        public string ProductName = "YYG App";

        public string PackageDomain = "de.yesyesgames";

        public int MajorCode = 0;
        public int MinorCode = 1;
        public int PatchCode = 0;
        public int BuildNumber = 0;

        public string VersionLine = "{V}.{v}.{p}B{b}.{t}.{d}.{e}";

        [ReadOnly]
        [SerializeField]
        public string Output;


        internal void Validate()
        {
            MajorCode = Mathf.Clamp(MajorCode, 0, int.MaxValue);
            MinorCode = Mathf.Clamp(MinorCode, 1, int.MaxValue);
            PatchCode = Mathf.Clamp(PatchCode, 0, int.MaxValue);
            BuildNumber = Mathf.Clamp(BuildNumber, 0, int.MaxValue);
        }

    }
}
