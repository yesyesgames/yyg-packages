///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    [System.Serializable]
    public class ProjectMenuModel
    {
        public string Name;
        public ProjectSceneTypes Type;
    }
}
