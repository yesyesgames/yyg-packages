///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public class ProjectMenu : MonoBehaviour
    {

        [Topic("ProjectMenu")]
        [Header("Settings")]
        [SerializeField]
        private List<ProjectMenuModel> customOverrides = new();


        private void OnValidate()
        {
            var values = System.Enum.GetValues(typeof(ProjectSceneTypes)).Cast<ProjectSceneTypes>();
            foreach (var item in values)
            {
                PlayerPrefs.DeleteKey(item.ToString());
            }
            foreach (var item in customOverrides)
            {
                PlayerPrefs.SetString(item.Type.ToString(), item.Name);
            }
        }

    }
}
