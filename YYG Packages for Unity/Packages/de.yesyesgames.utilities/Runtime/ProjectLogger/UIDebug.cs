using TMPro;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;

namespace YYG.Packages.Unity.Utilities
{
    public class UIDebug : MonoBehaviour
    {

        [Topic("UIDebug")]
        [Header("Declares")]
        [SerializeField]
        private GameObject overlay;
        [SerializeField]
        private TMP_Text lVersion;
        [SerializeField]
        private TMP_Text lProject;
        [SerializeField]
        private TMP_Text lFPS;
        [SerializeField]
        private TMP_Text lConsole;

        [Space]
        [SerializeField]
        private VersionModelObject versionObject;

        [Header("Settings")]
        [SerializeField]
        private string projectOutput = "{p} - �YesYesGames";
        [SerializeField]
        private string fpsOutput = "{fps} FPS";

        [Space]
        [SerializeField]
        private bool visibleOnStart = true;


        private void Awake()
        {
            lFPS.SetText(string.Empty);
            lVersion.SetText(string.Empty);
            lProject.SetText(string.Empty);
            lConsole.SetText(string.Empty);

            lVersion.SetText($"{versionObject?.VersionData.Output}");

            var p = projectOutput.Replace("{p}", versionObject?.VersionData.ProductName);
            lProject.SetText(p);

            if (visibleOnStart)
                overlay.SetActive(true);
        }


        private void Update()
        {
            // TODO: Change me to the new InputSystem.
            if (Input.GetKeyDown(KeyCode.F12))
            {
                overlay.SetActive(!overlay.activeSelf);
            }

            if (lVersion == null)
                return;

            var fps = Time.frameCount / Time.time;
            var text = fpsOutput.Replace("{fps}", $"{fps:0}");

            lFPS.SetText(text);
        }


        public void Log(string text) => lConsole.SetText(text);


    }
}
