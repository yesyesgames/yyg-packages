using UnityEngine;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    [System.Serializable]
    public class ProjectLoggerSettings
    {
        public int MaxLines = 12;
        public int MessageDecay = 4;

        [Space]
        public Color TimeColor = new(.85f, .85f, .85f);
        public Color DBGColor = new(.85f, .85f, .85f);
        public Color WRNColor = new(.75f, .75f, .15f);
        public Color ERRColor = new(.75f, .15f, .15f);

        [Space]
        public bool IncludeUnityConsole = true;
        public bool IncludeActivityLogging = true;
    }
}
