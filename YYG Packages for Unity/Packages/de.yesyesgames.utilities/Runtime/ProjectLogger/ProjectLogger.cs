/////////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using YYG.Packages.Unity.AppEvents.Objects;
using YYG.Packages.Unity.Core.Abstracts;
using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
using YYG.Packages.Unity.Core.Statics;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public class ProjectLogger : SingletonBase<ProjectLogger>
    {

        [Topic("ProjectLogger")]
        [Header("Settings")]
        [SerializeField]
        private ProjectLoggerSettings settings = new();

        [Header("DEBUGGING")]
        [ReadOnly]
        [SerializeField]
        private AppEvent appEvent1;
        [ReadOnly]
        [SerializeField]
        private AppEvent appEvent2;

        private bool _running;
        private Queue<string> _messages;

#pragma warning disable 0414
        private const string AssetName1 = "AppEvent.ProjectLogger.Log";
#pragma warning disable 0414
        private const string AssetName2 = "AppEvent.LoggingModule.Log";


        protected override void Awake()
        {
            base.Awake();
            _messages = new();
            DontDestroyOnLoad(this);
        }


        private void OnValidate()
        {
#if UNITY_EDITOR
            Goodies.FindAsset<AppEvent>(AssetName1, out appEvent1);
            Goodies.FindAsset<AppEvent>(AssetName2, out appEvent2);
#endif
        }


        public void DBG(string message, Object context) => Log(message, context, DebugTypes.DBG);
        public void WRN(string message, Object context) => Log(message, context, DebugTypes.WRN);
        public void ERR(string message, Object context) => Log(message, context, DebugTypes.ERR);
        public void Set(ProjectLoggerSettings setting) => settings = setting;


        private void Log(string text, Object context, DebugTypes type)
        {
#if YYG_USE_LOGGING
            string msg = $"<color=#{ColorUtility.ToHtmlStringRGB(settings.TimeColor)}>{System.DateTime.Now:hh:mm:ss}</color>: ";

            switch (type)
            {
                default:
                    if (settings.IncludeUnityConsole)
                    {
                        Debug.Log(text, context);
                    }

                    msg += $"<color=#{ColorUtility.ToHtmlStringRGB(settings.DBGColor)}>{text}</color>";
                    break;

                case DebugTypes.WRN:
                    if (settings.IncludeUnityConsole)
                        Debug.LogWarning(text, context);

                    msg += $"<color=#{ColorUtility.ToHtmlStringRGB(settings.WRNColor)}>{text}</color>";
                    break;

                case DebugTypes.ERR:
                    if (settings.IncludeUnityConsole)
                        Debug.LogError(text, context);

                    msg += $"<color=#{ColorUtility.ToHtmlStringRGB(settings.ERRColor)}>{text}</color>";
                    break;
            }

            _messages.Enqueue(msg);

            if (_messages.Count > settings.MaxLines)
            {
                _messages.Dequeue();
            }

            if (!_running)
            {
                StartCoroutine(Decay());
            }

            Output();
#else
            Debug.Log("You are trying to log something but you need to set the 'BPO_USE_LOGGING' symbol first.");
#endif
        }


        private void Output()
        {
            var output = string.Empty;
            _messages.ToList().ForEach(x => output += $"{x}\n");
            appEvent1?.Raise(output);

            if (settings.IncludeActivityLogging)
            {
                // FIXME: This wont work because the DebugActivity is expecting an LogObject.
                appEvent2?.Raise(output);
            }
        }


        private IEnumerator Decay()
        {
            _running = true;
            while (_messages.Count > 0)
            {
                yield return new WaitForSecondsRealtime(settings.MessageDecay);
                _messages.Dequeue();
                Output();
            }

            _running = false;
        }

    }
}
