///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Statics;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    // TODO: Add support for multiple platforms.
    public class ProjectSymbols : MonoBehaviour
    {

        [Topic("ProjectSymbols")]
        [Header("Settings")]
        [SerializeField]
        private PullTargets pullFrom = PullTargets.Default;
        [SerializeField]
        private PushTargets pushTo = PushTargets.Default;

        [Space]
        public List<string> customSymbols = new();

        [ReadOnly]
        public List<string> targetSymbols = new();


        public void Pull()
        {
#if UNITY_EDITOR
            if (pullFrom == PullTargets.Default)
            {
                var target = UnityEditor.EditorUserBuildSettings.selectedBuildTargetGroup;
                targetSymbols = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(target).Split(';').ToList();
                return;
            }

            if (pullFrom == PullTargets.Android)
            {
                targetSymbols = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Android).Split(';').ToList();
            }
            else if (pullFrom == PullTargets.iOS)
            {
                targetSymbols = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.iOS).Split(';').ToList();
            }
            else if (pullFrom == PullTargets.Standalone)
            {
                targetSymbols = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Standalone).Split(';').ToList();
            }
            else
            {
                targetSymbols.Clear();
                throw new CustomExceptions.FeatureNotYetImplementedException(Application.platform.ToString());
            }
#endif
        }

        public void Push()
        {
#if UNITY_EDITOR
            if (pushTo == PushTargets.Default)
            {
                var target = UnityEditor.EditorUserBuildSettings.selectedBuildTargetGroup;
                UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(target, customSymbols.ToArray());
                return;
            }

            if (pushTo.HasFlag(PushTargets.Android))
            {
                UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Android, customSymbols.ToArray());
            }
            else if (pushTo.HasFlag(PushTargets.iOS))
            {
                UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.iOS, customSymbols.ToArray());
            }
            else if (pushTo.HasFlag(PushTargets.Standalone))
            {
                UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(UnityEditor.BuildTargetGroup.Standalone, customSymbols.ToArray());
            }
            else
            {
                targetSymbols.Clear();
                throw new CustomExceptions.FeatureNotYetImplementedException(Application.platform.ToString());
            }
#endif
        }

        public void Copy()
        {
            customSymbols = new List<string>(targetSymbols);
        }


    }
}
