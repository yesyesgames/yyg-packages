namespace YYG.Packages.Utilities.ProjectSymbols
{
    public enum PlatformTargets
    {
        /// <summary>
        /// Based on build settings.
        /// </summary>
        Default = 0,

        iOS,
        Android,
        Windows64,
    }

    [System.Flags]
    public enum PlatformTargetFlags
    {
        /// <summary>
        /// Based on build settings.
        /// </summary>
        Default = 0,

        Android = 1,
        iOS = 2,
        Windows64 = 3,
    }
}
