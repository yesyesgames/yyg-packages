using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.Core.Objects;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public abstract class ProjectMono : MonoBehaviour
    {

        protected DebugLevel DebugLevel { set => debugLevel = value; }

        [Topic("ProjectMono")]
        [Header("Settings")]
        [SerializeField]
        private DebugLevel debugLevel = DebugLevel.Warnings | DebugLevel.Errors;


        public void DBG(string message, Object context = null) => Log(message, context, DebugTypes.DBG);
        public void WRN(string message, Object context = null) => Log(message, context, DebugTypes.WRN);
        public void ERR(string message, Object context = null) => Log(message, context, DebugTypes.ERR);


        [ContextMenu("Reset debug level")]
        private void ResetDebugLevel() => debugLevel = DebugLevel.Warnings | DebugLevel.Errors;


        private void Log(string message, Object context, DebugTypes type)
        {
            if (ProjectLogger.Instance == null)
            {
                Debug.Log("You are trying to log something but there is no ProjectLogger within any scene.");
                return;
            }

            // TODO: Use reflection to get the real caller (root level).
            if (context == null)
                context = this;

            switch (type)
            {
                case DebugTypes.DBG:
                    if (debugLevel.HasFlag(DebugLevel.Debugs))
                        ProjectLogger.Instance.DBG(message, context);
                    break;

                case DebugTypes.WRN:
                    if (debugLevel.HasFlag(DebugLevel.Warnings))
                        ProjectLogger.Instance.WRN(message, context);
                    break;

                case DebugTypes.ERR:
                    if (debugLevel.HasFlag(DebugLevel.Errors))
                        ProjectLogger.Instance.ERR(message, context);
                    break;

            }
        }


    }
}
