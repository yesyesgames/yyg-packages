///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public enum ProjectSceneTypes
    {
        NONE = 0,
        UI,
        Main,
        Core,
        Level,
        Camera,
        Custom1,
        Custom2,
        Custom3,
    }
}
