///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    [System.Serializable]
    public class ProjectScene
    {

        public string Name;
        public bool IsAsync;
        public bool IsActive;
        public bool IsAdditive;


        public ProjectScene(string sceneName, bool loadAdditive = false, bool loadAsync = false, bool setActive = true)
        {
            Name = sceneName;
            IsAsync = loadAsync;
            IsActive = setActive;
            IsAdditive = loadAdditive;
        }

    }
}
