///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.Utilities
{
    public class ProjectSceneLoader
    {

        public event System.Action<Scene> OnCompleted;

        private int _total;
        private int _loaded;
        private string _activeScene;


        public ProjectSceneLoader()
        {
            SceneManager.sceneLoaded += SceneLoaded;
        }

        ~ProjectSceneLoader()
        {
            SceneManager.sceneLoaded -= SceneLoaded;
        }


        public void Load(ProjectScene scene)
        {
            // TODO: This might be an issue, since the values are not independent from any caller and can be overwritten.
            _total = 1;
            _loaded = 0;
            LoadScene(scene);
        }

        public void Load(List<ProjectScene> scenes)
        {
            _loaded = 0;
            _total = scenes.Count;
            foreach (var item in scenes)
                LoadScene(item);
        }


        private void LoadScene(ProjectScene scene)
        {
            if (string.IsNullOrEmpty(scene.Name))
            {
                Debug.LogError($"Scene name is NULL or invalid.");
                return;
            }

            if (scene.IsActive)
                _activeScene = scene.Name;

            if (scene.IsAsync)
                SceneManager.LoadSceneAsync(scene.Name, scene.IsAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            else
                SceneManager.LoadScene(scene.Name, scene.IsAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
        }

        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            _loaded++;
            if (string.IsNullOrEmpty(_activeScene) == false)
            {
                if (_activeScene == scene.name)
                {
                    _activeScene = string.Empty;
                    SceneManager.SetActiveScene(scene);
                }
            }

            if (_loaded >= _total)
                OnCompleted?.Invoke(scene);
        }

    }
}
