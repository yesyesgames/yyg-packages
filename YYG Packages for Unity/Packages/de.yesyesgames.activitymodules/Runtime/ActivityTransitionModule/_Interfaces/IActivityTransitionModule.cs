﻿using System.Threading.Tasks;

namespace YYG.Packages.Unity.ActivityModules.Interfaces
{
    public interface IActivityTransitionModule
    {
        Task Show(bool instant = false);
        Task Hide(bool instant = false);

        void OverlayFade(bool isBlack);
        void OverlayBlocksRaycasts(bool state);
        Task OverlayFade(bool fromBlack, bool toBlack, bool blockRaycasts = true);
    }
}
