﻿/////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivityModules.Objects
{
    [System.Serializable]
    public class TransitionElement
    {

        [SerializeField]
        [HideInInspector]
        internal string label;

        public RectTransform Element;

        [Space]
        public bool ApplyScaling = false;
        public TransitionTypes Transition = TransitionTypes.Vertical;


        [Space]
        public OffsetTypes Type = OffsetTypes.CUSTOM;
        public float Offset = 5000;

    }
}
