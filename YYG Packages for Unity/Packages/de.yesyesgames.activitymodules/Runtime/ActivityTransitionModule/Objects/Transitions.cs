﻿/////////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivityModules.Objects
{

    public enum TransitionTypes
    {
        Vertical = 0,
        Horizontal = 1,
        Popping = 3,
    }

    public enum OffsetTypes
    {
        CUSTOM = 0,
        HeaderFromTop = 1000,
        MiddleFromTop = 5000,
        MiddleFromBottom = -5000,
        FooterFromBottom = -1000,
    }

}
