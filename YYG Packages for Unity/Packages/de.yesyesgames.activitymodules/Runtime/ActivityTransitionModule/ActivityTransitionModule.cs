﻿///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.Core.Attributes;
using YYG.Packages.Unity.ActivitySystem;
using YYG.Packages.Unity.ModuleSystem.Abstracts;
using YYG.Packages.Unity.ActivityModules.Interfaces;
using YYG.Packages.Unity.ActivityModules.Objects;

#if YYG_USE_DOTWEEN
using DG.Tweening;
#endif
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.Unity.ActivityModules
{
    [DisallowMultipleComponent]
    public sealed class ActivityTransitionModule : ModuleBase, IActivityTransitionModule
    {

        #region DECLARES


        [Topic("ActivityTransitionModule")]
        [Header("Settings")]
        [SerializeField]
        private List<TransitionElement> elements;

        [Space]
        [SerializeField]
        [Tooltip("Optional")]
        private List<CanvasGroup> fadings;

        [Space]
        [SerializeField]
        [Tooltip("OPTIONAL: Add the 'Overlay' prefab coming with the ActivitySystem samples.")]
        private UIOverlay overlay;

#if YYG_USE_DOTWEEN
        [SerializeField, Space]
        private float time = .64f;

        [SerializeField]
        private Ease easing = Ease.InOutQuint;
#endif

        private readonly float _minimumScale = .0001f;


        #endregion



        #region MONO_METHODS


        private void OnEnable() => OnDestroy();
        private void OnDisable() => OnDestroy();


        private void OnValidate()
        {
            if (elements == null)
                return;

            foreach (var e in elements)
            {
                if (e.Element == null)
                    continue;

                e.label = e.Element.name;
                if (e.Type == OffsetTypes.CUSTOM)
                    return;

                e.Offset = (int)e.Type;
            }
        }


        private void OnDestroy()
        {
#if YYG_USE_DOTWEEN
            fadings?.ForEach(x => x.DOKill());
            elements?.ForEach(x => x.Element.DOKill());
#endif
        }


        #endregion



        #region PUBLIC_METHODS



        public async Task Show(bool instant = false) => await DoIt(true, instant);
        public async Task Show(int index, bool instant = false) => await DoIt(index, true, instant);

        public async Task Hide(bool instant = false) => await DoIt(false, instant);
        public async Task Hide(int index, bool instant = false) => await DoIt(index, false, instant);

        public void OverlayFade(bool isBlack) => overlay.SetFade(isBlack);
        public void OverlayBlocksRaycasts(bool state) => overlay.BlockRaycats(state);
        public async Task OverlayFade(bool fromBlack, bool toBlack, bool blockRaycasts = true) => await overlay.DoFade(fromBlack, toBlack, blockRaycasts);


        #endregion



        #region PRIVATE_METHODS


#pragma warning disable
        private async Task DoIt(bool visibility, bool instant)
        {
            if (elements == null || elements.Count == 0)
                return;

            Fade(visibility, instant);
            if (instant)
            {
                elements.ForEach(x => Instant(x.Element, visibility ? 0 : x.Offset, x.Transition));
            }
            else
            {
                List<Task> tasks = new();
                foreach (var e in elements)
                {
#if YYG_USE_DOTWEEN
                    e.Element.transform.DOKill();
                    tasks.Add(Move(e.Element, visibility ? 0 : e.Offset, e.Transition));
                    Scale(e, visibility, instant);
#else
                    Scale(e, visibility, instant);
                    Instant(e.Element, visibility ? 0 : e.Offset, e.Transition);
#endif
                }
#if YYG_USE_DOTWEEN
                await Task.WhenAll(tasks);
#endif
            }
        }


        private async Task DoIt(int index, bool visibility, bool instant)
        {
            if (elements == null || elements.Count == 0)
                return;

            var e = elements[index];
            if (e == null)
                return;

            var offset = visibility ? 0 : e.Offset;
            if (instant)
            {
                Instant(e.Element, offset, e.Transition);
            }
            else
            {
#if YYG_USE_DOTWEEN
                e.Element.transform.DOKill();
                Scale(e, visibility, instant);
                await Move(e.Element, offset, e.Transition);
#else
                Scale(e, visibility, instant);
                Instant(e.Element, offset, e.Transition);
#endif
            }
        }


        private async Task Move(RectTransform element, float offset, TransitionTypes transition)
        {
            switch (transition)
            {
                case TransitionTypes.Popping: await Popping(element, offset); break;
                case TransitionTypes.Vertical: await Vertical(element, offset); break;
                case TransitionTypes.Horizontal: await Horizontal(element, offset); break;
            }
        }


        private void Fade(bool visibility, bool instant)
        {
            if (fadings == null || fadings.Count == 0)
                return;

            var a = visibility ? 1 : 0;
            fadings.ForEach(ui =>
            {
                if (instant)
                {
                    ui.alpha = (a);
                }
                else
                {
#if YYG_USE_DOTWEEN
                    ui.DOKill();
                    DOTween.Kill(ui.alpha);
                    DOTween.To(() => ui.alpha, x => ui.alpha = x, a, time);
#else
                    ui.alpha = (a);
#endif
                }
            });
        }


        private void Instant(RectTransform element, float offset, TransitionTypes transition)
        {
            switch (transition)
            {
                case TransitionTypes.Popping: element.localScale = Vector3.one * (offset == 0 ? 1 : _minimumScale); break;
                case TransitionTypes.Vertical: element.anchoredPosition = new Vector2(element.anchoredPosition.x, offset); break;
                case TransitionTypes.Horizontal: element.anchoredPosition = new Vector2(offset, element.anchoredPosition.y); break;
            }
        }

        CanvasGroup t;
        private async Task Scale(TransitionElement e, bool visibility, bool instant)
        {
            if (e.ApplyScaling == false)
                return;

            var to = visibility ? 1f : 0f;
            var from = visibility ? .0001f : 1f;

            if (instant)
            {
                e.Element.transform.localScale = Vector3.one * to;
                return;
            }
#if YYG_USE_DOTWEEN
            e.Element.transform.localScale = Vector3.one * from;
            await e.Element.DOScale(to, time).SetEase(easing).AsyncWaitForCompletion();
#endif
        }


        private async Task Vertical(RectTransform element, float offset)
        {
#if YYG_USE_DOTWEEN
            await element.DOAnchorPosY(offset, time).SetEase(easing).AsyncWaitForCompletion();
#else
            element.anchoredPosition = new Vector2(element.anchoredPosition.x, offset);
#endif
        }


        private async Task Horizontal(RectTransform element, float offset)
        {
#if YYG_USE_DOTWEEN
            await element.DOAnchorPosX(offset, time).SetEase(easing).AsyncWaitForCompletion();
#else
            element.anchoredPosition = new Vector2(offset, element.anchoredPosition.y);
#endif
        }


        private async Task Popping(RectTransform element, float offset)
        {
#if YYG_USE_DOTWEEN
            await element.DOScale(Vector3.one * (offset == 0 ? 1 : _minimumScale), time).SetEase(easing).AsyncWaitForCompletion();
#else
            element.localScale = Vector3.one * (offset == 0 ? 1 : _minimumScale);
#endif
        }


        #endregion



    }
}
