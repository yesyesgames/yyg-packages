using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
public class DemoObject
{

    public string Text;
    public int Number;

    public DemoObject(string text, int number)
    {
        Text = text;
        Number = number;
    }

}

namespace YYG.Packages
{
    public class CustomEditorDemo : MonoBehaviour
    {

        public List<DemoObject> DemoList = new()
        {
            new DemoObject("Hello",42),
            new DemoObject("World",64)
        };
    }
}
