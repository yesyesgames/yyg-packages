
using UnityEditor;

using YYG.Packages.Unity.Core.Editor.Abstracts;

using static UnityEditor.Progress;

namespace YYG.Packages
{
    [CustomEditor(typeof(CustomEditorDemo))]
    public class CustomEditorDemoEditor : EditorBase
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DrawGap();
            DrawLabel("ErrorStyle", ErrorStyle);
            DrawLabel("HeaderStyle", HeaderStyle);
            DrawLabel("DefaultStyle", DefaultStyle);
            DrawLabel("DefaultStyle", DefaultStyle);
            DrawLabel("WarningStyle", WarningStyle);
            DrawGap();

            DrawLabel("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", DefaultStyle);

            if (GetScriptTarget<CustomEditorDemo>(out var script, out var isPlaymode))
            {
                DrawList<DemoObject>("Demo list:", script.DemoList);
            }
        }

    }
}
