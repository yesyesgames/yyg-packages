using UnityEngine;

using YYG.Packages.Unity.Core.Statics;

public class IOTestings : MonoBehaviour
{

    [System.Serializable]
    public class DataObject
    {
        public string HelloWorld = "Test";
    }

    public DataObject data;



    private async void OnEnable()
    {
        var uri = System.IO.Path.Combine(Application.persistentDataPath, "IO/Data.json");
        await IOSystem.SaveJson(uri, data, Progress);
    }



    private async void OnDisable()
    {
        var uri = System.IO.Path.Combine(Application.persistentDataPath, "IO/Data.json");
        var task = IOSystem.LoadJson<DataObject>(uri, Progress);
        await task;
        data = task.Result ?? new DataObject();
    }


    private void Progress(float a, float b)
    {
        Debug.Log($"{a} / {b}");
    }

}
