///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
public class HomeActivity : ActivityBase
{
    [Header("HomeActivity")]
    [SerializeField, Header("Declares")]
    private Param popupActivity = null;


    public void Popup()
    {
        DBG($"HomeActivity::Popup()");
        Switch(popupActivity);
    }


}
