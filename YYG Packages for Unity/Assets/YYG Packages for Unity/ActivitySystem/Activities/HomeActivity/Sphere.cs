
using UnityEngine;

using YYG.Packages.Unity.Core.Objects;

namespace YYG.Packages
{
    public class Sphere : MonoBehaviour
    {

        public void Hit(RaycastHit hit)
        {
            Debug.Log($"Hit({hit})");
        }


        public void HitData(RaycastHit hit, DataPackage data)
        {
            Debug.Log($"Hit({hit}, {data})");
            if (data.Custom is SphereData value)
            {
                Debug.Log(value.Number);
            }
        }
    }
}
