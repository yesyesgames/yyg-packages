///////////////////////////////////////////////////////////////////////////////
using YYG.Packages.Unity.ActivityModules.Interfaces;
using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
public class PopupActivity : ActivityBase
{

    public override void OnInit(DataPackage package)
    {
        base.OnInit(package);
        DBG($"PopupActivity::OnInit()");
        _ = Modules.Get<IActivityTransitionModule>().Hide(true);
        _ = Modules.Get<IActivityTransitionModule>().Show();
    }


    public async override void Back()
    {
        DBG($"PopupActivity::Back()");
        await Modules.Get<IActivityTransitionModule>().Hide();
        base.Back();
    }

}
