﻿///////////////////////////////////////////////////////////////////////////////
using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.Core.Objects;
///////////////////////////////////////////////////////////////////////////////
namespace YYG.Packages.ActivitySystem.Samples
{
    public class ProjectStart : ComponentBase
    {

        public static event System.Action OnComplete;

        [Header("ProjectStart")]
        [SerializeField, Header("Declares")]
        private Param rootActivity = null;

        [SerializeField, Header("Settings")]
        private int targetFPS = 30;


        protected override void Awake()
        {
            base.Awake();
            DBG($"ProjectStart::Awake()");
            Application.targetFrameRate = targetFPS;
        }


        private void Start()
        {
            DBG($"ProjectStart::Start()");
            Switch(rootActivity, new DataPackage().Set(new ProjectStartDataPackage()
            {
                OnComplete = OnComplete
                //

            }));
        }


        // What's that business doing here?!
        public void SphereDataReceiver(RaycastHit hit, object value)
        {
            if (value is SphereData data)
            {
                DBG($"ProjectStart::SphereDataReceiver(): Number is '{data.Number}'");
            }
        }


    }
}

public class SphereData
{
    public int Number = -1;
}