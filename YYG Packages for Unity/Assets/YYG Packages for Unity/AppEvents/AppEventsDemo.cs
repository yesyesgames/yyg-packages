using UnityEngine;

using YYG.Packages.Unity.AppEvents.Objects;

namespace YYG.Packages
{
    public class ObjectModel
    {
        public string Text;
    }

    public class AppEventsDemo : MonoBehaviour
    {

        public AppEvent appEventObject;


        private void OnEnable()
        {
            appEventObject.Raise(new ObjectModel()
            {
                Text = "Hello World",
            });
        }

        public void ReceiveObject(object value)
        {
            if (value is ObjectModel model)
            {
                Debug.Log(model.Text);
            }
        }

    }
}
