using System;
using System.Collections.Generic;

using Google.XR.ARCoreExtensions;

using Newtonsoft.Json;

using TMPro;

using UnityEngine;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.ARModules.Interfaces;
using YYG.Packages.Unity.Core.Objects;

[System.Serializable]
public struct Vector2D
{
    public double x;
    public double y;
}

[Serializable]
public class GeoAnchorHistoryCollection
{
    [JsonRequired]
    public List<GeoSpatialModel> Data = new();
}


public class GeoSpatialActivity : ActivityBase
{

    [Header("GeoSpatialActivity")]
    [SerializeField]
    private TMP_Text NFOText;
    [SerializeField]
    private GameObject GeoObject;

    private bool _earthIsReady;
    private bool _trackingIsReady;
    private string _storageKey;

    private GeoAnchorHistoryCollection _anchorCollection;


    protected override void Awake()
    {
        base.Awake();
        _anchorCollection = new();
        _storageKey = SystemInfo.deviceUniqueIdentifier;
    }


    private void Update()
    {
        PrintNFO();
    }


    public override void OnInit(DataPackage package = null)
    {
        base.OnInit(package);
#if UNITY_ANDROID && UNITY_EDITOR == false
        FindObjectOfType<ARCoreExtensions>(true).gameObject.SetActive(true);
#endif
    }


    public override void OnFocus(bool hasFocus, DataPackage dataPackage = null)
    {
        base.OnFocus(hasFocus, dataPackage);
        if (hasFocus)
        {
            Modules.Get<IAREarthIntegrityModule>().OnIntegrityChanged += EarthStateChanged;
            Modules.Get<IAREarthIntegrityModule>().OnTrackingChanged += EarthTrackingChanged;
            Modules.Get<IAREarthIntegrityModule>().Check();
        }
        else
        {
            Modules.Get<IAREarthIntegrityModule>().Cancel();
            Modules.Get<IAREarthIntegrityModule>().OnIntegrityChanged -= EarthStateChanged;
            Modules.Get<IAREarthIntegrityModule>().OnTrackingChanged -= EarthTrackingChanged;
        }
    }


    public void Load(bool isResolve)
    {
        LoadHistory();
        Clear(false);

        for (int i = 0; i < _anchorCollection.Data.Count; i++)
        {
            var pose = _anchorCollection.Data[i];
            if (isResolve)
            {
                pose.Altitude = 0;
                ResolveAnchor(pose, false);
                continue;
            }

            Create(pose, false);
        }
    }


    public void Clear(bool wipeHistory)
    {
        Modules.Get<IAREarthAnchorModule>().Clear();
        if (wipeHistory == false)
        {
            return;
        }

        _anchorCollection.Data.Clear();
        SaveHistory();
    }


    public void CreateTerrain(bool isResolve)
    {
        var pose = Modules.Get<IAREarthManagerModule>().Data;
        pose.Type = AnchorTypes.Terrain;
        Create(pose, isResolve);
    }


    public void CreateGeoSpatial(bool isResolve)
    {
        var pose = Modules.Get<IAREarthManagerModule>().Data;
        pose.Type = AnchorTypes.GeoSpatial;
        Create(pose, isResolve);
    }


    private void Create(GeoSpatialModel model, bool isResolve)
    {
        if (_trackingIsReady == false)
        {
            ERR("Tracking is not ready.");
            return;
        }

        if (isResolve)
        {
            ResolveAnchor(model, true);
            return;
        }

        CreateAnchor(model, true);
    }


    private async void CreateAnchor(GeoSpatialModel pose, bool save)
    {
        var result = await Modules.Get<IAREarthAnchorModule>().Create(pose);
        if (result.Success)
        {
            Create(result.Anchor);
            if (save)
            {
                Save(pose);
            }
        }
        else
        {
            ERR("GeoSpatialActivity::CreateAnchor(): Anchor not created.");
        }
    }


    private async void ResolveAnchor(GeoSpatialModel pose, bool save)
    {
        var result = await Modules.Get<IAREarthAnchorModule>().Resolve(pose);
        if (result.Success)
        {
            Create(result.Anchor);
            if (save)
            {
                Save(pose);
            }
        }
        else
        {
            ERR("GeoSpatialActivity::ResolveAnchor(): Anchor not resolved.");
        }
    }


    private void Create(ARGeospatialAnchor anchor)
    {
        var obj = Instantiate(GeoObject, anchor.transform);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localEulerAngles = Vector3.zero;
    }


    private void Save(GeoSpatialModel pose)
    {
        _anchorCollection.Data.Add(pose);
        SaveHistory();
    }


    private void PrintNFO()
    {
        var earthData = Modules.Get<IAREarthManagerModule>().Data;
        var locaData = Modules.Get<IUnityLocationServiceModule>().Data;

        var nfo = $"Location: Lat: {locaData.Lat:F9} / Lon: {locaData.Lon:F9}\n" +
            $"GeoPose: Lat: {earthData.Lat:F9} / Lon: {earthData.Lon:F9}\n" +
            $"Accuracy: L: {locaData.HorizontalAccuracy:F2} / E: {earthData.HorizontalAccuracy:F2} / H: {earthData.HeadingAccuracy:F2}\n" +
            $"Earth: {_earthIsReady} / Tracking: {_trackingIsReady} / VPS: {earthData.VPS}\n";

        NFOText.SetText(nfo);
    }


    private void LoadHistory()
    {
        if (PlayerPrefs.HasKey(_storageKey))
        {
            var json = PlayerPrefs.GetString(_storageKey);
            _anchorCollection = JsonConvert.DeserializeObject<GeoAnchorHistoryCollection>(json);
        }
        else
        {
            WRN($"Empty history created.");
            _anchorCollection = new();
        }
    }


    private void SaveHistory()
    {
        DBG("GeoSpatialActivity::SaveHistory()");
        var json = JsonConvert.SerializeObject(_anchorCollection, new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        });
        PlayerPrefs.SetString(_storageKey, json);
        PlayerPrefs.Save();
    }


    private void EarthStateChanged()
    {
        _earthIsReady = Modules.Get<IAREarthIntegrityModule>().IsAvailable;
        if (_earthIsReady)
        {
            Modules.Get<IAREarthManagerModule>().OnPoseChanged += GeoChanged;
            Modules.Get<IUnityLocationServiceModule>().OnLocationChanged += LocationChanged;

            Modules.Get<IAREarthManagerModule>().Begin();
            Modules.Get<IUnityLocationServiceModule>().Begin();
        }
        else
        {
            Modules.Get<IAREarthManagerModule>().Cancel();
            Modules.Get<IUnityLocationServiceModule>().Cancel();

            Modules.Get<IAREarthManagerModule>().OnPoseChanged -= GeoChanged;
            Modules.Get<IUnityLocationServiceModule>().OnLocationChanged -= LocationChanged;
        }
    }


    private void EarthTrackingChanged()
    {
        _trackingIsReady = Modules.Get<IAREarthIntegrityModule>().IsTracking;
    }


    private void GeoChanged(GeoSpatialModel value)
    {
        // DBG($"GeoChanged");
    }


    private void LocationChanged(LocationModel value)
    {
        // DBG($"LocationChanged");
    }


}
