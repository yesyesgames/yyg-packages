using YYG.Packages.Unity.ActivitySystem.Abstracts;

namespace YYG.Packages
{
    public class LocationServiceDemo : ComponentBase
    {

        private void OnEnable()
        {
            Modules.Get<IUnityLocationServiceModule>().OnLocationChanged += LocationChanged;
            Modules.Get<IUnityLocationServiceModule>().Begin();
        }


        private void OnDisable()
        {
            Modules.Get<IUnityLocationServiceModule>().OnLocationChanged -= LocationChanged;
            Modules.Get<IUnityLocationServiceModule>().Cancel();
        }


        private void LocationChanged(LocationModel model)
        {
            DBG($"{model.Lat}:{model.Lon}");
        }


    }
}
