
using UnityEngine;
using UnityEngine.AddressableAssets;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.AddressableModules;
using YYG.Packages.Unity.AddressableModules.Interfaces;
using YYG.Packages.Unity.Core.Objects;

public class AddressablesDemo1 : ComponentBase
{

    public Param m_label;
    public AssetReference m_asset;


    private async void OnEnable()
    {
        var asset1 = await Modules.Get<AddressableCreatorBasic>().Create(m_asset);
        if (asset1 is GameObject go1)
            go1.transform.position = Vector3.zero;

        var asset2 = await Modules.Get<IAddressableCreatorBasic>().Create(m_asset);
        if (asset2 is GameObject go2)
            go2.transform.position = Vector3.one;

        var asset3 = await Modules.Get<IAddressableCreatorAdvanced>().Create(m_label);
        if (asset3 is GameObject go3)
            go3.transform.position = -Vector3.one;
    }


}
