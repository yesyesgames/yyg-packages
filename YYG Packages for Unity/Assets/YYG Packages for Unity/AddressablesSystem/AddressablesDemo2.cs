
using UnityEngine;
using UnityEngine.AddressableAssets;

using YYG.Packages.Unity.ActivitySystem.Abstracts;
using YYG.Packages.Unity.AddressableModules.Interfaces;

public class AddressablesDemo2 : ComponentBase
{

    public AssetReference m_asset;


    private void Start()
    {
        Modules.Get<IAddressableResources>().OnProgress += (a, b) =>
        {
            Debug.Log($"IAddressablesResources: {a} / {b}");
        };

        Modules.Get<IAddressableResources>().OnComplete += () =>
        {
            var locations = Modules.Get<IAddressableResources>().Locations;
            Debug.Log($"IAddressablesResources::OnComplete with {locations.Count} locations.");
            foreach (var item in locations)
            {
                Modules.Get<IAddressableDownloader>().Load(item);
            }
        };

        Modules.Get<IAddressableDownloader>().OnProgress += (a, b) =>
        {
            Debug.Log($"IAddressablesDownloader: {a} / {b}");

        };

        Modules.Get<IAddressableDownloader>().OnComplete += async () =>
        {
            Debug.Log($"IAddressablesDownloader::OnComplete");

            Debug.Log(m_asset.IsDone);
            Debug.Log(m_asset.IsValid());

            var asset = await Modules.Get<IAddressableCreatorBasic>().Create(m_asset);
            (asset as GameObject).transform.position = Vector3.one;
        };

        Modules.Get<IAddressableResources>().Load();

    }


}
