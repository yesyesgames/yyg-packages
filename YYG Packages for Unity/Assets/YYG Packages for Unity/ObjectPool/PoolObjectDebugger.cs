
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using YYG.Packages.Unity.Core;
using YYG.Packages.Unity.Core.Objects;

namespace YYG.Packages
{
    public class PoolObjectDebugger : MonoBehaviour
    {

        public Param poolID;
        public GameObject prefab;

        [Range(1, 10)]
        public int amount = 4;

        private List<GameObject> gos = new List<GameObject>();


        public async void Begin()
        {
            for (int i = 0; i < amount; i++)
            {
                var pos = new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10));
                if (ObjectPool.Instance.Pull(poolID, out var go))
                {
                    go.transform.position = pos;
                    gos.Add(go);
                }
                else
                {
                    go = Instantiate(prefab);
                    go.transform.position = pos;
                    gos.Add(go);
                }
                await Task.Yield();
            }
        }


        public async void Cancel()
        {
            await ObjectPool.Instance.Push(gos);
            gos.Clear();
        }

    }
}
